/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute;

/**
 * Exception for print attribute combination.
 */
public class PrintAttributeException extends  PrintException{

    private String mProhibitionJobSetting;

    public PrintAttributeException(String prohibitionJobSetting) {
        super(prohibitionJobSetting);
        this.mProhibitionJobSetting = prohibitionJobSetting;
    }

    public String getUnsupportedAttributes() {
        return this.mProhibitionJobSetting;
    }
}

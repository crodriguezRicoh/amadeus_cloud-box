/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print;

import java.util.ArrayList;
import java.util.List;

import jp.co.ricoh.ssdk.function.print.attribute.PrintException;
import jp.co.ricoh.ssdk.function.print.attribute.PrintJobAttribute;
import jp.co.ricoh.ssdk.function.print.attribute.PrintJobAttributeSet;
import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttributeSet;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintJobState;
import jp.co.ricoh.ssdk.function.print.event.PrintJobAttributeEvent;
import jp.co.ricoh.ssdk.function.print.event.PrintJobAttributeListener;
import jp.co.ricoh.ssdk.function.print.event.PrintJobEvent;
import jp.co.ricoh.ssdk.function.print.event.PrintJobListener;
import jp.co.ricoh.ssdk.function.print.impl.job.JobListener;
import jp.co.ricoh.ssdk.function.print.impl.job.PrintJobMessageDispatcher;

/**
 * This class operates print jobs of the device.
 * This is available after print service notifies availability state.
 */
public class PrintJob {

    private PrintJobMessageDispatcher mHandler = null;

    private final List<PrintJobAttributeListener> mAttributeListeners = new ArrayList<PrintJobAttributeListener>();
    private final List<PrintJobListener> mJobListeners = new ArrayList<PrintJobListener>();

    // the job ID is assigned when the job starts
    private String mJobId = null;


    /**
     * Creates a print job.
     */
    public PrintJob(){
        this.mHandler = new PrintJobMessageDispatcher();

        // sets the listener for the asynchronous communications
        this.mHandler.setPrintJobListener(new JobListener() {
            @Override
            public void onChangeJobStatus(PrintJobAttributeSet jobStatus) {
                changeJobStatus(jobStatus);
            }

            @Override
            public void setJobId(String jobId) {
                setCurrentJobId(jobId);
            }

            @Override
            public String getJobId() {
                return getCurrentJobId();
            }
        });
    }

    /**
     * Sets the attribute listener for the print job.
     *
     * @param listener Attribute listener for the print job
     */
    public void addPrintJobAttributeListener(PrintJobAttributeListener listener) {
        if(listener == null) throw new IllegalArgumentException("listener is null");

        synchronized (this.mAttributeListeners) {
            if(this.mAttributeListeners.contains(listener)) {
                return;
            }

            this.mAttributeListeners.add(listener);
        }
    }

    /**
     * Removes the attribute listener.
     *
     * @param listener Attribute listener for the print job
     */
    public void removePrintJobAttributeListener(PrintJobAttributeListener listener) {
        if(listener == null) throw new IllegalArgumentException("listener is null");

        synchronized (this.mAttributeListeners) {
            if(!this.mAttributeListeners.contains(listener)) return;

            this.mAttributeListeners.remove(listener);
        }
    }

    /**
     * Sets the print job listener
     *
     * @param listener print job listener
     */
    public void addPrintJobListener(PrintJobListener listener) {
        if(listener == null) throw new IllegalArgumentException("listener is null");

        synchronized (this.mJobListeners) {
            if(this.mJobListeners.contains(listener)) return;

            this.mJobListeners.add(listener);
        }
    }

    /**
     * Removes the print job listener
     *
     * @param listener print job listener
     */
    public void removePrintJobListener(PrintJobListener listener) {
        if(listener == null) throw new IllegalArgumentException("listener is null");

        synchronized (this.mJobListeners) {
            if(!this.mJobListeners.contains(listener)) return;

            this.mJobListeners.remove(listener);
        }
    }

    /**
     * Starts the print job
     * If the device cannot accept the print job, "false" is returned.
     *
     * @return Indicates whether or not the request to start the print job has succeeded
     * @throws PrintException
     */
    public boolean print(PrintFile printFile, PrintRequestAttributeSet attributes, PrintUserCode userCode) throws PrintException {
        if(this.mHandler == null) throw new IllegalStateException("Cannot print after printing is completed.");
        if(printFile == null) throw new IllegalArgumentException("printFile is null");

        return mHandler.requestStartPrintJob(printFile, attributes, userCode);
    }
    public boolean print(PrintFile printFile, PrintRequestAttributeSet attributes) throws PrintException {
        return print(printFile, attributes, null);
    }

    /**
     * Validates the print job request attribute.
     *
     * @return Indicates whether or not the request to start the print job has succeeded
     * @throws PrintException
     */
    public boolean verify(PrintFile printFile, PrintRequestAttributeSet attributes, PrintUserCode userCode)
            throws PrintException {
        if(this.mHandler == null) throw new IllegalStateException("Cannot print after printing is completed.");
        return mHandler.verifyPrintJob(printFile, attributes, userCode);
    }
    public boolean verify(PrintFile printFile, PrintRequestAttributeSet attributes)
            throws PrintException {
        return verify(printFile, attributes, null);
    }

    /**
     * Obtains the job attribute value of the specified job.
     *
     * @param category The PrintJobAttribute category to obtain. Null cannot be specified.
     * @return The current job value. If there is no corresponding attribute value, null is returned.
     * @throws IllegalStateException specifying null to category
     */
    public PrintJobAttribute getJobAttribute(Class<? extends PrintJobAttribute> category, PrintUserCode userCode) {
        if(category == null) throw new IllegalArgumentException("category is null");
        if(this.mHandler == null) throw new IllegalStateException("Cannot print after printing is completed.");

        PrintJobAttributeSet attributes = mHandler.requestJobStates(userCode);
        return attributes.get(category);
    }
    public PrintJobAttribute getJobAttribute(Class<? extends PrintJobAttribute> category) {
        return getJobAttribute(category, null);
    }

    /**
     * Obtains the current job attribute set for this job.
     * @return attribute set
     */
    public PrintJobAttributeSet getJobAttributes(PrintUserCode userCode) {
        if(this.mHandler == null) throw new IllegalStateException("Cannot print after printing is completed.");
        return mHandler.requestJobStates(userCode);
    }
    public PrintJobAttributeSet getJobAttributes() {
        return getJobAttributes(null);
    }

    /**
     * Cancels the print job.
     * Requests the device to cancel the print job if the print job is being paused or being in process.
     * Print job cancel may not be accepted depending on the device state.
     * @throws  PrintException
     */
    public boolean cancelPrintJob(PrintUserCode userCode) throws PrintException {
        if(this.mHandler == null) throw new IllegalStateException("Cannot print after printing is completed.");
        return mHandler.requestCancelPrintJob(userCode);
    }
    public boolean cancelPrintJob() throws PrintException {
        return cancelPrintJob(null);
    }

    /**
     * Job state change
     */
    private void changeJobStatus(PrintJobAttributeSet attributeSet) {
        if(attributeSet.size() > 0) {
            notifyAttributeListeners(attributeSet);
        }

        // notify job state if the job state is changed
        if(attributeSet.get(PrintJobState.class) != null) {
            notifyJobListeners(attributeSet);
        }

        // The job ends if the job state is ABORTED, CANCELED, or COMPLETED.
        PrintJobState jobState = (PrintJobState)attributeSet.get(PrintJobState.class);

        if(PrintJobState.COMPLETED.equals(jobState) ||
                PrintJobState.CANCELED.equals(jobState) ||
                PrintJobState.ABORTED.equals(jobState)) {
            // releases unnecessary objects.
            this.mHandler.destroyJob();
            this.mAttributeListeners.clear();
            this.mJobListeners.clear();
            this.mHandler = null;
        }
    }

    private void setCurrentJobId(String jobId) {
        this.mJobId = jobId;
    }

    private String getCurrentJobId() {
        return this.mJobId;
    }

    private void notifyAttributeListeners(PrintJobAttributeSet attributes) {
        PrintJobAttributeListener[] listeners;
        synchronized (this.mAttributeListeners) {
            listeners = this.mAttributeListeners
                    .toArray(new PrintJobAttributeListener[this.mAttributeListeners.size()]);
        }

        if(listeners.length > 0) {
            PrintJobAttributeEvent event = new PrintJobAttributeEvent(this, attributes);
            for(PrintJobAttributeListener listener : listeners) {
                listener.updateAttributes(event);
            }
        }
    }

    private void notifyJobListeners(PrintJobAttributeSet attributes) {
        PrintJobListener[] listeners;
        synchronized (this.mJobListeners) {
            listeners = this.mJobListeners.toArray(new PrintJobListener[this.mJobListeners.size()]);
        }

        if(listeners.length > 0) {
            PrintJobEvent event = new PrintJobEvent(attributes);
            PrintJobState jobState = (PrintJobState)attributes.get(PrintJob.class);
            for(PrintJobListener listener : listeners) {
                switch(jobState){
                    case ABORTED:
                        listener.jobAborted(event);
                        break;
                    case CANCELED:
                        listener.jobCanceled(event);
                        break;
                    case COMPLETED:
                        listener.jobCompleted(event);
                        break;
                    case PENDING:
                        listener.jobPending(event);
                        break;
                    case PROCESSING:
                        listener.jobProcessing(event);
                        break;
                    case PROCESSING_STOPPED:
                        listener.jobProcessingStop(event);
                        break;
                    default:
                        throw new AssertionError("Unknown PrintJobState:" + jobState);
                }
            }
        }
    }
}

/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.event;

/**
 * The listener interface to receive job attribute events.
 */
public interface PrintJobAttributeListener {

    /**
     * Executed when print job state changes.
     *
     * @param attributesEvent Print job attribute event
     * @see PrintServiceAttributeEvent
     */
    public abstract void updateAttributes(PrintJobAttributeEvent attributesEvent);
}

/*
 *  Copyright (C) 2016 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.wrapper.rws.service.fax;

import java.util.Map;

import jp.co.ricoh.ssdk.wrapper.common.Element;
import jp.co.ricoh.ssdk.wrapper.common.ResponseBody;

/*
 * @since SmartSDK V2.12
 */
public class CreateLockResponseBody extends Element implements ResponseBody {

	private static final String KEY_LOCK_ID	= "lockId";

	CreateLockResponseBody(Map<String, Object> values) {
		super(values);
	}

	/*
	 * lockId (String)
	 * @since SmartSDK V2.12
	 */
	public String getLockId() {
		return getStringValue(KEY_LOCK_ID);
	}

}

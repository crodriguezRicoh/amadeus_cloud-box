/*
 *  Copyright (C) 2016 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.wrapper.rws.service.fax;

import java.util.Map;

import jp.co.ricoh.ssdk.wrapper.common.Element;
import jp.co.ricoh.ssdk.wrapper.common.ResponseBody;

/*
 * @since SmartSDK V2.12
 */
public class CreateFaxFileResponseBody extends Element implements ResponseBody {

	private static final String KEY_FILE_ID	= "fileId";

	CreateFaxFileResponseBody(Map<String, Object> values) {
		super(values);
	}

	/*
	 * fileId (String)
	 * @since SmartSDK V2.12
	 */
	public String getFileId() {
		return getStringValue(KEY_FILE_ID);
	}

}

/**
 * Copyright (C) 2016 Ricoh Spain IT Services.
 * All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute.standard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttribute;

/**
 * The class to indicate paper tray.
 *
 * @author Manuel Madera
 */
public enum PaperTray implements PrintRequestAttribute {
    LARGE_CAPACITY("large_capacity"),
    MAIN("auto"),
    MANUAL("manual"),
    TRAY1("tray1"),
    TRAY2("tray2"),
    TRAY3("tray3"),
    TRAY4("tray4"),
    TRAY5("tray5"),
    TRAY6("tray6"),
    TRAY7("tray7");

    private static final String PAPER_TRAY = "paperTray";
    private final String mPaperTray;

    private PaperTray(String value) {
        this.mPaperTray = value;
    }

    @Override
    public Object getValue() {
        return mPaperTray;
    }

    @Override
    public Class<?> getCategory() {
        return getClass();
    }

    @Override
    public String getName() {
        return PAPER_TRAY;
    }

    private static volatile Map<String, PaperTray> directory = null;

    private static Map<String, PaperTray> getDirectory() {
        if(directory == null) {
            Map<String, PaperTray> d = new HashMap<String, PaperTray>();
            for(PaperTray tray : values()) {
                d.put(tray.getValue().toString(), tray);
            }
            directory = d;
        }
        return directory;
    }

    private static PaperTray fromString(String value) {
        return getDirectory().get(value);
    }

    public static List<PaperTray> getSupportedValue(List<String> values) {
        if( values == null ) {
            return Collections.emptyList();
        }

        List<PaperTray> list = new ArrayList<PaperTray>();
        for(String value : values) {
            PaperTray tray = fromString(value);
            if( tray != null ) {
                list.add(tray);
            }
        }

        return list;
    }
}

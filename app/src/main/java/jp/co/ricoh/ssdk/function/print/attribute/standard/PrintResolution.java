/**
 * Copyright (C) 2016 Ricoh Spain IT Services.
 * All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute.standard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttribute;

/**
 * The class to indicate print resolution mode.
 *
 * @author Manuel Madera
 */
public enum PrintResolution implements PrintRequestAttribute {

    DPI_1200_1200_1("1200_1200_1"),
    DPI_1200_1200_2("1200_1200_2"),
    DPI_1200_600_1("1200_600_1"),
    DPI_200_200_1("200_200_1"),
    DPI_300_300_1("300_300_1"),
    DPI_400_400_1("400_400_1"),
    DPI_600_1200_1("600_1200_1"),
    DPI_600_600_1("600_600_1"),
    DPI_600_600_2("600_600_2"),
    DPI_600_600_4("600_600_4"),
    DPI_600_600_8("600_600_8");

    private static final String PRINT_RESOLUTION = "printResolution";
    private final String mPrintResolution;

    private PrintResolution(String resolution) {
        this.mPrintResolution = resolution;
    }

    @Override
    public Object getValue() {
        return mPrintResolution;
    }

    @Override
    public Class<?> getCategory() {
        return getClass();
    }

    @Override
    public String getName() {
        return PRINT_RESOLUTION;
    }

    private static volatile Map<String, PrintResolution> directory = null;

    private static Map<String, PrintResolution> getDirectory() {
        if(directory == null) {
            Map<String, PrintResolution> d = new HashMap<String, PrintResolution>();
            for(PrintResolution resolution : values()) {
                d.put(resolution.getValue().toString(), resolution);
            }
            directory = d;
        }
        return directory;
    }

    private static PrintResolution fromString(String value) {
        return getDirectory().get(value);
    }

    public static List<PrintResolution> getSupportedValue(List<String> values) {
        if( values == null ) {
            return Collections.emptyList();
        }

        List<PrintResolution> list = new ArrayList<PrintResolution>();
        for(String value : values) {
            PrintResolution resolution = fromString(value);
            if( resolution != null ) {
                list.add(resolution);
            }
        }

        return list;
    }
}

/*
 *  Copyright (C) 2013-2016 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.wrapper.rws.addressbook;

import jp.co.ricoh.ssdk.wrapper.common.RequestBody;
import jp.co.ricoh.ssdk.wrapper.common.Utils;
import jp.co.ricoh.ssdk.wrapper.json.EncodedException;
import jp.co.ricoh.ssdk.wrapper.json.JsonUtils;
import jp.co.ricoh.ssdk.wrapper.log.Logger;

import java.util.HashMap;
import java.util.Map;

public class CreateEntryRequestBody extends Entry implements RequestBody {

	private static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";
	
    /**
     * Define the prefix for log information with abbreviation package and class name
     */
    private final static String PREFIX = "addressBook:CreateEntryReq:";
	
	public CreateEntryRequestBody() {
		super(new HashMap<String, Object>());
	}
	public CreateEntryRequestBody(Map<String, Object> values) {
		super(values);
	}

	@Override
	public String getContentType() {
		return CONTENT_TYPE_JSON;
	}
	
	@Override
	public String toEntityString() {
		try {
			return JsonUtils.getEncoder().encode(values);
		} catch (EncodedException e) {
            Logger.warn(Utils.getTagName(),PREFIX + e.toString());
			return "{}";
		}
	}
	
}

/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.wrapper.rws.addressbook;

import java.util.Map;

import jp.co.ricoh.ssdk.wrapper.common.ResponseBody;

public class UpdateTagResponseBody extends Tag implements ResponseBody {

	UpdateTagResponseBody(Map<String, Object> values) {
		super(values);
	}

}

/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.scan.attribute;

import jp.co.ricoh.ssdk.function.attribute.AttributeSet;

/**
 * スキャンジョブの属性を管理する属性セットインターフェイスです
 * Attribute set interface to manage scan job attributes.
 */
public interface ScanJobAttributeSet extends AttributeSet<ScanJobAttribute> {


}

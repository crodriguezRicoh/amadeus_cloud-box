/*
 *  Copyright (C) 2016 RICOH Co.,LTD.
 *  All rights reserved.
 */

package jp.co.ricoh.ssdk.wrapper.rws.property;
import java.util.Map;

import jp.co.ricoh.ssdk.wrapper.common.Element;
import jp.co.ricoh.ssdk.wrapper.common.ResponseBody;

/*
 * @since SmartSDK V2.12
 */
public class GetLdapSettingResponseBody extends Element implements ResponseBody{
	
	private static final String KEY_LDAP_SEARCH = "ldapSearch";
	
	GetLdapSettingResponseBody(Map<String, Object> values) {
		super(values);
	}

	/*
	 * ldapSearch (String)
	 * @since SmartSDK V2.12
	 */
	public String getLdapSearch() {
		return getStringValue(KEY_LDAP_SEARCH);
	}
}
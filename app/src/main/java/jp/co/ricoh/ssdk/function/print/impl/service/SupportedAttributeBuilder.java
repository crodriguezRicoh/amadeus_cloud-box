/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.impl.service;

import java.util.HashMap;
import java.util.Map;

import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttribute;
import jp.co.ricoh.ssdk.function.print.attribute.standard.Copies;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PaperTray;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintColor;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintResolution;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintSide;
import jp.co.ricoh.ssdk.function.print.attribute.standard.Staple;
import jp.co.ricoh.ssdk.function.print.supported.MaxMinSupported;
import jp.co.ricoh.ssdk.wrapper.rws.service.printer.Capability;

/**
 * The class to create supportedAttribute from specified capability object.
 */
public class SupportedAttributeBuilder {
    private SupportedAttributeBuilder() {
    }

    public static Map<Class<? extends PrintRequestAttribute>, Object> getSupportedAttribute(Capability cap) {
        if( cap == null ) return null;

        Map<Class<? extends PrintRequestAttribute>, Object> retList = new HashMap<Class<? extends PrintRequestAttribute>, Object>();

        if(cap.getCopiesRange() != null ) {
            retList.put(Copies.class, MaxMinSupported.getMaxMinSupported(cap.getCopiesRange()));
        }

        if(cap.getStapleList() != null) {
            retList.put(Staple.class, Staple.getSupportedValue(cap.getStapleList()));
        }

        if(cap.getPrintColorList() != null) {
            retList.put(PrintColor.class, PrintColor.getSupportedValue(cap.getPrintColorList()));
        }

        if(cap.getPrintResolutionList() != null) {
            retList.put(PrintResolution.class, PrintResolution.getSupportedValue(cap.getPrintResolutionList()));
        }

        if(cap.getPrintSideList() != null) {
            retList.put(PrintSide.class, PrintSide.getSupportedValue(cap.getPrintSideList()));
        }

        if(cap.getPaperTrayList() != null) {
            retList.put(PaperTray.class, PaperTray.getSupportedValue(cap.getPaperTrayList()));
        }

        return retList;
    }
}

/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute;

import jp.co.ricoh.ssdk.function.attribute.AttributeSet;

/**
 * Attribute set interface to indicate print job attributes.
 */
public interface PrintJobAttributeSet extends AttributeSet<PrintJobAttribute> {

}

/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.event;

/**
 * The listener interface to notify that the print service state has changed.
 */
public interface PrintServiceAttributeListener {

    /**
     * Executed when print service state changes.
     *
     * @param event Print service event
     * @see PrintServiceAttributeEvent
     */
    public abstract void attributeUpdate(PrintServiceAttributeEvent event);
}

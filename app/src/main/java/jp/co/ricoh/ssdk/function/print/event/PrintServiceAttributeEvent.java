/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.event;

import jp.co.ricoh.ssdk.function.print.attribute.PrintServiceAttributeSet;

/**
 * Print service state change event
 */
public final class PrintServiceAttributeEvent {
    PrintServiceAttributeSet mAttributes = null;

    public PrintServiceAttributeEvent(PrintServiceAttributeSet attributes) {
        this.mAttributes = attributes;
    }

    public PrintServiceAttributeSet getAttributes() {
        return this.mAttributes;
    }
}

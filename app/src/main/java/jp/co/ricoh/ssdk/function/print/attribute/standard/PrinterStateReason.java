/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute.standard;

import java.util.HashMap;
import java.util.Map;

import jp.co.ricoh.ssdk.function.print.attribute.PrintServiceAttribute;

/**
 * The enum to indicate print service state reason.
 */
public enum PrinterStateReason implements PrintServiceAttribute {

    /**
     * Device cover is open.
     */
    COVER_OPEN("cover_open"),

    /**
     * Developer is empty.
     */
    DEVELOPER_EMPTY("developer_empty"),

    /**
     * Developer is almost empty.
     */
    DEVELOPER_LOW("developer_low"),

    /**
     * Paper tray is not set to the device.
     */
    INPUT_TRAY_MISSING("input_tray_missing"),

    /**
     * Interpreter resources (e.g. fonts, forms) are unavailable.
     */
    INTERPRETER_RESOURCE_UNAVAILABLE("interpreter_resource_unavailable"),

    /**
     * Marker supply waste receptacle is almost full.
     */
    MARKER_WASTE_ALMOST_FULL("marker_waste_almost_full"),

    /**
     * Marker supply waste receptacle is full.
     */
    MARKER_WASTE_FULL("marker_waste_full"),

    /**
     * A tray is empty.
     */
    MEDIA_EMPTY("media_empty"),

    /**
     * Paper jam occurred.
     */
    MEDIA_JAM("media_jam"),

    /**
     * OPC life is over.
     */
    OPC_LIFE_OVER("opc_life_over"),

    /**
     * OPC needs to be replaced soon.
     */
    OPC_NEAR_EOL("opc_near_eol"),

    /**
     * Other error has been detected.
     */
    OTHER("other"),

    /**
     * An output tray is full.
     */
    OUTPUT_AREA_FULL("output_area_full"),

    /**
     * Output tray is not set to the device.
     */
    OUTPUT_TRAY_MISSING("output_tray_missing"),

    /**
     * Printer has paused and the state is "stopped"
     */
    PAUSED("paused"),

    /**
     * If printer controls multiple output devices, this indicates that one or more output device is being stopped.
     */
    STOPPED_PARTLY("stopped_partly"),

    /**
     * Toner is empty.
     */
    TONER_EMPTY("toner_empty"),

    /**
     * Toner is almost empty.
     */
    TONER_LOW("toner_low"),

    /**
     * Communication log is full. Log output is needed.
     */
    COMMUNICATION_LOG_FULL("communication_log_full");


    private final String mPrinterStateReason;

    private PrinterStateReason(String value) {
        this.mPrinterStateReason = value;
    }

    @Override
    public String toString() {
        return mPrinterStateReason;
    }

    @Override
    public Class<?> getCategory() {
        return getClass();
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }


    private static volatile Map<String, PrinterStateReason> reasons = null;

    private static Map<String, PrinterStateReason> getReasons() {
        if(reasons == null) {
            PrinterStateReason[] reasonArray = values();
            Map<String, PrinterStateReason> r = new HashMap<String, PrinterStateReason>();
            for(PrinterStateReason reason : reasonArray) {
                r.put(reason.mPrinterStateReason, reason);
            }

            reasons = r;
        }
        return reasons;
    }

    public static PrinterStateReason fromString(String value) {
        return getReasons().get(value);
    }

}

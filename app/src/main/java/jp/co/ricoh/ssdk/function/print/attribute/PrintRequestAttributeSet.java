/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute;

import jp.co.ricoh.ssdk.function.attribute.AttributeSet;

/**
 * The Attribute set that can be specified at the time of printing.
 */
public interface PrintRequestAttributeSet extends AttributeSet<PrintRequestAttribute> {

}

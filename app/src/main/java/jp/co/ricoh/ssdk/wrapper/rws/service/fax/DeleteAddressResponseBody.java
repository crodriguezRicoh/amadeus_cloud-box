/*
 *  Copyright (C) 2016 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.wrapper.rws.service.fax;

import java.util.Map;

import jp.co.ricoh.ssdk.wrapper.common.Element;
import jp.co.ricoh.ssdk.wrapper.common.ResponseBody;

/*
 * @since SmartSDK V2.12
 */
public class DeleteAddressResponseBody extends Element implements ResponseBody {

	private static final String KEY_ADDRESS_COUNT	= "addressCount";

	DeleteAddressResponseBody(Map<String, Object> values) {
		super(values);
	}

	/*
	 * addressCount (Number)
	 * @since SmartSDK V2.12
	 */
	public Integer getAddressCount() {
		return getNumberValue(KEY_ADDRESS_COUNT);
	}

}

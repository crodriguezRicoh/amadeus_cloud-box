/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.scan.attribute.standard;

public interface DestinationSettingItem {
	
	public Object getValue();

}

/**
 * Copyright (C) 2016 Ricoh Spain IT Services.
 * All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute.standard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttribute;

/**
 * The class to indicate print side mode.
 *
 * @author Manuel Madera
 */
public enum PrintSide implements PrintRequestAttribute {
    ONE_SIDED("one_sided"),
    DUPLEX_TOP_TO_TOP("top_to_top"),
    DUPLEX_TOP_TO_BOTTOM("top_to_bottom");


    private static final String PRINT_SIDE = "printSide";
    private final String mPrintSide;

    private PrintSide(String value) {
        this.mPrintSide = value;
    }

    @Override
    public Object getValue() {
        return mPrintSide;
    }

    @Override
    public Class<?> getCategory() {
        return getClass();
    }

    @Override
    public String getName() {
        return PRINT_SIDE;
    }

    private static volatile Map<String, PrintSide> directory = null;

    private static Map<String, PrintSide> getDirectory() {
        if(directory == null) {
            Map<String, PrintSide> d = new HashMap<String, PrintSide>();
            for(PrintSide side : values()) {
                d.put(side.getValue().toString(), side);
            }
            directory = d;
        }
        return directory;
    }

    private static PrintSide fromString(String value) {
        return getDirectory().get(value);
    }

    public static List<PrintSide> getSupportedValue(List<String> values) {
        if( values == null ) {
            return Collections.emptyList();
        }

        List<PrintSide> list = new ArrayList<PrintSide>();
        for(String value : values) {
            PrintSide side = fromString(value);
            if( side != null ) {
                list.add(side);
            }
        }

        return list;
    }
}

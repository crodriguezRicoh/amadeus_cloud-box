/*
 *  Copyright (C) 2013-2014 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.ricoh.ssdk.function.common.impl.AsyncConnectState;
import jp.co.ricoh.ssdk.function.print.attribute.PrintJobAttributeSet;
import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttribute;
import jp.co.ricoh.ssdk.function.print.attribute.PrintServiceAttribute;
import jp.co.ricoh.ssdk.function.print.attribute.PrintServiceAttributeSet;
import jp.co.ricoh.ssdk.function.print.event.PrintServiceAttributeEvent;
import jp.co.ricoh.ssdk.function.print.event.PrintServiceAttributeListener;
import jp.co.ricoh.ssdk.function.print.impl.service.FunctionMessageDispatcher;
import jp.co.ricoh.ssdk.function.print.impl.service.ServiceListener;
import jp.co.ricoh.ssdk.function.print.impl.service.SupportedAttributeBuilder;

/**
 * Provides the print service functions for printing.
 */
public class PrintService {

    private static PrintService mServiceInstance;

    private final List<PrintServiceAttributeListener> mPrintServiceAttributeListeners = new ArrayList<PrintServiceAttributeListener>();

    private final Map<PrintFile.PDL, Map<Class<? extends PrintRequestAttribute>, Object>> mCapabilitiesMap;

    static {
        mServiceInstance = new PrintService();
    }

    private PrintService() {
        this.mCapabilitiesMap = new HashMap<PrintFile.PDL, Map<Class<? extends PrintRequestAttribute>, Object>>();
    }

    /**
     * Obtains the print service.
     * @return Available print service
     */
    public static PrintService getService() {
        return mServiceInstance;
    }

    /**
     * Registers the listener to monitor print service state changes.
     *
     * @param listener print service attribute listener
     * @throws IllegalArgumentException When the specified listener is null
     * @return Returns the asynchronous connection state. If the device is unavailable, null is returned.
     */
    public AsyncConnectState addPrintServiceAttributeListener(PrintServiceAttributeListener listener) {
        if(listener == null) throw new IllegalArgumentException("listener is null");

        AsyncConnectState addListenerResult = null;
        synchronized (this.mPrintServiceAttributeListeners) {
            if( mPrintServiceAttributeListeners.contains(listener) ) {
                // do nothing if the specified listener is already registered
                return AsyncConnectState.valueOf(AsyncConnectState.STATE.CONNECTED, AsyncConnectState.ERROR_CODE.NO_ERROR);
            }

            if( this.mPrintServiceAttributeListeners.size() == 0 ) {
                addListenerResult =
                    FunctionMessageDispatcher.getInstance().addServiceListener(new ServiceListener() {
                        @Override
                        public void onChangePrintServiceAttributes(PrintServiceAttributeSet attributes) {
                            changePrintServiceAttributes(attributes);
                        }
                    });

                if(addListenerResult == null) {
                    return null;
                }
                if(addListenerResult.getState() != AsyncConnectState.STATE.CONNECTED) {
                    return addListenerResult;
                }
            } else {
                addListenerResult = AsyncConnectState.valueOf(AsyncConnectState.STATE.CONNECTED, AsyncConnectState.ERROR_CODE.NO_ERROR);
            }
            mPrintServiceAttributeListeners.add(listener);
        }

        // notify the specified listener of the current service state.
        PrintServiceAttributeSet notifySet = getAttributes();
        if(notifySet.size() > 0) {
            listener.attributeUpdate(new PrintServiceAttributeEvent(notifySet));
        }

        return addListenerResult;
    }


    /**
     * Removes the listener to monitor print service state changes
     *
     * @param listener The listener to remove
     * @return Returns true if the listener unregistration has succeeded.
     * @throws IllegalArgumentException When the specified listener is null
     * @throws IllegalStateException When the specified listener is not registered
     */
    public boolean removePrintServiceAttributeListener(PrintServiceAttributeListener listener) {
        if(listener == null) throw new IllegalArgumentException("listener is null");

        synchronized( this.mPrintServiceAttributeListeners ) {
            if(!this.mPrintServiceAttributeListeners.contains(listener)) return false;

            if(this.mPrintServiceAttributeListeners.size() == 1) {
                FunctionMessageDispatcher.getInstance().removeServiceListener();
            }

            this.mPrintServiceAttributeListeners.remove(listener);
        }

        return true;
    }

    /**
     * Obtains the state of asynchronous event connection
     *
     * @return State of asynchronous event connection
     */
    public AsyncConnectState getAsyncConnectState() {
        return FunctionMessageDispatcher.getInstance().getAsyncConnectState();
    }

    /**
     * Obtains the print service attribute set.
     *
     * @return Current print service attribute set. If the attribute set cannot be obtained, an empty set is returned.
     */
    public PrintServiceAttributeSet getAttributes() {
        return FunctionMessageDispatcher.getInstance().getPrintStatus();
    }

    /**
     * Obtains the print service attribute for the specified category.
     *
     * @param category
     * @return Returns the current print service attribute for the specified category. If the attribute cannot be obtained, null is returned.
     */
    public PrintServiceAttribute getAttribute(Class<? extends PrintServiceAttribute> category) {
        if( category == null ) throw new IllegalArgumentException("category is null");

        PrintServiceAttributeSet attributeSet = FunctionMessageDispatcher.getInstance().getPrintStatus();
        if(attributeSet == null) return null;

        return attributeSet.get(category);
    }


    /**
     * Obtains information on supported PDL
     * @return supported PDL list
     */
    public List<PrintFile.PDL> getSupportedPDL() {
        List<PrintFile.PDL> supportedPdlList = new ArrayList<PrintFile.PDL>();
        List<String> supportedPdlValueList = FunctionMessageDispatcher.getInstance().getSupportedPDL();
        if(supportedPdlValueList == null) return null;

        for(String pdlValue : supportedPdlValueList) {
            PrintFile.PDL pdl = PrintFile.PDL.fromString(pdlValue);
            if(pdl != null) {
                supportedPdlList.add(pdl);
            }
        }

        return supportedPdlList;
    }

    /**
     * Obtains the list of print job attribute categories that can be set, at the time of setting the attribute of the job
     *
     * @param pdl
     * @return Set of job attribute categories
     */
    public Set<Class<? extends PrintRequestAttribute>> getSupportedAttributeCategories(PrintFile.PDL pdl) {
        if(pdl == null) throw new IllegalArgumentException("pdl is null");

        Map<Class<? extends PrintRequestAttribute>, Object> capabilities = null;
        synchronized (this.mCapabilitiesMap) {
            capabilities  = this.mCapabilitiesMap.get(pdl);
        }

        if(capabilities == null) {
            capabilities = SupportedAttributeBuilder.getSupportedAttribute(
                    FunctionMessageDispatcher.getInstance().getPrintCapability(pdl));

            if(capabilities == null) return null;

            synchronized (this.mCapabilitiesMap){
                mCapabilitiesMap.put(pdl,capabilities);
            }
        }

        return Collections.unmodifiableSet(capabilities.keySet());
    }

    /**
     * Obtains the values that can be specified for each print job attribute category at the time of setting the job for this print service.
     *
     * @param pdl
     * @param category
     * @return attribute value
     */
    public Object getSupportedAttributeValues(PrintFile.PDL pdl, Class<? extends PrintRequestAttribute> category) {
        if(category == null) throw new IllegalArgumentException("category is null");
        if(pdl == null) throw new IllegalArgumentException("pdl is null");

        Map<Class<? extends PrintRequestAttribute>, Object> capabilities = null;
        synchronized (this.mCapabilitiesMap) {
            capabilities  = this.mCapabilitiesMap.get(pdl);
        }

        if(capabilities == null) {
            capabilities = SupportedAttributeBuilder.getSupportedAttribute(
                    FunctionMessageDispatcher.getInstance().getPrintCapability(pdl));

            if(capabilities == null) return null;

            synchronized (this.mCapabilitiesMap){
                mCapabilitiesMap.put(pdl,capabilities);
            }
        }

        return capabilities.get(category);
    }

    /**
     * Obtains the value type that can be specified for each print job attribute category at the time of setting the job for this print service.
     *
     * @param  pdl
     * @param category
     * @return type
     */
    public Class<?> getSupportedAttributeType(PrintFile.PDL pdl, Class<? extends PrintRequestAttribute> category) {
        if(category == null) throw new IllegalArgumentException("category is null");
        if(pdl == null) throw new IllegalArgumentException("pdl is null");

        Map<Class<? extends PrintRequestAttribute>, Object> capabilities = null;
        synchronized (this.mCapabilitiesMap) {
            capabilities  = this.mCapabilitiesMap.get(pdl);
        }

        if(capabilities == null) {
            capabilities = SupportedAttributeBuilder.getSupportedAttribute(
                    FunctionMessageDispatcher.getInstance().getPrintCapability(pdl));

            if(capabilities == null) return null;

            synchronized (this.mCapabilitiesMap){
                mCapabilitiesMap.put(pdl,capabilities);
            }
        }

        return capabilities.get(category).getClass();
    }


    public List<PrintJobAttributeSet> getJobList(PrintUserCode userCode) {
        return FunctionMessageDispatcher.getInstance().getPrintJobList(userCode);
    }

    /**
     * Service state change notification sent from internal management layer
     */
    private void changePrintServiceAttributes(PrintServiceAttributeSet attributes) {

        synchronized (this.mPrintServiceAttributeListeners) {
            if(attributes.size() <= 0) return;
            notifyAttributeListeners(attributes);
        }
    }

    /**
     * Service state change notification
     *
     * @param attributeSet
     */
    private void notifyAttributeListeners(PrintServiceAttributeSet attributeSet) {
        PrintServiceAttributeListener[] listeners;
        synchronized (this.mPrintServiceAttributeListeners) {
            listeners = this.mPrintServiceAttributeListeners
                            .toArray(new PrintServiceAttributeListener[this
                                    .mPrintServiceAttributeListeners.size()]);
        }

        if(listeners.length > 0) {
            PrintServiceAttributeEvent event = new PrintServiceAttributeEvent(attributeSet);
            for(PrintServiceAttributeListener listener : listeners) {
                listener.attributeUpdate(event);
            }
        }
    }
}

/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.event;

import jp.co.ricoh.ssdk.function.print.attribute.PrintJobAttributeSet;

/**
 * The event class which stores event information for the time of notifying print job events.
 */
public class PrintJobEvent {
    private PrintJobAttributeSet mAttributeSet;

    public PrintJobEvent(PrintJobAttributeSet attributeSet) {
         this.mAttributeSet = attributeSet;
    }

    public PrintJobAttributeSet getAttributeSet() {
        return this.mAttributeSet;
    }
}

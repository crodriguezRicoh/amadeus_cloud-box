/*
 *  Copyright (C) 2016 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function;

public class Const {
    
    //private constructor
    private Const(){};
    
    //The log tag to be used in all classes of ScanSample module
    public final static String TAG = "ScanToBox";

}

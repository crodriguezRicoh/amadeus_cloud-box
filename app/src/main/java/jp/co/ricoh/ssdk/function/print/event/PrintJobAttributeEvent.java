/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.event;

import jp.co.ricoh.ssdk.function.print.PrintJob;
import jp.co.ricoh.ssdk.function.print.attribute.PrintJobAttributeSet;

/**
 * The event class to notify print job state changes.
 */
public class PrintJobAttributeEvent {

    private PrintJob mEventSource = null;
    private PrintJobAttributeSet mPrintJobAttributeSet = null;

    public PrintJobAttributeEvent(PrintJob source, PrintJobAttributeSet attributes) {
        this.mEventSource = source;
        this.mPrintJobAttributeSet = attributes;
    }

    public PrintJob getSource() {
        return this.mEventSource;
    }

    public PrintJobAttributeSet getUpdateAttributes() {
        return this.mPrintJobAttributeSet;
    }
}

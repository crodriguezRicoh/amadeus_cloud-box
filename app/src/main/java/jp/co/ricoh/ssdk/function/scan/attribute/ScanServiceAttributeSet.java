/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.scan.attribute;

import jp.co.ricoh.ssdk.function.attribute.AttributeSet;

/**
 * スキャンサービス属性を管理する属性セットインタフェースです。
 * Attribute set interface to manage scan service attributes.
 */
public interface ScanServiceAttributeSet extends AttributeSet<ScanServiceAttribute> {

}

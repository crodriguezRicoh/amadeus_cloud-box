/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.impl.service;

import jp.co.ricoh.ssdk.wrapper.rws.service.printer.GetJobStatusResponseBody;

/**
 * Listener for asynchronous communication by job
 */
public interface AsyncJobEventHandler {

    void onReceiveJobEvent(GetJobStatusResponseBody event);

    String getJobId();
}

/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute;

import jp.co.ricoh.ssdk.function.attribute.Attribute;

/**
 *  Attribute set interface to indicate print request attributes.
 */
public interface PrintRequestAttribute extends Attribute {
    public abstract Object getValue();
}

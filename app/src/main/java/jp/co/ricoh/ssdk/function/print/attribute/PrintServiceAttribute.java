/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute;

import jp.co.ricoh.ssdk.function.attribute.Attribute;

/**
 * The interface to indicate print service attributes.
 */
public interface PrintServiceAttribute extends Attribute {

}

/*
 *  Copyright (C) 2013-2016 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute.standard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttribute;

/**
 * The class to indicate staple settings.
 */
public enum Staple implements PrintRequestAttribute {

    /**
     * 2 at Left
     */
    DUAL_LEFT("dual_left"),

    /**
     * 2 at right
     */
    DUAL_RIGHT("dual_right"),

    /**
     * 2 at top
     */
    DUAL_TOP("dual_top"),

    /**
     * Saddle Stitch
     */
    SADDLE_STITCH("saddle_stitch"),

    /**
     * Top left
     */
    TOP_LEFT("top_left"),

    /**
     * Top left slant
     */
    TOP_LEFT_SLANT("top_left_slant"),

    /**
     * Top right
     */
    TOP_RIGHT("top_right"),

    /**
     * Top right slant
     */
    TOP_RIGHT_SLANT("top_right_slant"),

    /**
     * Bottom left
     */
    BOTTOM_LEFT("bottom_left"),

    /**
     * Bottom left slant.
     */
    BOTTOMLEFT_SLANT("bottomleft_slant"),

    /**
     * No staple
     */
    NONE("none"),
    
    /**
     * Top left slant stapleless
     * 
     * @since SmartSDK V2.12
     */
    TOP_LEFT_SLANT_STAPLELESS("top_left_slant_stapleless"),
    
    /**
     * Top right slant stapleless
     * 
     * @since SmartSDK V2.12
     */
    TOP_RIGHT_SLANT_STAPLELESS("top_right_slant_stapleless");
    
    private final String STAPLE = "staple";
    private final String mStaple;

    private Staple(String staple) {
        this.mStaple = staple;
    }

    @Override
    public Object getValue() {
        return mStaple;
    }

    @Override
    public Class<?> getCategory() {
        return getClass();
    }

    @Override
    public String getName() {
        return STAPLE;
    }


    private static volatile Map<String, Staple> directory = null;

    private static Map<String, Staple> getDirectory() {
        if(directory == null) {
            Map<String, Staple> d = new HashMap<String, Staple>();
            for(Staple staple : values()) {
                d.put(staple.getValue().toString(), staple);
            }
            directory = d;
        }
        return directory;
    }

    private static Staple fromString(String value) {
        return getDirectory().get(value);
    }

    public static List<Staple> getSupportedValue(List<String> values) {
        if( values == null ) {
            return Collections.emptyList();
        }

        List<Staple> list = new ArrayList<Staple>();
        for(String value : values) {
            Staple staple = fromString(value);
            if( staple != null ) {
                list.add(staple);
            }
        }

        return list;
    }
}

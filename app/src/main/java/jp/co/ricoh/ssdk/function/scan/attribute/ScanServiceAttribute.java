/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.scan.attribute;

import jp.co.ricoh.ssdk.function.attribute.Attribute;

/**
 * スキャンサービス属性のインタフェースです。
 * The interface of scan service attributes.
 */
public interface ScanServiceAttribute extends Attribute {

}

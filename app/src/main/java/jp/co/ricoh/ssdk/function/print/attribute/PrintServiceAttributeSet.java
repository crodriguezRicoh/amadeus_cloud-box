/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.attribute;

import jp.co.ricoh.ssdk.function.attribute.AttributeSet;

/**
 * The interface to indicate print service attribute set.
 */
public interface PrintServiceAttributeSet extends AttributeSet<PrintServiceAttribute> {

}

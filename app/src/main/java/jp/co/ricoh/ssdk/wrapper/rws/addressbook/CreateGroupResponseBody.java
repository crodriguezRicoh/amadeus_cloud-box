/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.wrapper.rws.addressbook;

import java.util.Map;

import jp.co.ricoh.ssdk.wrapper.common.ResponseBody;

public class CreateGroupResponseBody extends Group implements ResponseBody {

	CreateGroupResponseBody(Map<String, Object> value) {
		super(value);
	}

}

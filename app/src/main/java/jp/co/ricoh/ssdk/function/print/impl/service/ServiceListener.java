/*
 *  Copyright (C) 2013 RICOH Co.,LTD.
 *  All rights reserved.
 */
package jp.co.ricoh.ssdk.function.print.impl.service;

import jp.co.ricoh.ssdk.function.print.attribute.PrintServiceAttributeSet;

/**
 * The listener interface to notify the asynchronous service events from SDKService.
 */
public interface ServiceListener {

    void onChangePrintServiceAttributes(PrintServiceAttributeSet attributes);

}

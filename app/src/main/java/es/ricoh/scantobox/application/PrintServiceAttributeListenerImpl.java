/*
 *  Copyright (C) 2013-2016 RICOH Co.,LTD.
 *  All rights reserved.
 */
package es.ricoh.scantobox.application;

import android.os.Handler;

import es.ricoh.scantobox.activity.PrintActivity;
import es.ricoh.scantobox.activity.ScanActivity;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterOccuredErrorLevel;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterState;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterStateReasons;
import jp.co.ricoh.ssdk.function.print.event.PrintServiceAttributeEvent;
import jp.co.ricoh.ssdk.function.print.event.PrintServiceAttributeListener;

// TODO: Revisar

/**
 * The listener class to monitor scan service attribute changes.
 */
public class PrintServiceAttributeListenerImpl implements PrintServiceAttributeListener {

    /** Reference to MainActivity */
    PrintActivity mActivity;

    /** UI thread handler */
    Handler mHandler;

    /** Level of the currently occurring error */
    private PrinterOccuredErrorLevel mLastErrorLevel = null;

    /** Flag to indicate if system warning screen is displayed */
    private volatile  boolean mAlertDialogDisplayed = false;
    
    /**
     * Application type
     * Used for setting system warning dialog.
     */
    public final static String ALERT_DIALOG_APP_TYPE_PRINTER = "PRINTER";
    
    public PrinterOccuredErrorLevel getLastErrorLevel() {
        return mLastErrorLevel;
    }

    public void setLastErrorLevel(PrinterOccuredErrorLevel mLastErrorLevel) {
        this.mLastErrorLevel = mLastErrorLevel;
    }

    public boolean isAlertDialogDisplayed() {
        return mAlertDialogDisplayed;
    }

    public void setAlertDialogDisplayed(boolean mAlertDialogDisplayed) {
        this.mAlertDialogDisplayed = mAlertDialogDisplayed;
    }
    
    /** Event received from print service */
    PrintServiceAttributeEvent mEvent;

    public PrintServiceAttributeListenerImpl(PrintActivity activity, Handler handler){
        mActivity = activity;
        mHandler = handler;
    }

    /**
     * The method called when receive an event from the print service.
     *
     * @param event
     */
    @Override
    public void attributeUpdate(PrintServiceAttributeEvent event) {
        mEvent = event;
        ScanToBoxApplication mApplication = (ScanToBoxApplication) mActivity.getApplication();
        
        /**
         * Obtain error level and judge whether display or hide or update warning screen
         */
        PrinterState state = (PrinterState)event.getAttributes().get(PrinterState.class);
        PrinterStateReasons stateReasons = (PrinterStateReasons)event.getAttributes().get(PrinterStateReasons.class);
        PrinterOccuredErrorLevel errorLevel = (PrinterOccuredErrorLevel) event.getAttributes().get(PrinterOccuredErrorLevel.class);
      
        if (PrinterOccuredErrorLevel.ERROR.equals(errorLevel)
                || PrinterOccuredErrorLevel.FATAL_ERROR.equals(errorLevel)) {

            String stateString = mActivity.makeAlertStateString(state);
            String reasonString = mActivity.makeAlertStateReasonString(stateReasons);

            if (mLastErrorLevel == null) {
                // Normal -> Error
                if (mActivity.isForegroundApp(mActivity.getPackageName())) {
                    mApplication.displayAlertDialog(ALERT_DIALOG_APP_TYPE_PRINTER, stateString, reasonString);
                    mAlertDialogDisplayed = true;
                }
            } else {
                // Error -> Error
                if (mAlertDialogDisplayed) {
                    mApplication.updateAlertDialog(ALERT_DIALOG_APP_TYPE_PRINTER, stateString, reasonString);
                }
            }
            mLastErrorLevel = errorLevel;

        } else {
            if (mLastErrorLevel != null) {
                // Error -> Normal
                if (mAlertDialogDisplayed) {
                    String activityName = mActivity.getTopActivityClassName(mActivity.getPackageName());
                    if (activityName == null) {
                        activityName = ScanActivity.class.getName();
                    }
                    mApplication.hideAlertDialog(ALERT_DIALOG_APP_TYPE_PRINTER, activityName);
                    mAlertDialogDisplayed = false;
                }
            }
            mLastErrorLevel = null;
        }

    }
}
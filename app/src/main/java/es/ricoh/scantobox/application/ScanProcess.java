package es.ricoh.scantobox.application;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.TextView;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.activity.PreviewActivity;
import es.ricoh.scantobox.activity.ScanActivity;
import es.ricoh.scantobox.exception.PdfCreationException;
import es.ricoh.scantobox.imageprocessing.ImageEngine;
import es.ricoh.scantobox.imageprocessing.PdfEngine;
import es.ricoh.scantobox.imageprocessing.TiffEngine;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Constants;
import es.ricoh.scantobox.util.Dialogs.DialogUtil;
import es.ricoh.scantobox.util.Preferences;
import es.ricoh.scantobox.util.Utils;
import jp.co.ricoh.ssdk.function.scan.ScanImage;
import jp.co.ricoh.ssdk.wrapper.rws.addressbook.Entry;

/**
 * Created by cristian.rodriguez on 27/01/2017.
 * Checked: OK!
 */

public class ScanProcess {

    private static final Logger log = ALogger.getLogger(ScanProcess.class);
    public static Dialog mProgressDialog;
    private ScanToBoxApplication mApplication;
    private Context mContext;
    private ScanActivity activity = null;
    private int pageNo;

    private boolean proceso = false;
    private String msgError = "";

    static boolean isProcessStop = false;

    public static Entry result;
    static ProcesoFicherosTask processTask = null;

    public ScanProcess(ScanToBoxApplication application, Context context, int pages) {
        this.mApplication = application;
        this.mContext = context;
        this.pageNo = pages;
    }

    public void initProcess() {

        log.debug("***** ScanProcess - Ejecutando INIT PROCESS *****");
        processTask = new ProcesoFicherosTask();
        processTask.execute();
    }

    static public void stopProcess(){
        log.debug("***** ScanProcess - Parando STOP PROCESS *****");

        isProcessStop = true;

        if(processTask != null) {
            log.debug("Cancel EXECUTE");
            processTask.cancel(true);
        }
    }

    public void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel origen = null;
        FileChannel destino = null;
        try {
            origen = new FileInputStream(sourceFile).getChannel();
            destino = new FileOutputStream(destFile).getChannel();

            long count = 0;
            long size = origen.size();
            while ((count += destino.transferFrom(origen, count, size - count)) < size) ;
        } finally {
            if (origen != null) {
                origen.close();
            }
            if (destino != null) {
                destino.close();
            }
        }
    }

    private class ProcesoFicherosTask extends AsyncTask<Integer, Integer, Integer> {

        ArrayList<File> recognizedFiles = new ArrayList<File>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            proceso = false;
            isProcessStop = false;

            activity = (ScanActivity) mApplication.getStateMachine().getActivity();
            activity.runOnUiThread(new Runnable() {
                public void run() {

                    mProgressDialog = DialogUtil.createProcessWaitDialog(activity, R.layout.dlg_procesando);
                    DialogUtil.showDialog(mProgressDialog, 540, 290);
                }
            });

            //Lock APP
            Utils.Lock_logout(activity, mApplication.getPackageName());
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            try {

                String extension = ".jpg";

                Date init = new Date();

                log.debug("Recuperando SCAN JOB");
                ScanImage scanImage = new ScanImage(mApplication.getScanJob());

                int actualPage = 1;

                String rotateAngle = scanImage.getImageRotationAngle(1);
                log.debug("Detectando ANGULO de rotación: " + rotateAngle);

                ImageEngine iEngine = new ImageEngine();

                for(int i=1; i<=pageNo; i++) {

                    log.debug("Procesando hoja: " + i);

                    String origPathFile = scanImage.getImageFilePath(i);
                    while (origPathFile == null) {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        origPathFile = scanImage.getImageFilePath(i);
                    }

                    log.debug("Ruta fichero ORIGINAL: " + origPathFile);

                    if(!rotateAngle.equals("0")) {

                        log.debug("Esta hoja necesita rotar: " + rotateAngle + " grados.");
                        try {

                            publishProgress(1);
                            iEngine.processImages_rotateImage(new File(origPathFile), Integer.parseInt(rotateAngle));

                        }catch (Exception ex){
                            log.error("Error en la rotación: " + Utils.printStackTraceToString(ex));
                        }
                    }

                    //TODO Creamos una ruta de almacenamiento temporal
                    File scanDirectory = new File("/mnt/hdd/" + mApplication.getApplicationContext().getPackageName() + "/scans/");
                    if (!scanDirectory.exists()) {
                        scanDirectory.mkdir();
                    }

                    log.debug("Procesando hojas");

                    File scanTempFile = new File(origPathFile);

                    /*if(!Preferences.isColor) {
                        MainImageProcess mainImageProcess = new MainImageProcess();
                        mainImageProcess.imageToBW(scanTempFile);
                    }*/

                    //File scanFile = new File("/mnt/hdd/" + mApplication.getApplicationContext().getPackageName() + "/scans/" + Preferences.basename + "_00" + actualPage + extension);
                    File scanFile = new File("/mnt/hdd/" + mApplication.getApplicationContext().getPackageName() + "/scans/" + Preferences.basename + String.format("%03d", actualPage) + extension);

                    log.debug("Fichero agregado: " + scanFile.getAbsolutePath());
                    copyFile(scanTempFile, scanFile);
                    recognizedFiles.add(scanFile);
                    actualPage ++;
                }

                if(Preferences.fileType.equalsIgnoreCase(Constants.fileTypePDF)) {
                    //Create PDF
                    new PdfEngine(recognizedFiles);

                }else if(!Preferences.isColor){
                    //Create TIFF
                    new TiffEngine(recognizedFiles, Preferences.basename);
                }

                mApplication.getStateMachine().procScanEvent(es.ricoh.scantobox.application.ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_COMPLETED);

                proceso = true;

                Date end = new Date();
                long diff = end.getTime() - init.getTime();
                long diffSeconds = diff / 1000;
                log.debug("********* Tiempo de procesado: " + diffSeconds + " segundos *********");
                log.debug("The DPI is: " + Preferences.resolutionDPI);
                log.debug("The Color is: " + Preferences.isColor);

                if (proceso) {

//                    log.debug("Proceso correcto");
//                    Activity mActivity = mApplication.getStateMachine().getActivity();
//                    log.debug("Activity set: " + mActivity.getLocalClassName() + ", " + mActivity);
                    //Intent intent = new Intent(mActivity, PreviewActivity.class);


                    //ScanActivity.mContext.callPreviewActivity(recognizedFiles.size());

//                    Intent intent = new Intent(ScanActivity.mContext, PreviewActivity.class);
//                    log.debug("Create intent");
//                    intent.putExtra("pages", recognizedFiles.size());
//                    log.debug("Put extras");
//
//                    //TODO Aquí pasamos como parámetro las facturas separadas detectadas, para mostrar en el preview y luego realizar los PDF's correspondientes.
//                    //mActivity.startActivityForResult(intent, PreviewActivity.REQUEST_CODE_PREVIEW_ACTIVITY);
//                    ScanActivity.mContext.startActivityForResult(intent, PreviewActivity.REQUEST_CODE_PREVIEW_ACTIVITY);
//
//                    log.debug("START ACTIVITY FOR RESULT EXECUTED...");
                }

            } catch (PdfCreationException e) {
                proceso = false;
                log.error(Utils.printStackTraceToString(e));
                msgError = mContext.getString(R.string.generic_error_pdfcreate);

                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }

            } catch (Exception e) {
                proceso = false;
                log.error(Utils.printStackTraceToString(e));
                msgError = mContext.getString(R.string.unkonwn_error_processing);

                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
            }

            if(msgError == null || msgError.isEmpty()) {
                msgError = mContext.getString(R.string.unkonwn_error_processing);
            }

            return 0;
        }

        public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }

            log.debug("SampleSize: " + inSampleSize);

            return inSampleSize;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            if (values[0] == 1) {
                ((TextView) mProgressDialog.findViewById(R.id.info)).setText(mContext.getString(R.string.txid_cmn_b_rotando));
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            log.debug("***** OnPostExecute *****");

            //UnLock APP
            //Timer timer = new Timer();
            //timer.schedule(lockTimeOutTask, 30000);

            //Utils.Lock_logout(activity, mApplication.getPackageName());

            try {

                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }

                if (!isProcessStop) {

                    log.debug("ScanProcessStop = " + isProcessStop);

                    if (!proceso) {

                        log.debug("Proceso = " + proceso);

                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                mProgressDialog = es.ricoh.scantobox.util.Dialogs.DialogUtil.createErrorDialog(activity, msgError);
                                mProgressDialog.show();
                            }
                        });
                    } else {
                        log.debug("Lanzando PREVIEW...");
                        ScanActivity.mContext.callPreviewActivity(recognizedFiles.size());
                    }
                }else{

                    log.debug("ScanProcessStop = " + isProcessStop);
                }

            }catch (Exception ex){
                Utils.printStackTraceToString(ex);
            }finally {

                try{

                    Timer timer = new Timer();
                    timer.schedule(activity.lockTimeOutTask, 30000);

                }catch (Exception ex){
                    Utils.printStackTraceToString(ex);
                }
            }
        }
    }
}

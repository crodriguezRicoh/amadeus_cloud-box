/**
 * Copyright (C) 2016 Ricoh Spain IT Services.
 * All rights reserved.
 */
package es.ricoh.scantobox.application;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import org.apache.log4j.Logger;

import java.io.File;
import java.util.Map;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.activity.PrintActivity;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Dialogs.DialogUtil;
import es.ricoh.scantobox.util.Dialogs.InfoDialog;
import es.ricoh.scantobox.util.Dialogs.PrintDialog;
import es.ricoh.scantobox.util.Dialogs.WaitDialog;
import es.ricoh.scantobox.util.Utils;
import jp.co.ricoh.ssdk.function.application.AfterPowerModeLock;
import jp.co.ricoh.ssdk.function.print.PrintFile;
import jp.co.ricoh.ssdk.function.print.PrintJob;
import jp.co.ricoh.ssdk.function.print.attribute.PrintException;
import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttributeSet;
import jp.co.ricoh.ssdk.function.print.attribute.PrintResponseException;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintJobPrintingInfo;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterStateReason;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterStateReasons;

/**
 * Job state machine of print sample application.
 * Display/update/hide processes of the following dialog/screen are always executed by this statemachine.
 *   - Please wait dialog
 *   - Printing dialog
 */
public class PrintStateMachine {

    public static final String INTENT_ACTION_LOCK_SYSTEM_RESET = "jp.co.ricoh.isdk.sdkservice.system.SystemManager.LOCK_SYSTEM_RESET";
    public static final String INTENT_ACTION_UNLOCK_SYSTEM_RESET = "jp.co.ricoh.isdk.sdkservice.system.SystemManager.UNLOCK_SYSTEM_RESET";

    final static Logger log = ALogger.getLogger(PrintStateMachine.class);

    private static Handler mHandler;
    private static ScanToBoxApplication mApplication;
    private static Context mContext;
    private static PrintDialog mProgressDialog;
    private static WaitDialog mWaitDialog;
    private static InfoDialog mBootFailedDialog;
    private static PrintJob currentPrintJob;
    private static AfterPowerModeLock mAfterPowerModeLock;

    PrintStateMachine(ScanToBoxApplication application, Handler handler) {
        mApplication = application;
        mHandler = handler;
        mContext = null;
        mAfterPowerModeLock = null;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void removeContext() {
        mContext = null;
    }

    /**
     * State transition event.
     */
    public enum PrintEvent {
        CHANGE_APP_ACTIVITY_INITIAL,
        CHANGE_APP_ACTIVITY_STARTED,
        CHANGE_APP_ACTIVITY_START_FAILED,
        CHANGE_APP_ACTIVITY_DESTROYED,
        CHANGE_JOB_STATE_INITIAL,
        CHANGE_JOB_STATE_PRE_PROCESS,
        CHANGE_JOB_STATE_PRE_PENDING,
        CHANGE_JOB_STATE_PENDING,
        CHANGE_JOB_STATE_PROCESSING,
        CHANGE_JOB_STATE_PROCESSING_STOPPED,
        CHANGE_JOB_STATE_COMPLETED,
        CHANGE_JOB_STATE_ABORTED,
        CHANGE_JOB_STATE_CANCELED,
        REQUEST_JOB_CANCEL,
    }

    /**
     * Current state of the state machine
     */
    private State mState = State.STATE_APP_INITIAL;

    /**
     * State definition.
     */
    public enum State {
        /**
         * Initial state
         */
        STATE_APP_INITIAL {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch (event) {
                    case CHANGE_APP_ACTIVITY_INITIAL:
                        showPleaseWaitDialog(mContext);
                        return STATE_APP_INITIAL;
                    case CHANGE_APP_ACTIVITY_STARTED:
                        closePleaseWaitDialog();
                        return STATE_JOB_INITIAL;
                    case CHANGE_APP_ACTIVITY_START_FAILED:
                        closePleaseWaitDialog();
                        showBootFailedDialog();
                        return STATE_APP_INITIAL;
                    default:
                        return super.getNextState(event,param);
                }
            }
        },
        /**
         * Job initial state
         */
        STATE_JOB_INITIAL {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch(event) {
                    case CHANGE_JOB_STATE_PRE_PROCESS:
                        if(param == null) return STATE_JOB_INITIAL;
                        if(!(param instanceof PrintSettingDataHolder)) return STATE_JOB_INITIAL;

                        new StartPrintJobTask().execute((PrintSettingDataHolder) param);

                        return STATE_JOB_PRE_PROCESS;

                    case CHANGE_JOB_STATE_INITIAL:
                        return STATE_JOB_INITIAL;
                    default:
                        return super.getNextState(event, param);
                }
            }
        },

        /**
         * The state before the job is started after attribute set.
         */
        STATE_JOB_PRE_PROCESS {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch ( event) {
                    case CHANGE_JOB_STATE_PRE_PENDING :
                        return STATE_JOB_PRE_PENDING;

                    case CHANGE_JOB_STATE_INITIAL:
                        //failed to start job
                        closePrintingDialog(false);

                        return STATE_JOB_INITIAL;
                    default:
                        return super.getNextState(event, param);
                }
            }

            @Override
            public void entry(Object... params) {
                //show the print start dialog
                showPrintingDialog(mContext);
                mProgressDialog.setCancelable(false);
            }
        },

        /**
         * The state after the job is started.
         */
        STATE_JOB_PRE_PENDING {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch (event) {
                    case CHANGE_JOB_STATE_PENDING:
                        return STATE_JOB_PENDING;
                    case CHANGE_JOB_STATE_PROCESSING:
                        return STATE_JOB_PROCESSING;

                    case CHANGE_APP_ACTIVITY_DESTROYED:
                        return WAITING_JOB_CANCEL;
                    default:
                        return super.getNextState(event, param);
                }
            }

        },
        /**
         * Job pending
         */
        STATE_JOB_PENDING {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch (event) {
                    case CHANGE_JOB_STATE_PROCESSING:
                        return STATE_JOB_PROCESSING;
                    case REQUEST_JOB_CANCEL:
                        return WAITING_JOB_CANCEL;
                    case CHANGE_JOB_STATE_PROCESSING_STOPPED:
                        return STATE_JOB_PROCESSING_STOPPED;

                    case CHANGE_APP_ACTIVITY_DESTROYED:
                        return WAITING_JOB_CANCEL;
                    default:
                        return super.getNextState(event,param);
                }
            }
        },
        /**
         * Job processing
         */
        STATE_JOB_PROCESSING {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch (event) {
                    case CHANGE_JOB_STATE_ABORTED:
                        return STATE_JOB_ABORTED;

                    case CHANGE_JOB_STATE_CANCELED:
                        return STATE_JOB_CANCELED;

                    case CHANGE_JOB_STATE_COMPLETED:
                        return STATE_JOB_COMPLETED;

                    case CHANGE_JOB_STATE_PROCESSING:
                        return STATE_JOB_PROCESSING;

                    case REQUEST_JOB_CANCEL:
                        return WAITING_JOB_CANCEL;

                    case CHANGE_JOB_STATE_PROCESSING_STOPPED:
                        return STATE_JOB_PROCESSING_STOPPED;

                    case CHANGE_APP_ACTIVITY_DESTROYED:
                        return WAITING_JOB_CANCEL;

                    default:
                        return super.getNextState(event,param);
                }
            }

            @Override
            public void entry(Object... params) {
                //Show print start dialog
                if(params[0] instanceof PrintJobPrintingInfo) {
                    PrintJobPrintingInfo printingInfo = (PrintJobPrintingInfo) params[0];

                    if(printingInfo == null) return;
                    String printedMessage = String.format(mContext.getResources().getString(R.string.dlg_printing_message_printing));
                    updatePrintingDialog(mContext, printedMessage);
                    mProgressDialog.setCancelable(true);
                }
            }
        },
        /**
         * Job pausing
         */
        STATE_JOB_PROCESSING_STOPPED{
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch (event) {
                    case CHANGE_JOB_STATE_ABORTED:
                        return STATE_JOB_ABORTED;

                    case CHANGE_JOB_STATE_CANCELED:
                        return STATE_JOB_CANCELED;

                    case CHANGE_JOB_STATE_COMPLETED:
                        return STATE_JOB_COMPLETED;

                    case CHANGE_JOB_STATE_PROCESSING:
                        return STATE_JOB_PROCESSING;

                    case REQUEST_JOB_CANCEL:
                        return WAITING_JOB_CANCEL;
                    case CHANGE_APP_ACTIVITY_DESTROYED:
                        return WAITING_JOB_CANCEL;

                    default:
                        return super.getNextState(event,param);
                }
            }

            @Override
            public void entry(Object... params) {
                if(params[0] == null) return;

                PrinterStateReasons reasons = (PrinterStateReasons)params[0];
                StringBuilder sb = new StringBuilder();
                for(PrinterStateReason reason : reasons.getReasons()) {
                    sb.append(reason.toString());
                    sb.append("\n");
                }

                // Update job pausing dialog
                updatePrintingDialog(mContext,
                        mContext.getResources().getString(
                                R.string.dlg_printing_message_printing_stopped) + "\n" + sb.toString());

            }
        },

        /**
         * Job waiting to be canceled
         */
        WAITING_JOB_CANCEL {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch (event){
                    case CHANGE_JOB_STATE_INITIAL:
                        return STATE_JOB_INITIAL;
                    case CHANGE_JOB_STATE_CANCELED:
                        return STATE_JOB_CANCELED;
                    case CHANGE_JOB_STATE_ABORTED:
                        return STATE_JOB_ABORTED;
                    case CHANGE_JOB_STATE_PROCESSING:
                        return STATE_JOB_PROCESSING;
                    case CHANGE_JOB_STATE_COMPLETED:
                        return STATE_JOB_COMPLETED;
                    default:
                        return super.getNextState(event, param);
                }
            }

            @Override
            public void entry(Object... params) {
                new CancelPrintJobTask().execute();
            }

        },

        /**
         * Job completed
         */
        STATE_JOB_COMPLETED {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch (event) {
                    case CHANGE_JOB_STATE_INITIAL:
                        return STATE_JOB_INITIAL;

                    default:
                        return super.getNextState(event,param);
                }
            }

            @Override
            public void entry(Object... params) {
                unLockMode();
                //close printing dialog
                closePrintingDialog(true);
            }
        },
        /**
         * Job aborted
         */
        STATE_JOB_ABORTED {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch (event) {
                    case CHANGE_JOB_STATE_INITIAL:
                        return STATE_JOB_INITIAL;

                    default:
                        return super.getNextState(event,param);
                }
            }

            @Override
            public void entry(Object... params) {
                unLockMode();
                //close printing dialog
                closePrintingDialog(false);

                // show toast message with aborted reason
                String message = "job aborted.";
                if ((params != null) && (params.length > 0)) {
                    if (params[0] instanceof PrinterStateReasons) {
                        PrinterStateReasons reasons = (PrinterStateReasons)params[0];
                        StringBuilder sb = new StringBuilder();
                        sb.append(message);
                        sb.append(System.getProperty("line.separator"));
                        sb.append(reasons.getReasons().toString());
                        message = sb.toString();
                    }
                }
                showToastMessage(mContext, message);
            }
        },
        /**
         * Job canceled
         */
        STATE_JOB_CANCELED {
            @Override
            public State getNextState(PrintEvent event, Object param) {
                switch (event) {
                    case CHANGE_JOB_STATE_INITIAL:
                        return STATE_JOB_INITIAL;

                    default:
                        return super.getNextState(event,param);
                }
            }

            @Override
            public void entry(Object... params) {
                unLockMode();
                //close printing dialog
                closePrintingDialog(false);
            }
        } ;

        /******************************************************************
         * State change
         *
         * Common methods.
         * These methods are called in the following order.
         * ・getNextState()
         * ・exit()
         * ・entry()
         ******************************************************************/

        /**
         * Obtains the next methods.
         */
        public State getNextState(final PrintEvent event, final Object param) {
            switch (event){
                default:
                    return null;
            }
        }

        /**
         * Entry method
         */
        public void entry(final Object... params) {

        }

        /**
         * Exit method
         */
        public void exit(final Object... params) {

        }
    }

    private static void unLockMode() {
        if (mAfterPowerModeLock != null) {
            mAfterPowerModeLock.finish();
            mAfterPowerModeLock = null;
        }

        mApplication.unlockPowerMode();
        mApplication.unlockOffline();
        
        Intent intent = new Intent(INTENT_ACTION_UNLOCK_SYSTEM_RESET);
        intent.setPackage(mApplication.getPackageName());
        mApplication.sendBroadcast(intent);
    }

    /**
     * Changes states.
     */
    public void procPrintEvent(final PrintEvent event) {
        procPrintEvent(event, null);
    }

    /**
     * Changes states.
     */
    public void procPrintEvent(final PrintEvent event, final Object prm) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                State newState = mState.getNextState(event, prm);
                if (newState != null) {
//                    log.info("#evtp :" + event + " state:" + mState + " > " + newState);
                    mState.exit(prm);
                    mState = newState;
                    mState.entry(prm);
                }
            }
        });
    }


    /******************************************************************
     * Action method.
     ******************************************************************/

    /**
     * Displays please wait dialog
     */
    private static void showPleaseWaitDialog(Context context) {
        mWaitDialog = new WaitDialog(context);
        mWaitDialog.show();
        mWaitDialog.setMessage(context.getResources().getString(R.string.dlg_waiting_message));

    }

    /**
     * Hides please wait dialog
     */
    private static void closePleaseWaitDialog() {
        if (mWaitDialog == null)
            return;
        mWaitDialog.dismiss();
        mWaitDialog = null;
    }

    /**
     * Displays boot failed dialog
     */
    private static void showBootFailedDialog() {
        if(mBootFailedDialog==null || mBootFailedDialog.isShowing()==false) {
            mBootFailedDialog = new InfoDialog(mContext);
            mBootFailedDialog.show();
            mBootFailedDialog.viewInfoIcon(true);
            mBootFailedDialog.setMessage(mContext.getResources().getString(R.string.error_cannot_connect));
            mBootFailedDialog.setButtonLabel(mContext.getResources().getString(R.string.txid_cmn_b_close));
            mBootFailedDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Activity)mContext).finish();
                }
            });
        }

    }

    /**
     * Displays scanning dialog.
     * @param context Context of MainActivity
     */
    private static void showPrintingDialog(Context context) {
        mProgressDialog = new PrintDialog(context);
        mProgressDialog.show();
        mProgressDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mApplication.setmIsWinShowing(false);
                mApplication.getPrintStateMachine().procPrintEvent(PrintEvent.REQUEST_JOB_CANCEL);
            }
        });

    }

    /**
     * Updates the printing dialog.
     *
     * @param context Context of MainActivity
     * @param updateMessage String to display on the dialog
     */
    private static void updatePrintingDialog(Context context, String updateMessage) {
        if(mProgressDialog == null)
            return;
        mProgressDialog.setMessage(updateMessage);
    }

    /**
     * Close the printing dialog,
     */
    private static void closePrintingDialog(Boolean result) {

        mApplication.setmIsWinShowing(false);

        if (mProgressDialog == null){
            return;
        }

        try {
            mProgressDialog.dismiss();
            mProgressDialog = null;

            PrintActivity.showMSG_Print_Process(result);


        }catch (Exception ex){
            log.error("ATENCIÓN: No se puede cerrar el popUp de impresión, porque el proceso ha sido cancelado....");
        }

        mApplication.clearPrintFileNames();

//        String msgSuccess = mContext.getResources().getString(R.string.text_success_print);
//        String msgError = mContext.getResources().getString(R.string.text_error_print_process);
//
//        if(result) {
//            Dialog dialog = DialogUtil.createSuccessDialog(mContext, msgSuccess);
//            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface paramDialogInterface) {
//
//                }
//            });
//            dialog.show();
//        }else{
//            Dialog dialog = DialogUtil.createErrorDialog(mContext, msgError);
//            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface paramDialogInterface) {
//
//                }
//            });
//            dialog.show();
//        }


    }

    private static void deleteRecursive(File f) {
        if (f.isDirectory())
            for (File child : f.listFiles())
                deleteRecursive(child);

        f.delete();
    }

    /**
     * Display the toast message
     *
     * @param context Context of MainActivity
     * @param message String to display in the toast
     */
    private static void showToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /******************************************************************
     * Asynchronous task
     ******************************************************************/

    /**
     * Creates the string of the ScanResponseException error information.
     * The format is as below.
     *
     * base[separator]
     * [separator]
     * message_id: message[separator]
     * message_id: message[separator]
     * message_id: message
     *
     * @param e ScanResponseException to be converted as a string
     * @param base Starting string of the message
     * @return Message string
     */
    private String makeJobErrorResponseMessage(PrintResponseException e, String base) {
        StringBuilder sb = new StringBuilder(base);
        if (e.hasErrors()) {
            Map<String, String> errors = e.getErrors();
            if (errors.size() > 0) {
                String separator = System.getProperty("line.separator");
                sb.append(separator);

                for (Map.Entry<String, String> entry : errors.entrySet()) {
                    sb.append(separator);
                    // message_id
                    sb.append(entry.getKey());
                    // message (exists only)
                    String message = entry.getValue();
                    if ((message != null) && (message.length() > 0)) {
                        sb.append(": ");
                        sb.append(message);
                    }
                }
            }
        }
        return sb.toString();
    }

    /**
     * The asynchronous task to start the print job.
     */
    static class StartPrintJobTask extends AsyncTask<PrintSettingDataHolder, Void, Boolean> {

        private String message = null;

        @Override
        protected Boolean doInBackground(PrintSettingDataHolder... holders) {
            final Logger log = ALogger.getLogger(PrintStateMachine.class);

            if (!mApplication.lockPowerMode()) {
                log.warn("lockPowerMode failed. start after lock");
                mAfterPowerModeLock = new AfterPowerModeLock(mApplication);
                mAfterPowerModeLock.start();
            }

            if (!mApplication.lockOffline()) {
                mHandler.post(new Runnable() {
                     @Override
                     public void run() {
                         log.error("Error: cannot start print job. lockOffline() failed.");
                         Toast.makeText(mContext, R.string.error_cannot_connect, Toast.LENGTH_SHORT).show();
                     }
                });
                return false;
            }

            Intent intent = new Intent(INTENT_ACTION_LOCK_SYSTEM_RESET);
            intent.setPackage(mApplication.getPackageName());
            mApplication.sendBroadcast(intent);
            
            PrintFile printFile;
            try {
                printFile = holders[0].getPrintFile();
            } catch (PrintException e) {
                log.warn(e.toString());
                message = "Print Job Failed. " + e.getMessage();
                if (e instanceof PrintResponseException) {
                    message = mApplication.getPrintStateMachine().makeJobErrorResponseMessage((PrintResponseException)e, message);
                }
                return false;
            }

            PrintRequestAttributeSet attributeSet = holders[0].getPrintRequestAttributeSet();
            if(printFile == null || attributeSet == null) {
                return false;
            }

            try {
                currentPrintJob = mApplication.getPrintJob();
                currentPrintJob.print(printFile, attributeSet);
                return true;
            } catch (PrintException e) {
                log.warn(e.toString());
                message = "Print Job Failed. " + e.getMessage();
                if (e instanceof PrintResponseException) {
                    message = mApplication.getPrintStateMachine().makeJobErrorResponseMessage((PrintResponseException)e, message);
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(!result) {
                Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                mApplication.getPrintStateMachine().procPrintEvent(PrintEvent.CHANGE_JOB_STATE_INITIAL);
                unLockMode();
                return;
            }

            mApplication.getPrintStateMachine().procPrintEvent(
                    PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_PRE_PENDING);
        }
    }

    /**
     * The task to cancel the job.
     */
    static class CancelPrintJobTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            Logger log = ALogger.getLogger(PrintStateMachine.class);

            boolean result = false;

            if(currentPrintJob == null) return false;

            try {
                result = currentPrintJob.cancelPrintJob();
            } catch (PrintException ex) {
                log.error(Utils.printStackTraceToString(ex));
            }
            return result;
        }
    }

}

/*
 *  Copyright (C) 2013-2016 RICOH Co.,LTD.
 *  All rights reserved.
 */
package es.ricoh.scantobox.application;

import android.app.Activity;
import android.os.Handler;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.activity.PreviewActivity;
import es.ricoh.scantobox.activity.ScanActivity;
import es.ricoh.scantobox.util.ALogger;
import jp.co.ricoh.ssdk.function.Const;
import jp.co.ricoh.ssdk.function.application.DestinationSettingDataHolder;
import jp.co.ricoh.ssdk.function.application.ScanSettingDataHolder;
import jp.co.ricoh.ssdk.function.application.StorageSettingDataHolder;
import jp.co.ricoh.ssdk.function.application.SystemStateMonitor;
import jp.co.ricoh.ssdk.function.common.SmartSDKApplication;
import jp.co.ricoh.ssdk.function.print.PrintJob;
import jp.co.ricoh.ssdk.function.print.PrintService;
import jp.co.ricoh.ssdk.function.print.attribute.PrintJobAttributeSet;
import jp.co.ricoh.ssdk.function.print.attribute.PrintServiceAttributeSet;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintJobPrintingInfo;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintJobState;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterStateReasons;
import jp.co.ricoh.ssdk.function.print.event.PrintJobAttributeEvent;
import jp.co.ricoh.ssdk.function.print.event.PrintJobAttributeListener;
import jp.co.ricoh.ssdk.function.scan.ScanJob;
import jp.co.ricoh.ssdk.function.scan.ScanService;
import jp.co.ricoh.ssdk.function.scan.attribute.ScanJobAttributeSet;
import jp.co.ricoh.ssdk.function.scan.attribute.standard.ScanJobScanningInfo;
import jp.co.ricoh.ssdk.function.scan.attribute.standard.ScanJobSendingInfo;
import jp.co.ricoh.ssdk.function.scan.attribute.standard.ScanJobState;
import jp.co.ricoh.ssdk.function.scan.attribute.standard.ScanJobStateReason;
import jp.co.ricoh.ssdk.function.scan.attribute.standard.ScanJobStateReasons;
import jp.co.ricoh.ssdk.function.scan.event.ScanJobAttributeEvent;
import jp.co.ricoh.ssdk.function.scan.event.ScanJobAttributeListener;
import jp.co.ricoh.ssdk.function.scan.event.ScanJobEvent;
import jp.co.ricoh.ssdk.function.scan.event.ScanJobListener;
import jp.co.ricoh.ssdk.wrapper.common.Utils;


/**
 * スキャンサンプルアプリのアプリケーションクラスです。
 * 設定情報を保持し、スキャンサービスとジョブの管理を行います。
 * Application class of Scan sample application.
 * Saves the setting information and manages scan service and job.
 */
public class ScanToBoxApplication extends SmartSDKApplication {

    public static boolean processed = false;

    private ScanStateMachine mScanStateMachine;

    /** Reference to Main activity */
    private static Activity mActivity;

    /**
     * 宛先設定
     * Destination setting
     */
	private DestinationSettingDataHolder mDestinationSettingDataHolder;

	/**
	 * スキャン設定
	 * Scan setting
	 */
	private ScanSettingDataHolder mScanSettingDataHolder;

	/**
	 * 蓄積ファイル設定
	 * Storage file setting
	 */
	private StorageSettingDataHolder mStorageSettingDataHolder;

	/**
	 * スキャンサービス
	 * Scan service
	 */
	private ScanService mScanService;

	/**
	 * スキャンジョブ
	 * Scan job
	 */
	private ScanJob mScanJob;

	/**
	 * スキャンジョブ状態変化監視リスナー
	 * Scan job listener
	 */
	private ScanJobListener mScanJobListener;

	/**
	 * スキャンジョブ属性変化監視リスナー
	 * Scan job attribute listener
	 */
	private ScanJobAttributeListener mScanJobAttrListener;

	/**
	 * ステートマシン
	 * Statemachine
	 */
	private ScanStateMachine mStateMachine;

    /**
     * システム状態監視
     * System state monitor
     */
    private SystemStateMonitor mSystemStateMonitor;

    /**
     * 読み取ったページ数
     * Number of pages scanned
     */
    protected int scannedPages;

    /**
     * Dialogの最前面表示状態を保持します。
     * Dialog showing flag
     */
    private boolean mIsWinShowing = false;

    /**
     * Buttonの状態を保持します。
     * Button click flag
     */
    private boolean mIsDialogBtnClicked = false;

    private PrintService mPrintService;
    private PrintJob mPrintJob;
    private PrintJobAttributeListener mJobAttributeListener;

    private PrintStateMachine mPrintStateMachine;

    private Set<String> mPrintFileNames;
    private Iterator<String> mPrintFileNamesIterator;

    /** Supported print properties */
    private PrintSettingSupportedHolder mSettingSupportedHolder;

    /** Selected print properties */
    private PrintSettingDataHolder mSettingDataHolder;

    public boolean ismIsDialogBtnClicked() {
        return mIsDialogBtnClicked;
    }

    public void setmIsDialogBtnClicked(boolean mIsDialogBtnClicked) {
        this.mIsDialogBtnClicked = mIsDialogBtnClicked;
    }

    public boolean ismIsWinShowing() {
        return mIsWinShowing;
    }

    public void setmIsWinShowing(boolean mIsWinShowing) {
        this.mIsWinShowing = mIsWinShowing;
    }

    /**
     * 次原稿受付までの最大待ち時間です。
     * 0を指定した場合は、待ち続けます。
     * Maximum waiting time for accept the next page.
     * This timeout value supports "0" which means "keep waiting forever".
     */
    protected int timeOfWaitingNextOriginal = 0;

	@Override
	public void onCreate() {

	    //Register the log tag of ScanSample module which is used in function/wrapper layer
	    System.setProperty("jp.co.ricoh.ssdk.sample.log.TAG", Const.TAG);
	    setTagName();
	    Utils.setTagName();

		super.onCreate();
		mSystemStateMonitor = new SystemStateMonitor(this);
		mSystemStateMonitor.start();
		init();

        mPrintStateMachine = new PrintStateMachine(this, new Handler());
        mPrintService = PrintService.getService();
        mPrintFileNames = new HashSet<>();
	}

	@Override
    public void onTerminate() {
        mSystemStateMonitor.stop();
	    super.onTerminate();
	    mDestinationSettingDataHolder = null;
	    mScanSettingDataHolder = null;
	    mStorageSettingDataHolder = null;
	    mScanService = null;
	    mScanJob = null;
	    mScanJobListener = null;
	    mScanJobAttrListener = null;
	    mStateMachine = null;
	    mSystemStateMonitor = null;
	}

    /**************Print methods***********************/

    /***********************************************************
     * Public methods
     ***********************************************************/

    /**
     * Obtains the print service
     */
    public PrintService getPrintService() {
        return mPrintService;
    }

    /**
     * Sets the print supported setting object.
     * @param holder
     */
    public void setPrintSettingSupportedHolder(PrintSettingSupportedHolder holder) {
        mSettingSupportedHolder = holder;
    }

    /**
     * Sets the print settings object
     * @param holder
     */
    public void setPrintSettingDataHolder(PrintSettingDataHolder holder) {
        mSettingDataHolder = holder;
    }

    /**
     * Start print.
     */
    public void startPrint() {
        if(mSettingDataHolder == null) return;
        if(mPrintFileNames.isEmpty()) return;

        mPrintFileNamesIterator = mPrintFileNames.iterator();
        mSettingDataHolder.setSelectedPrintFileName(mPrintFileNamesIterator.next());
        Logger log = ALogger.getLogger(PrintStateMachine.class);
        log.info("Printing file " + mSettingDataHolder.getSelectedPrintFileName());
        mPrintStateMachine.procPrintEvent(PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_INITIAL, mSettingDataHolder);
        mPrintStateMachine.procPrintEvent(PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_PRE_PROCESS, mSettingDataHolder);
    }

    public void addPrintFileName(String fileName) {
        mPrintFileNames.add(fileName);
    }

    public void removePrintFileName(String fileName) {
        mPrintFileNames.remove(fileName);
    }

    public void clearPrintFileNames() {
        mPrintFileNames.clear();
    }

    /****************************************************************
     * Private methods
     ****************************************************************/

    /**
     * Initializes the print job.
     */
    private void initialJob() {
        if( mPrintJob != null) {
            if(mJobAttributeListener != null) {
                mPrintJob.removePrintJobAttributeListener(mJobAttributeListener);
            }
        }

        mPrintJob = new PrintJob();
        mJobAttributeListener = new PrintJobAttributeListenerImpl();

        mPrintJob.addPrintJobAttributeListener(mJobAttributeListener);

    }

    /**
     * Obtains the print job.
     */
    PrintJob getPrintJob() {
        initialJob();
        return mPrintJob;
    }

    /****************************************************************
     * Setter/Getter method
     ****************************************************************/

    /**
     * Obtains the statemachine.
     */
    public PrintStateMachine getPrintStateMachine() {
        return this.mPrintStateMachine;
    }

    /**
     * Obtains the print setting supported data.
     */
    public PrintSettingSupportedHolder getSettingSupportedDataHolders() {
        return mSettingSupportedHolder;
    }

    /**
     * Obtains the print settings data
     */
    public PrintSettingDataHolder getSettingDataHolder() {
        return mSettingDataHolder;
    }

    /**
     * The listener class to monitor scan job state changes.
     */
    class PrintJobAttributeListenerImpl implements PrintJobAttributeListener {
        /**
         * Called when the job state changes.
         * Receives the job state event and posts the appropriate event to the statemachine.
         */
        @Override
        public void updateAttributes(PrintJobAttributeEvent attributesEvent) {
            PrintJobAttributeSet attributeSet = attributesEvent.getUpdateAttributes();

            PrintJobState jobState = (PrintJobState)attributeSet.get(PrintJobState.class);
//            log.info("JobState[" + jobState + "]");
            if(jobState == null) return;

            switch (jobState) {
                case PENDING:
                    mPrintStateMachine.procPrintEvent(
                            PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_PENDING, null);
                    break;
                case PROCESSING:
                    PrintJobPrintingInfo printingInfo = (PrintJobPrintingInfo)attributeSet.get(
                            PrintJobPrintingInfo.class);

                    mPrintStateMachine.procPrintEvent(
                            PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_PROCESSING, printingInfo);
                    break;
                case PROCESSING_STOPPED:
                    PrintServiceAttributeSet serviceAttributeSet = mPrintService.getAttributes();
                    PrinterStateReasons reasons = (PrinterStateReasons) serviceAttributeSet.get((PrinterStateReasons.class));

                    mPrintStateMachine.procPrintEvent(
                            PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_PROCESSING_STOPPED, reasons);
                    break;
                case ABORTED:
                    serviceAttributeSet = mPrintService.getAttributes();
                    reasons = (PrinterStateReasons) serviceAttributeSet.get((PrinterStateReasons.class));

                    mPrintStateMachine.procPrintEvent(
                            PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_ABORTED, reasons);
                    break;
                case CANCELED:
                    mPrintStateMachine.procPrintEvent(
                            PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_CANCELED, null);
                    break;
                case COMPLETED:
                    mPrintStateMachine.procPrintEvent(
                            PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_COMPLETED, null);

                    if (mPrintFileNamesIterator.hasNext()) {
                        mSettingDataHolder.setSelectedPrintFileName(mPrintFileNamesIterator.next());
                        Logger log = ALogger.getLogger(PrintStateMachine.class);
                        log.info("Printing file " + mSettingDataHolder.getSelectedPrintFileName());
                        mPrintStateMachine.procPrintEvent(PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_INITIAL, mSettingDataHolder);
                        mPrintStateMachine.procPrintEvent(PrintStateMachine.PrintEvent.CHANGE_JOB_STATE_PRE_PROCESS, mSettingDataHolder);
                    }

                    break;
                default:
                    break;
            }
        }
    }


    /******Scan methods ********/

	/**
	 * リソースを初期化します。
	 * Initializes the resources.
	 */
	public void init() {
        mDestinationSettingDataHolder = new DestinationSettingDataHolder();
        mScanSettingDataHolder = new ScanSettingDataHolder();
        mStorageSettingDataHolder = new StorageSettingDataHolder();
        mStateMachine = new ScanStateMachine(this, new Handler());
        mScanService = ScanService.getService();
        initJobSetting();
	}

	/**
	 * スキャンジョブを初期化します。
	 * Initializes the scan job.
	 */
	public void initJobSetting() {
	    //If a state change listener is registered to the current scan job, the listener is removed.
	    if(mScanJob!=null) {
    	    if(mScanJobListener!=null) {
    	        mScanJob.removeScanJobListener(mScanJobListener);
    	    }
    	    if(mScanJobAttrListener!=null) {
    	        mScanJob.removeScanJobAttributeListener(mScanJobAttrListener);
    	    }
	    }

        mScanJob = new ScanJob();
        mScanJobListener = new ScanJobListenerImpl();
        mScanJobAttrListener = new ScanJobAttributeListenerImpl();

        //Registers a new listener to the new scan job.
        mScanJob.addScanJobListener(mScanJobListener);
        mScanJob.addScanJobAttributeListener(mScanJobAttrListener);

	}

    /**
     * スキャンジョブの属性変化監視リスナー実装クラスです。
     * [処理内容]
     *    (1)読み取り情報があれば、ステートマシンに通知します。
     *    (2)送信情報があれば、ステートマシンに通知します。
     *
     * The class to implement the listener to monitor scan job state changes.
     * [Processes]
     *    (1) If scan information exists, the information is notified to the state machine.
     *    (2) If data transfer information exists, the information is notified to the state machine.
     */
    class ScanJobAttributeListenerImpl implements ScanJobAttributeListener {

        @Override
        public void updateAttributes(ScanJobAttributeEvent attributesEvent) {
            ScanJobAttributeSet attributes = attributesEvent.getUpdateAttributes();
            mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.UPDATE_JOB_STATE_PROCESSING, attributes);

            //(1)
            ScanJobScanningInfo scanningInfo = (ScanJobScanningInfo) attributes.get(ScanJobScanningInfo.class);
            if (scanningInfo != null && scanningInfo.getScanningState()== ScanJobState.PROCESSING) {
                String status = getString(R.string.txid_scan_d_scanning) + " "
                        + String.format(getString(R.string.txid_scan_d_count), scanningInfo.getScannedCount());
                scannedPages = Integer.valueOf(scanningInfo.getScannedCount());
                mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.UPDATE_JOB_STATE_PROCESSING, status);
            }
            if (scanningInfo != null && scanningInfo.getScanningState() == ScanJobState.PROCESSING_STOPPED) {
                timeOfWaitingNextOriginal = Integer.valueOf(scanningInfo.getRemainingTimeOfWaitingNextOriginal());

            }

            //(2)
            ScanJobSendingInfo sendingInfo = (ScanJobSendingInfo) attributes.get(ScanJobSendingInfo.class);
            if (sendingInfo != null && sendingInfo.getSendingState()==ScanJobState.PROCESSING) {
                String status = getString(R.string.txid_scan_d_sending);
                mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.UPDATE_JOB_STATE_PROCESSING, status);
            }
        }
    }

    /**
     * スキャンジョブの状態監視リスナーです。
     * The listener to monitor scan job state changes.
     */
    class ScanJobListenerImpl  implements ScanJobListener {

		@Override
		public void jobCanceled(ScanJobEvent event) {
		    // set application state to normal
		    setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);

		    mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_CANCELED);
		}

		@Override
		public void jobCompleted(ScanJobEvent event) {
		    // set application state to normal
            setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);

            ScanProcess scn = new ScanProcess(ScanActivity.getmApplication(),ScanActivity.getAppContext(), scannedPages);
            scn.initProcess();

            //mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_COMPLETED);
		}

		@Override
		public void jobAborted(ScanJobEvent event) {

            ScanJobAttributeSet attributes = event.getAttributeSet();
            ScanJobStateReasons reasons = (ScanJobStateReasons) attributes.get(ScanJobStateReasons.class);
            ScanJobStateReason jobStateReason = null;

            boolean isError = false;
            if (reasons != null) {
                Set<ScanJobStateReason> reasonSet = reasons.getReasons();
                Iterator<ScanJobStateReason> iter = reasonSet.iterator();
                while(iter.hasNext()){
                    jobStateReason = iter.next();
                    switch(jobStateReason){
                        case MEMORY_OVER:
                        case EXCEEDED_MAX_EMAIL_SIZE:
                        case EXCEEDED_MAX_PAGE_COUNT:
                        case RESOURCES_ARE_NOT_READY:
                        case BROADCAST_NUMBER_OVER:
                        case BLANK_PAGES_ONLY:
                        case CERTIFICATE_INVALID:
                        case JOB_FULL:
                        case CHARGE_UNIT_LIMIT:
                            isError = true;
                            break;
                        default :
                            isError = false;
                            break;
                    }
                    if(isError){
                        break;
                    }
                }
            }
            if(isError){
                // set application state to error
                setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_ERROR, jobStateReason.toString(), SmartSDKApplication.APP_TYPE_SCANNER);
            }
            else{
                // set application state to normal
                setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);
            }

			mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_ABORTED, attributes);
		}

		@Override
		public void jobProcessing(ScanJobEvent event) {
		    // set application state to normal
            setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);

            ScanJobAttributeSet attributes = event.getAttributeSet();
			mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_PROCESSING, attributes);
		}

		@Override
		public void jobPending(ScanJobEvent event) {

            ScanJobAttributeSet attributes = event.getAttributeSet();
            ScanJobStateReasons reasons = (ScanJobStateReasons) attributes.get(ScanJobStateReasons.class);
            ScanJobStateReason jobStateReason = null;

            boolean isError = false;
            if (reasons != null) {
                Set<ScanJobStateReason> reasonSet = reasons.getReasons();
                Iterator<ScanJobStateReason> iter = reasonSet.iterator();
                while(iter.hasNext()){
                    jobStateReason = iter.next();
                    switch(jobStateReason){
                        case RESOURCES_ARE_NOT_READY:
                            isError = true;
                            break;
                        default :
                            isError = false;
                            break;
                    }
                    if(isError){
                        break;
                    }
                }
            }
            if(isError){
                // set application state to error
                setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_ERROR, jobStateReason.toString(), SmartSDKApplication.APP_TYPE_SCANNER);
            }
            else{
                // set application state to normal
                setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);
            }

            mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_PENDING);
		}

		/**
		 * Called when the job is in paused state.
		 * If multiple reasons exist, only the first reason is checked.
		 * [Processes]
		 *   (1) For the pause event for waiting for the next document when using exposure glass
		 *        - Sends the document waiting event to the state machine.
		 *   (2) For the pause event for preview display
		 *        - Sends the preview display event to the state machine.
		 *   (3) For the pause event for other reasons
		 *        - The job is cancelled in this case.
		 */
        @Override
        public void jobProcessingStop(ScanJobEvent event) {
            ScanJobAttributeSet attributes = event.getAttributeSet();

            ScanJobStateReasons reasons = (ScanJobStateReasons) attributes.get(ScanJobStateReasons.class);
            if (reasons != null) {
                Set<ScanJobStateReason> reasonSet = reasons.getReasons();

                boolean isError = false;
                Iterator<ScanJobStateReason> iter = reasonSet.iterator();
                ScanJobStateReason jobStateReason = null;
                while(iter.hasNext()){
                    jobStateReason = iter.next();
                    switch(jobStateReason){
                        case SCANNER_JAM:
                        case MEMORY_OVER:
                        case EXCEEDED_MAX_EMAIL_SIZE:
                        case EXCEEDED_MAX_PAGE_COUNT:
                            isError = true;
                            break;
                        default :
                            isError = false;
                            break;
                    }
                    if(isError){
                        break;
                    }
                }
                if(isError){
                    // set application state to error
                    setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_ERROR, jobStateReason.toString(), SmartSDKApplication.APP_TYPE_SCANNER);
                }
                else{
                    // set application state to normal
                    if(reasonSet.contains(ScanJobStateReason.WAIT_FOR_ORIGINAL_PREVIEW_OPERATION)) {
                        setAppState(PreviewActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);
                    }
                    else{
                        setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);
                    }
                }

                // show Preview window
                if (reasonSet.contains(ScanJobStateReason.WAIT_FOR_ORIGINAL_PREVIEW_OPERATION)) {
                    mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_STOPPED_PREVIEW);
                    return;
                }

                // show CountDown window
                if (reasonSet.contains(ScanJobStateReason.WAIT_FOR_NEXT_ORIGINAL_AND_CONTINUE)) {
                    mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_STOPPED_COUNTDOWN);
                    return;
                }

                // show Other reason job stopped window
                StringBuilder sb = new StringBuilder();
                for (ScanJobStateReason reason : reasonSet) {
                    sb.append(reason.toString()).append("\n");
                }

                mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_STOPPED_OTHER, sb.toString());
                return;
            }

            // set application state to normal
            setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);

            // Unknown job stop reason
            mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.CHANGE_JOB_STATE_STOPPED_OTHER, "(unknown reason)");
        }
    }

    /**
     * スキャンサービスを取得します。
     * obtains the scan service
     */
    public ScanService getScanService() {
        return mScanService;
    }

    /**
     * スキャンジョブを取得します。
     * Obtains the scan job.
     */
    public ScanJob getScanJob() {
        return mScanJob;
    }

	/**
	 * 宛先のデータ保持クラスのインスタンスを取得します。
	 * Obtains the instance for the class to save destination data.
	 */
	public DestinationSettingDataHolder getDestinationSettingDataHolder() {
		return mDestinationSettingDataHolder;
	}

    /**
     * スキャンジョブの設定データ保持クラスのインスタンスを取得します。
     * Obtains the instance for the class to save scan setting data.
     */
	public ScanSettingDataHolder getScanSettingDataHolder() {
	    return mScanSettingDataHolder;
	}

	/**
	 * 蓄積ファイル設定データ保持クラスのインスタンスを取得します。
	 * Obtains the instance for the class to save storage setting.
	 */
	public StorageSettingDataHolder getStorageSettingDataHolder() {
	    return mStorageSettingDataHolder;
	}

	/**
	 * このアプリのステートマシンを取得します。
	 * Obtains the statemachine.
	 */
	public ScanStateMachine getStateMachine() {
		return mStateMachine;
	}

    /**
     * システム状態監視クラスのインスタンスを取得します。
     * Obtains the instance for the class to system state monitor.
     */
    public SystemStateMonitor getSystemStateMonitor() {
        return mSystemStateMonitor;
    }

    /**
     * スキャンページ総数を取得します。
     * Obtains the number of total pages scanned.
     */
	public int getScannedPages() {
	    return scannedPages;
	}

	/**
	 * 次原稿までの最大待ち時間を取得します。
	 * Obtains the maximum waiting time.
	 * @return
	 */
	public int getTimeOfWaitingNextOriginal() {
        return timeOfWaitingNextOriginal;
    }

}

/**
 * Copyright (C) 2016 Ricoh Spain IT Services.
 * All rights reserved.
 */
package es.ricoh.scantobox.application;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import es.ricoh.scantobox.util.ALogger;
import jp.co.ricoh.ssdk.function.print.PrintFile;
import jp.co.ricoh.ssdk.function.print.PrintFile.PDL;
import jp.co.ricoh.ssdk.function.print.attribute.HashPrintRequestAttributeSet;
import jp.co.ricoh.ssdk.function.print.attribute.PrintException;
import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttributeSet;
import jp.co.ricoh.ssdk.function.print.attribute.standard.Copies;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PaperTray;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintColor;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintResolution;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintSide;
import jp.co.ricoh.ssdk.function.print.attribute.standard.Staple;

/**
 * Print setting data class.
 *
 * @author Manuel Madera
 */
public class PrintSettingDataHolder {

    /** Staple setting */
    private Staple mSelectedStapleValue;

    /** Number of copies */
    private Copies mSelectedCopiesValue;

    /** File name */
    private String mSelectedPrintFileName;

    /** Print color setting */
    private PrintColor mSelectedPrintColorValue;

    /** Print resolution setting */
    private PrintResolution mSelectedPrintResolutionValue;

    /** Paper tray setting */
    private PaperTray mSelectedPaperTrayValue;

    /** Print side setting */
    private PrintSide mSelectedPrintSideValue;

    /** PDL setting
     */
    private PrintFile.PDL mSelectedPDL;

    private static Logger log = ALogger.getLogger(PrintSettingDataHolder.class);


    /***********************************************************
     * Public methods
     ***********************************************************/

    /**
     * Create PrintFile object from the selected file.
     *
     * @return
     * @throws PrintException
     */
    public PrintFile getPrintFile() throws PrintException {
        InputStream is = null;
        PrintFile printfile = null;
        try {
            is = new FileInputStream(mSelectedPrintFileName);
        } catch (IOException e) {
            log.warn(e.toString());
            return null;
        }

        printfile = (new PrintFile.Builder()).localFileInputStream(is).pdl(mSelectedPDL).build();
        return printfile;
    }

    /**
     * Create Print request attribute set from current print settings.
     * @return
     */
    public PrintRequestAttributeSet getPrintRequestAttributeSet() {
        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        if(mSelectedStapleValue != null) attributeSet.add(mSelectedStapleValue);
        if(mSelectedCopiesValue != null) attributeSet.add(mSelectedCopiesValue);
        if(mSelectedPrintColorValue != null) attributeSet.add(mSelectedPrintColorValue);
        if(mSelectedPrintResolutionValue != null) attributeSet.add(mSelectedPrintResolutionValue);
        if(mSelectedPaperTrayValue != null) attributeSet.add(mSelectedPaperTrayValue);
        if(mSelectedPrintSideValue != null) attributeSet.add(mSelectedPrintSideValue);

        return attributeSet;
    }

    /***********************************************************
     * Setter methods
     ***********************************************************/

    /**
     * Set the staple setting to the specified value.
     * @param value
     */
    public void setSelectedStaple(Staple value) {
        mSelectedStapleValue = value;
    }

    /**
     * Set the number of copies to the specified value.
     * @param selectedCopiesValue
     */
    public void setSelectedCopiesValue(Copies selectedCopiesValue) {
        mSelectedCopiesValue = selectedCopiesValue;
    }

    /**
     * Set the print file name to the specified value.
     * @param selectedPrintFileName
     */
    public void setSelectedPrintFileName(String selectedPrintFileName) {
        mSelectedPrintFileName = selectedPrintFileName;
    }

    /**
     * Set the PDL to the specified value.
     * @param pdl
     */
    public void setSelectedPDL(PrintFile.PDL pdl) {
        mSelectedPDL = pdl;
    }

    /**
     * Set the print color setting to the specified value
     * @param printColor
     */
    public void setSelectedPrintColorValue(PrintColor printColor) {
        mSelectedPrintColorValue = printColor;
    }

    /**
     * Set the print resolution setting to the specified value
     * @param printResolution
     */
    public void setSelectedPrintResolutionValue(PrintResolution printResolution) {
        mSelectedPrintResolutionValue = printResolution;
    }

    /**
     * Set the paper tray setting to the specified value
     * @param paperTray
     */
    public void setSelectedPaperTrayValue(PaperTray paperTray) {
        mSelectedPaperTrayValue = paperTray;
    }

    /**
     * Set the print side setting to the specified value
     * @param printSide
     */
    public void setSelectedPrintSideValue(PrintSide printSide) {
        mSelectedPrintSideValue = printSide;
    }

    /***********************************************************
     * Getter methods
     ***********************************************************/

    /**
     * Obtains the current staple setting value.
     * @return
     */
    public Staple getSelectedStaple() {
        return mSelectedStapleValue;
    }

    /**
     * Get the current number of pages.
     * @return
     */
    public Copies getSelectedCopiesValue() {
        return mSelectedCopiesValue;
    }

    /**
     * Get the current print file name.
     * @return
     */
    public String getSelectedPrintFileName() {
        return mSelectedPrintFileName;
    }

    /**
     * Get the current PDL setting value.
     * @return
     */
//    public PrintFile.PDL getSelectedPDL() {
//        return mSelectedPDL;
//    }

    /**
     * Get the current print color setting value.
     * @return
     */
    public PrintColor getSelectedPrintColorValue() {
        return mSelectedPrintColorValue;
    }

    /**
     * Get the current print resolution setting value.
     * @return
     */
    public PrintResolution getSelectedPrintResolutionValue() {
        return mSelectedPrintResolutionValue;
    }

    /**
     * Get the current paper tray setting value.
     * @return
     */
    public PaperTray getSelectedPaperTrayValue() {
        return mSelectedPaperTrayValue;
    }

    /**
     * Get the current print side setting value.
     * @return
     */
    public PrintSide getSelectedPrintSideValue() {
        return mSelectedPrintSideValue;
    }
}

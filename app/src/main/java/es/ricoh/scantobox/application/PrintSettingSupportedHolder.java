/**
 * Copyright (C) 2016 Ricoh Spain IT Services.
 * All rights reserved.
 */
package es.ricoh.scantobox.application;

import java.util.List;
import java.util.Set;

import jp.co.ricoh.ssdk.function.print.PrintFile;
import jp.co.ricoh.ssdk.function.print.PrintService;
import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttribute;
import jp.co.ricoh.ssdk.function.print.attribute.standard.Copies;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PaperTray;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintColor;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintResolution;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintSide;
import jp.co.ricoh.ssdk.function.print.attribute.standard.Staple;
import jp.co.ricoh.ssdk.function.print.supported.MaxMinSupported;

/**
 * The class saves the list of supported print setting values.
 *
 * @author Manuel Madera
 */
public class PrintSettingSupportedHolder {

    private Set<Class<? extends PrintRequestAttribute>> mSelectableCategories;

    private List<Staple> mSelectableStapleLabelList = null;
    private MaxMinSupported mSelectableCopiesRange = null;
    private List<PrintColor> mSelectablePrintColorList = null;
    private List<PrintResolution> mSelectablePrintResolutionList = null;
    private List<PrintSide> mSelectablePrintSideList = null;
    private List<PaperTray> mSelectablePaperTrayList = null;

    /**
     * Constructor.
     *  [Processes]
     *    Obtains the list of supported attributes and saves them.
     *
     * @param service Reference to the print service.
     * @param pdl PDL
     */
    public PrintSettingSupportedHolder(PrintService service, PrintFile.PDL pdl) {
        mSelectableCategories = getSelectableCategory(service, pdl);
        if (mSelectableCategories != null) {
            if(mSelectableCategories.contains(Staple.class)) {
                mSelectableStapleLabelList = getStapleListFromSupportedValue(service, pdl);
            }

            if(mSelectableCategories.contains(Copies.class)) {
                mSelectableCopiesRange = getCopiesRangeFromSupportedValue(service, pdl);
            }

            if(mSelectableCategories.contains(PrintColor.class)) {
                mSelectablePrintColorList = getPrintColorListFromSupportedValue(service, pdl);
            }

            if(mSelectableCategories.contains(PrintResolution.class)) {
                mSelectablePrintResolutionList = getPrintResolutionListFromSupportedValue(service, pdl);
            }

            if(mSelectableCategories.contains(PaperTray.class)) {
                mSelectablePaperTrayList = getPaperTrayListFromSupportedValue(service, pdl);
            }

            if(mSelectableCategories.contains(PrintSide.class)) {
                mSelectablePrintSideList = getPrintSideListFromSupportedValue(service, pdl);
            }
        }
    }

    /***********************************************************
     * Public methods
     ***********************************************************/

    /**
     * Get the list of supported staple setting values.
     * @return
     */
    public List<Staple> getSelectableStapleList() {
        return mSelectableStapleLabelList;
    }

    /**
     * Get the range of the number of copies.
     * @return
     */
    public MaxMinSupported getSelectableCopiesRange() {
        return mSelectableCopiesRange;
    }

    /**
     * Get the set of the supported print attribute setting categories.
     * @return
     */
    public Set<Class<? extends PrintRequestAttribute>> getSelectableCategories() {
        return this.mSelectableCategories;
    }

    /**
     * Get the list of supported print color setting values.
     * @return
     */
    public List<PrintColor> getSelectablePrintColorList() {
        return mSelectablePrintColorList;
    }

    /**
     * Get the list of supported print resolution setting values.
     * @return
     */
    public List<PrintResolution> getSelectablePrintResolutionList() {
        return mSelectablePrintResolutionList;
    }

    /**
     * Get the list of supported paper tray values.
     * @return
     */
    public List<PaperTray> getSelectablePaperTrayList() {
        return mSelectablePaperTrayList;
    }

    /**
     * Get the list of supported print side setting values.
     * @return
     */
    public List<PrintSide> getSelectablePrintSideList() {
        return mSelectablePrintSideList;
    }

    /***********************************************************
     * Private methods
     ***********************************************************/

    /**
     * Obtains the list of the supported staple setting from the service.
     * @param service
     * @param pdl
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<Staple> getStapleListFromSupportedValue(PrintService service, PrintFile.PDL pdl) {
        return (List<Staple>)service.getSupportedAttributeValues(pdl, Staple.class);
    }

    /**
     * Obtains the range of the number of copies from the service.
     * @param service
     * @param pdl
     * @return
     */
    private MaxMinSupported getCopiesRangeFromSupportedValue(PrintService service, PrintFile.PDL pdl) {
        return (MaxMinSupported)service.getSupportedAttributeValues(pdl, Copies.class);
    }

    /**
     * Obtains the list of the supported attribute categories from the service.
     * @param service
     * @param pdl
     * @return
     */
    private Set<Class<? extends PrintRequestAttribute>> getSelectableCategory(PrintService service, PrintFile.PDL pdl){
        return service.getSupportedAttributeCategories(pdl);
    }

    /**
     * Obtains the list of the supported print color setting from the service.
     * @param service
     * @param pdl
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<PrintColor> getPrintColorListFromSupportedValue(PrintService service, PrintFile.PDL pdl) {
        return (List<PrintColor>)service.getSupportedAttributeValues(pdl, PrintColor.class);
    }

    /**
     * Obtains the list of the supported print resolutions settings from the service.
     * @param service
     * @param pdl
     * @return
     */
    private List<PrintResolution> getPrintResolutionListFromSupportedValue(PrintService service, PrintFile.PDL pdl) {
        return (List<PrintResolution>)service.getSupportedAttributeValues(pdl, PrintResolution.class);
    }

    /**
     * Obtains the list of the supported trays from the service
     * @param service
     * @param pdl
     * @return
     */
    private List<PaperTray> getPaperTrayListFromSupportedValue(PrintService service, PrintFile.PDL pdl) {
        return  (List<PaperTray>)service.getSupportedAttributeValues(pdl, PaperTray.class);
    }


    private List<PrintSide> getPrintSideListFromSupportedValue(PrintService service, PrintFile.PDL pdl) {
        return (List<PrintSide>)service.getSupportedAttributeValues(pdl, PrintSide.class);
    }

}

package es.ricoh.scantobox.application;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by cristian.rodriguez on 27/01/2017.
 * Checked: OK!
 */

public class FileChooser {
    private static final String PARENT_DIR = "..";

    private final Activity activity;
    private ListView list;
    private Dialog dialog;
    private File currentPath;

    // filter on file extension
    private ArrayList<String> extensionList = new ArrayList();
    private FileSelectedListener fileListener;

    public FileChooser(Activity activity, ArrayList<String> extensionList) {

        for (int i = 0; i < extensionList.size(); i++) {

            String extension = (extensionList.get(i) == null) ? null : extensionList.get(i).toLowerCase();
            this.extensionList.add(extension);
        }

        this.activity = activity;
        dialog = new Dialog(activity);
        list = new ListView(activity);
        list.setBackgroundColor(Color.BLACK);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int which, long id) {
                String fileChosen = (String) list.getItemAtPosition(which);
                File chosenFile = getChosenFile(fileChosen);
                if (chosenFile.isDirectory()) {
                    refresh(chosenFile);
                } else {
                    if (fileListener != null) {
                        fileListener.fileSelected(chosenFile);
                    }
                    dialog.dismiss();
                }
            }
        });

        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(list);
        dialog.getWindow().setLayout(500, 400);

        refresh(Environment.getExternalStorageDirectory());
    }

    public FileChooser setFileListener(FileSelectedListener fileListener) {
        this.fileListener = fileListener;
        return this;
    }

    public void showDialog() {
        dialog.show();
    }

    /**
     * Sort, filter and display the files for the given path.
     */
    private void refresh(File path) {
        this.currentPath = path;
        if (path.exists()) {

            File[] dirs = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    //return (file.isDirectory() && file.canRead());
                    return false;
                }
            });

            File[] files = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {

                    if (!file.isDirectory()) {
                        if (!file.canRead()) {
                            return false;
                        } else if (extensionList == null) {
                            return true;
                        } else {

                            boolean result = false;

                            for (int i = 0; i < extensionList.size(); i++) {
                                result = file.getName().toLowerCase().endsWith(extensionList.get(i));

                                if (result) {
                                    break;
                                }
                            }

                            return result;

                        }
                    } else {
                        return false;

                    }
                }

            });

            // convert to an array
            int i = 0;
            String[] fileList;
           /*if (path.getParentFile() == null) {
               fileList = new String[dirs.length + files.length];
           } else {
               fileList = new String[dirs.length + files.length + 1];
               fileList[i++] = PARENT_DIR;
           }*/

            fileList = new String[dirs.length + files.length];

            Arrays.sort(dirs);
            Arrays.sort(files);
            for (File dir : dirs) {
                fileList[i++] = dir.getName();
            }
            for (File file : files) {
                fileList[i++] = file.getName();
            }

            // refresh the user interface
            //dialog.setTitle(currentPath.getPath());
            dialog.setTitle("Tarjeta SD");

            list.setAdapter(new ArrayAdapter(activity,
                    android.R.layout.simple_list_item_1, fileList) {
                @Override
                public View getView(int pos, View view, ViewGroup parent) {
                    view = super.getView(pos, view, parent);
                    ((TextView) view).setSingleLine(true);
                    view.setBackgroundColor(Color.GRAY);
                    return view;
                }
            });
        }
    }

    /**
     * Convert a relative filename into an actual File object.
     */
    private File getChosenFile(String fileChosen) {
        if (fileChosen.equals(PARENT_DIR)) {
            return currentPath.getParentFile();
        } else {
            return new File(currentPath, fileChosen);
        }
    }

    // file selection event handling
    public interface FileSelectedListener {
        void fileSelected(File file);
    }
}
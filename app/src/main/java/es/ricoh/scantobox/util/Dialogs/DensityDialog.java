package es.ricoh.scantobox.util.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.util.Constants;
import es.ricoh.scantobox.util.Preferences;

/**
 * Created by cristian.rodriguez on 18/01/2017.
 */

public class DensityDialog {

    public static Dialog createDensityDialog(final Context context) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_light_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.linearLayoutDensity);
        RelativeLayout layoutAuto = (RelativeLayout) mainLayout.findViewById(R.id.layoutAuto);
        RelativeLayout layoutLighter = (RelativeLayout) mainLayout.findViewById(R.id.layoutLighter);
        RelativeLayout layoutDark = (RelativeLayout) mainLayout.findViewById(R.id.layoutDark);
        if(Preferences.density.equalsIgnoreCase(Constants.densityAuto)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutAuto.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutAuto.findViewById(R.id.checkAuto).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutAuto.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutAuto.findViewById(R.id.checkAuto).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.density.equalsIgnoreCase(Constants.densityLighter)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLighter.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLighter.findViewById(R.id.checkLighter).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutLighter.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLighter.findViewById(R.id.checkLighter).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.density.equalsIgnoreCase(Constants.densityDark)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutDark.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutDark.findViewById(R.id.checkDark).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutDark.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutDark.findViewById(R.id.checkDark).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }

        layoutAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.density = Constants.densityAuto;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkAuto)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkAuto)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout, R.id.checkAuto);
            }
        });
        layoutLighter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.density = Constants.densityLighter;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLighter)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLighter)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout, R.id.checkLighter);
            }
        });
        layoutDark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.density = Constants.densityDark;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkDark)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkDark)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout, R.id.checkDark);
            }
        });

        Button accept = (Button) dialog.findViewById(R.id.btn_acceptColor);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    private static void changeNoSelect(final Context context, LinearLayout mainLayout, int id){
        RelativeLayout layoutAuto = (RelativeLayout) mainLayout.findViewById(R.id.layoutAuto);
        RelativeLayout layoutLighter = (RelativeLayout) mainLayout.findViewById(R.id.layoutLighter);
        RelativeLayout layoutDark = (RelativeLayout) mainLayout.findViewById(R.id.layoutDark);

        if(id!=R.id.checkAuto) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutAuto.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutAuto.findViewById(R.id.checkAuto).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutAuto.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutAuto.findViewById(R.id.checkAuto).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkLighter) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLighter.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLighter.findViewById(R.id.checkLighter).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutLighter.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLighter.findViewById(R.id.checkLighter).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkDark) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutDark.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutDark.findViewById(R.id.checkDark).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutDark.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutDark.findViewById(R.id.checkDark).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
    }
}

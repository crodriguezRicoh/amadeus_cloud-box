package es.ricoh.scantobox.util.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.log4j.Logger;

import java.util.ArrayList;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.activity.SplashActivity;
import es.ricoh.scantobox.beans.BeanDocType;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Preferences;

/**
 * @author cristian.rodriguez
 */

public class DoctypeDialog {

    static ArrayList<RelativeLayout> listLayouts = new ArrayList<RelativeLayout>();
    static ArrayList<BeanDocType> listBeanDocType;
    private static final Logger log = ALogger.getLogger(SplashActivity.class);
    static LinearLayout mainLayout;

    public static Dialog createDoctypeDialog(final Context context) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_doctype_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mainLayout = (LinearLayout) dialog.findViewById(R.id.linearLayoutDoctype);

        /*listBeanDocType = new ArrayList<BeanDocType>();

        //Copiamos los beans guardados
        for(BeanDocType bDocType: Preferences.listBeanDocType){
            listBeanDocType.add(bDocType);
        }*/

//        listBeanDocType = Preferences.listBeanDocType;    //  TODO: se puede eliminar la clase DoctypeDialog entera?

        //Cargamos los layouts guardados
        for(int i = 0; i < listBeanDocType.size(); i ++){
            RelativeLayout layoutSp1 = generateSpaceLayout(context);
            RelativeLayout layout = generateButtonLayout(context, listBeanDocType.get(i).docType_id, listBeanDocType.get(i).docType_description);
            listBeanDocType.get(i).layout = layout;

            mainLayout.addView(layout);
            mainLayout.addView(layoutSp1);
        }

        Button accept = (Button) dialog.findViewById(R.id.btn_acceptColor);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainLayout.removeAllViews();
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static void deselectOthers(Context context, RelativeLayout layout){

        log.debug("DESELECT ENTRY:");

        log.debug("Comprobando: " + listBeanDocType.size() + " doctypes...");

        for(int i = 0; i < listBeanDocType.size(); i ++){

            RelativeLayout tempLayout = listBeanDocType.get(i).layout;

            if(tempLayout != layout){

                log.debug("docType: " + i + " es diferente... lo ponemos en blanco!");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    tempLayout.setBackgroundResource(R.drawable.bt_com_01_n);
                } else {
                    tempLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                }
            }else{
                log.debug("docType: " + i + " es IGUAL!... lo dejamos en AMARILLO!");
            }
        }
    }

    public static RelativeLayout generateSpaceLayout(final Context context){

        final RelativeLayout layout = new RelativeLayout(context);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(500, 20);
        layout.setLayoutParams(params);
        layout.setVisibility(View.INVISIBLE);

        return layout;
    }

    public static RelativeLayout generateButtonLayout(final Context context, final String id, final String text){

        TextView tv_id = new TextView(context);
        tv_id.setText(id);
        tv_id.setPadding(30, 10, 0, 0);
        tv_id.setTextSize(20);
        tv_id.setTextColor(Color.BLACK);

        TextView tv_name = new TextView(context);
        tv_name.setText(text);
        tv_name.setPadding(90, 10, 0, 0);
        tv_name.setTextSize(20);
        tv_name.setTextColor(Color.BLACK);

        final RelativeLayout layout = new RelativeLayout(context);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(500, 64);
        layout.setLayoutParams(params);

        //log.info("DOCTYPE IN PREFERENCES IS: " + Preferences.doctype + " AND DESCRIPTION IS: " + Preferences.doctypeDescription);
        //log.info("DOCTYPE ENTRY IS: " + id + " AND DESCRIPTION IS: " + text);

        if(Preferences.doctypeId.equals(id) && Preferences.doctypeDescription.equals(text)){
            layout.setBackgroundResource(R.drawable.bt_com_01_w);
        }else{
            layout.setBackgroundResource(R.drawable.bt_com_01_n);
        }

        layout.addView(tv_id);
        layout.addView(tv_name);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.doctypeId = id;
                Preferences.doctypeDescription = text;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackgroundResource(R.drawable.bt_com_01_w);
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                }

                deselectOthers(context, layout);
            }
        });

        return layout;
    }
}

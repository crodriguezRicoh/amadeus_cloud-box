package es.ricoh.scantobox.util.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.util.Properties;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.activity.MainActivity;
import es.ricoh.scantobox.activity.PreviewActivity;
import es.ricoh.scantobox.activity.ScanActivity;
import es.ricoh.scantobox.activity.SplashActivity;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Constants;
import es.ricoh.scantobox.util.CustomToast;
import es.ricoh.scantobox.util.Preferences;
import es.ricoh.scantobox.util.Utils;
import jp.co.ricoh.ssdk.wrapper.common.InvalidResponseException;
import jp.co.ricoh.ssdk.wrapper.common.Request;
import jp.co.ricoh.ssdk.wrapper.common.RequestQuery;
import jp.co.ricoh.ssdk.wrapper.common.Response;
import jp.co.ricoh.ssdk.wrapper.rws.property.DeviceProperty;
import jp.co.ricoh.ssdk.wrapper.rws.property.GetDeviceInfoResponseBody;

/**
 * Created by cristian.rodriguez on 18/01/2017.
 */

public class DialogUtil {

    private static final Logger log = ALogger.getLogger(DialogUtil.class);
    public static final int INPUT_DIALOG_WIDTH = 600;
    private static String serialNumber = "";
    public static TextView infoTextProcess;

    /**
     * The action name of the broadcast intent to be sent when the activity displayed.
     */
    public static final String INTENT_ACTION_SUB_ACTIVITY_RESUMED = "es.ricoh.app.base.util.SUB_ACTIVITY_RESUMED";

    public static void showDialog(Dialog d, int width, int height) {
        d.show();
        WindowManager.LayoutParams lp = d.getWindow().getAttributes();
        if (width > 0) {
            lp.width = width;
        }
        if (height > 0) {
            lp.height = height;
        }
        d.getWindow().setAttributes(lp);
    }


    public static void showDialogLogin(Dialog d, int width, int height) {
        showDialog(d, width, height);
    }

    public static Dialog createLoginDialog(final Context context, final int i) {

        log.info("***** Creating Config Login Dialog *****");

        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dlg_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    //dialog.cancel();
                    return true;
                }
                return false;
            }
        });

        final EditText edit_code = (EditText) dialog.findViewById(R.id.edit_user_code_popup);
        final RelativeLayout ventana = (RelativeLayout) dialog.findViewById(R.id.ventana);

        final Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok_popup);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_code.getText().toString().equals("R1c0h1")) {

                    log.info("El código es correcto");
                    if (i == 0) {
                        ((SplashActivity) context).launchConfig();
                    }else if(i == 1){
                        ((MainActivity) context).launchConfig();
                    }

                } else {
                    log.info("El código no es correcto");
                    Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);
                    ventana.startAnimation(shake);

                    CustomToast.showCustomAlert(context, context.getString(R.string.txt_invalid_code));
                }
            }
        });

        edit_code.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edit_code.getText().toString().equals("")) {
                    btn_ok.setEnabled(false);
                } else {
                    btn_ok.setEnabled(true);
                }
            }
        });

        final Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel_popup);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog createAboutDialog(final Context context, String message) {

        log.info("***** Creating About Dialog *****");
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dlg_about);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ((TextView)dialog.findViewById(R.id.txtDialogMessage)).setText(message);

        final Button accept = (Button) dialog.findViewById(R.id.accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog createFinishDialog(final Context context) {

        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dlg_finish);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final Button accept = (Button) dialog.findViewById(R.id.accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO se ha cambiado, comprobar que funciona...
                /*Intent intent = new Intent(context, ScanActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
                ((ScanActivity)context).finish();*/

                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog createScanInfoDialog(final Context context) {

        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dlg_scaninfo);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }

    public static Dialog createPreviewInfoDialog(final PreviewActivity context) {

        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dlg_previewinfo);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final Button accept = (Button) dialog.findViewById(R.id.accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.UnLock_logout(context, context.getPackageName());
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog createColorSettingDialog(final Context context/*, final ScanSettingDataHolder scanSettingDataHolder*/) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_color_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.linearLayoutColors);
        RelativeLayout layoutBW = (RelativeLayout) mainLayout.findViewById(R.id.layoutBW);
        RelativeLayout layoutColor = (RelativeLayout) mainLayout.findViewById(R.id.layoutColor);
        if(!Preferences.isColor){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutBW.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                (layoutBW.findViewById(R.id.checkBW)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutBW.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutBW.findViewById(R.id.checkBW).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else{

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutColor.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                (layoutColor.findViewById(R.id.checkColor)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutColor.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutColor.findViewById(R.id.checkColor).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }

        layoutColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.isColor = true;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkColor)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkColor)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                RelativeLayout layoutBW = (RelativeLayout) mainLayout.findViewById(R.id.layoutBW);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layoutBW.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutBW.findViewById(R.id.checkBW).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layoutBW.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutBW.findViewById(R.id.checkBW).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }
            }
        });

        layoutBW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.isColor = false;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkBW)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkBW)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                RelativeLayout layoutColor = (RelativeLayout) mainLayout.findViewById(R.id.layoutColor);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layoutColor.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutColor.findViewById(R.id.checkColor).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layoutColor.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutColor.findViewById(R.id.checkColor).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }
            }
        });

        Button accept = (Button) dialog.findViewById(R.id.btn_acceptColor);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog createColorPrintSettingDialog(final Context context) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_color_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.linearLayoutColors);
        RelativeLayout layoutBW = (RelativeLayout) mainLayout.findViewById(R.id.layoutBW);
        RelativeLayout layoutColor = (RelativeLayout) mainLayout.findViewById(R.id.layoutColor);
        if(!Preferences.print_isColor){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutBW.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                (layoutBW.findViewById(R.id.checkBW)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutBW.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutBW.findViewById(R.id.checkBW).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else{

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutColor.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                (layoutColor.findViewById(R.id.checkColor)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutColor.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutColor.findViewById(R.id.checkColor).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }

        layoutColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.print_isColor = true;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkColor)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkColor)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                RelativeLayout layoutBW = (RelativeLayout) mainLayout.findViewById(R.id.layoutBW);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layoutBW.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutBW.findViewById(R.id.checkBW).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layoutBW.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutBW.findViewById(R.id.checkBW).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }
            }
        });

        layoutBW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.print_isColor = false;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkBW)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkBW)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                RelativeLayout layoutColor = (RelativeLayout) mainLayout.findViewById(R.id.layoutColor);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layoutColor.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutColor.findViewById(R.id.checkColor).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layoutColor.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutColor.findViewById(R.id.checkColor).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }
            }
        });

        Button accept = (Button) dialog.findViewById(R.id.btn_acceptColor);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static class ScanDialog extends Dialog{
        public Activity activity;
        private Button cancel, stop;
        private TextView tMessage;

        public ScanDialog(Activity a) {
            super(a);
            activity = a;
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dlg_scan);
            setCancelable(false);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            cancel = (Button) findViewById(R.id.cancelScan);
            //stop = (Button) findViewById(R.id.stopScan);
            tMessage = (TextView) findViewById(R.id.messageScan);

        }

        public Button getCancel(){
            return cancel;
        }

        //public Button getStop(){
        //return stop;
        //}

        public void setMessage(String message){
            tMessage.setText(message);
        }
    }

    public static class ScanWaitDialog extends Dialog{
        public Activity activity;
        private Button cancel;
        private TextView tMessage;

        public ScanWaitDialog(Activity a) {
            super(a);
            activity = a;
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dlg_scanwait);
            setCancelable(false);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            cancel = (Button) findViewById(R.id.cancelScan);
            tMessage = (TextView) findViewById(R.id.messageScan);
        }

        public Button getCancel(){
            return cancel;
        }
    }

    public static class ScanBootFailedDialog extends Dialog{
        public Activity activity;
        private Button accept;

        public ScanBootFailedDialog(Activity a) {
            super(a);
            activity = a;
        }

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dlg_error);
            setCancelable(false);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            accept = (Button) findViewById(R.id.accept);

        }

        public Button getAccept(){
            return accept;
        }
    }

    public static Dialog createErrorDialog(final Context context, String message) {

        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dlg_error);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ((TextView)dialog.findViewById(R.id.message)).setText(message);

        final Button accept = (Button) dialog.findViewById(R.id.accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog createSuccessDialog(final Context context, String message) {

        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dlg_correct);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ((TextView)dialog.findViewById(R.id.message)).setText(message);

        final Button accept = (Button) dialog.findViewById(R.id.accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog createProcessWaitDialog(final Context context, int layout) {

        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.layoutProcesando);
        infoTextProcess = (TextView) mainLayout.findViewById(R.id.info);

        return dialog;
    }

    public static Dialog createResolutionSettingDialog(final Context context/*, final ScanSettingDataHolder scanSettingDataHolder*/) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_resolution_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.linearLayoutRes);
        final RelativeLayout layout200 = (RelativeLayout) mainLayout.findViewById(R.id.layout200);
        final RelativeLayout layout300 = (RelativeLayout) mainLayout.findViewById(R.id.layout300);
        final RelativeLayout layout400 = (RelativeLayout) mainLayout.findViewById(R.id.layout400);
        final RelativeLayout layout600 = (RelativeLayout) mainLayout.findViewById(R.id.layout600);
        if(Preferences.resolutionDPI == 200){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layout200.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layout200.findViewById(R.id.check200).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layout200.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layout200.findViewById(R.id.check200).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.resolutionDPI == 300){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layout300.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layout300.findViewById(R.id.check300).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layout300.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layout300.findViewById(R.id.check300).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.resolutionDPI == 400){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layout400.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layout400.findViewById(R.id.check400).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layout400.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layout400.findViewById(R.id.check400).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.resolutionDPI == 600){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layout600.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layout600.findViewById(R.id.check600).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layout600.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layout600.findViewById(R.id.check600).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }

        layout200.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.resolutionDPI = 200;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.check200)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.check200)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout300.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout300.findViewById(R.id.check300).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout300.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout300.findViewById(R.id.check300).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout400.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout400.findViewById(R.id.check400).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout400.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout400.findViewById(R.id.check400).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout600.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout600.findViewById(R.id.check600).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout600.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout600.findViewById(R.id.check600).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }
            }
        });

        layout300.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.resolutionDPI = 300;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.check300)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.check300)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout200.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout200.findViewById(R.id.check200).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout200.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout200.findViewById(R.id.check200).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout400.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout400.findViewById(R.id.check400).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout400.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout400.findViewById(R.id.check400).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout600.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout600.findViewById(R.id.check600).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout600.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout600.findViewById(R.id.check600).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }
            }
        });

        layout400.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.resolutionDPI = 400;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.check400)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.check400)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout300.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout300.findViewById(R.id.check300).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout300.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout300.findViewById(R.id.check300).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout200.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout200.findViewById(R.id.check200).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout200.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout200.findViewById(R.id.check200).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout600.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout600.findViewById(R.id.check600).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout600.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout600.findViewById(R.id.check600).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }
            }
        });

        layout600.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.resolutionDPI = 600;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.check600)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.check600)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout300.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout300.findViewById(R.id.check300).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout300.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout300.findViewById(R.id.check300).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout400.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout400.findViewById(R.id.check400).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout400.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout400.findViewById(R.id.check400).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout200.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout200.findViewById(R.id.check200).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layout200.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layout200.findViewById(R.id.check200).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }
            }
        });

        Button accept = (Button) dialog.findViewById(R.id.btn_acceptColor);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog createFiletypeSettingDialog(final Context context/*, final ScanSettingDataHolder scanSettingDataHolder*/) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_filetype_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.linearLayoutFiletype);
        final RelativeLayout layoutPDF = (RelativeLayout) mainLayout.findViewById(R.id.layoutPDF);
        final RelativeLayout layoutJpegTiff = (RelativeLayout) mainLayout.findViewById(R.id.layoutJpegTiff);

        if (Preferences.fileType == Constants.fileTypePDF) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutPDF.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutPDF.findViewById(R.id.checkPDF).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutPDF.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutPDF.findViewById(R.id.checkPDF).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        } else if (Preferences.fileType == Constants.fileTypeJpegTiff) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutJpegTiff.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutJpegTiff.findViewById(R.id.checkJpegTiff).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutJpegTiff.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutJpegTiff.findViewById(R.id.checkJpegTiff).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }

        layoutPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.fileType = Constants.fileTypePDF;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkPDF)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkPDF)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layoutJpegTiff.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutJpegTiff.findViewById(R.id.checkJpegTiff).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layoutJpegTiff.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutJpegTiff.findViewById(R.id.checkJpegTiff).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }
            }
        });

        layoutJpegTiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.fileType = Constants.fileTypeJpegTiff;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkJpegTiff)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkJpegTiff)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layoutPDF.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutPDF.findViewById(R.id.checkPDF).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                } else {
                    layoutPDF.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                    layoutPDF.findViewById(R.id.checkPDF).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
                }

            }
        });

        Button accept = (Button) dialog.findViewById(R.id.btn_acceptColor);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }
}

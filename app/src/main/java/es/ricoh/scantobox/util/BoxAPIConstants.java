package es.ricoh.scantobox.util;

/**
 * Created by alex.arroyo on 20/03/2017.
 */
public class BoxAPIConstants {

    public static String ERROR_400_BAD_REQUEST = "bad_request";
    public static String ERROR_400_ITEM_NAME_INVALID = "item_name_invalid";
    public static String ERROR_400_TERMS_OF_SERVICE_REQUIRED = "terms_of_service_required";
    public static String ERROR_400_REQUESTED_PREVIEW_UNAVAILABLE = "requested_preview_unavailable";
    public static String ERROR_400_folder_not_empty = "folder_not_empty";
    public static String ERROR_400_INVALID_REQUEST_PARAMETERS = "invalid_request_parameters";
    public static String ERROR_400_USER_ALREADY_COLLABORATOR = "user_already_collaborator";
    public static String ERROR_400_CANNOT_MAKE_COLLABORATED_SUBFOLDER_PRIVATE = "cannot_make_collaborated_subfolder_private";
    public static String ERROR_400_ITEM_NAME_TOO_LONG = "item_name_too_long";
    public static String ERROR_400_COLLABORATIONS_NOT_AVAILABLE_ON_ROOT_FOLDER = "collaborations_not_available_on_root_folder";
    public static String ERROR_400_SYNC_ITEM_MOVE_FAILURE = "sync_item_move_failure";
    public static String ERROR_400_REQUESTED_PAGE_OUT_OF_RANGE = "requested_page_out_of_range";
    public static String ERROR_400_CYCLICAL_FOLDER_STRUCTURE = "cyclical_folder_structure";
    public static String ERROR_400_BAD_DIGEST = "bad_digest";
    public static String ERROR_400_INVALID_COLLABORATION_ITEM = "invalid_collaboration_item";
    public static String ERROR_400_TASK_ASSIGNEE_NOT_ALLOWED = "task_assignee_not_allowed";
    public static String ERROR_400_INVALID_STATUS = "invalid_status";
    public static String ERROR_400_INVALID_GRANT = "invalid_grant";
    public static String ERROR_400_UNAUTHORIZED = "unauthorized";
    public static String ERROR_403_FORBIDDEN = "forbidden";
    public static String ERROR_403_STORAGE_LIMIT_EXCEEDED = "storage_limit_exceeded";
    public static String ERROR_403_ACCESS_DENIED_INSUFFICIENT_PERMISSIONS = "access_denied_insufficient_permissions";
    public static String ERROR_403_ACCESS_DENIED_ITEM_LOCKED = "access_denied_item_locked";
    public static String ERROR_403_FILE_SIZE_LIMIT_EXCEEDED = "file_size_limit_exceeded";
    public static String ERROR_403_INCORRECT_SHARED_ITEM_PASSWORD = "incorrect_shared_item_password";
    public static String ERROR_403_ACCESS_FROM_LOCATION_BLOCKED = "access_from_location_blocked";
    public static String ERROR_404_NOT_FOUND = "not_found";
    public static String ERROR_404_PREVIEW_CANNOT_BE_GENERATED = "preview_cannot_be_generated";
    public static String ERROR_404_TRASHED = "trashed";
    public static String ERROR_404_NOT_TRASHED = "not_trashed";
    public static String ERROR_405_METHOD_NOT_ALLOWED = "method_not_allowed";
    public static String ERROR_409_ITEM_NAME_IN_USE = "item_name_in_use";
    public static String ERROR_409_CONFLICT = "conflict";
    public static String ERROR_409_USER_LOGIN_ALREADY_USED = "user_login_already_used";
    public static String ERROR_409_RECENT_SIMILAR_COMMENT = "recent_similar_comment";
    public static String ERROR_409_OPERATION_BLOCKED_TEMPORARY = "operation_blocked_temporary";
    public static String ERROR_412_SYNC_STATE_PRECONDITION_FAILED = "sync_state_precondition_failed";
    public static String ERROR_412_PRECONDITION_FAILED = "precondition_failed";
    public static String ERROR_429_RATE_LIMIT_EXCEEDED = "rate_limit_exceeded";
    public static String ERROR_500_INTERNAL_SERVER_ERROR = "internal_server_error";
    public static String ERROR_503_UNAVAILABLE = "unavailable";
}

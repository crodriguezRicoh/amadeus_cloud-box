package es.ricoh.scantobox.util.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.util.Preferences;

/**
 * Created by cristian.rodriguez on 18/01/2017.
 */

public class SideDialog {

    public static Dialog createSideDialog(final Context context) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_side_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.linearLayoutSides);
        RelativeLayout layoutOneSide = (RelativeLayout) mainLayout.findViewById(R.id.layoutOneSide);
        RelativeLayout layoutTwoSide = (RelativeLayout) mainLayout.findViewById(R.id.layoutTwoSide);
        if(!Preferences.duplex){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutOneSide.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutOneSide.findViewById(R.id.checkOneSide).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutOneSide.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutOneSide.findViewById(R.id.checkOneSide).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else{

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutTwoSide.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutTwoSide.findViewById(R.id.checkTwoSide).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutTwoSide.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutTwoSide.findViewById(R.id.checkTwoSide).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }

        layoutOneSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.duplex = false;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOneSide)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOneSide)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout, R.id.checkOneSide);
            }
        });
        layoutTwoSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.duplex = true;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkTwoSide)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkTwoSide)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout, R.id.checkTwoSide);
            }
        });

        Button accept = (Button) dialog.findViewById(R.id.btn_acceptSide);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog createPrintSideDialog(final Context context) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_side_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.linearLayoutSides);
        RelativeLayout layoutOneSide = (RelativeLayout) mainLayout.findViewById(R.id.layoutOneSide);
        RelativeLayout layoutTwoSide = (RelativeLayout) mainLayout.findViewById(R.id.layoutTwoSide);
        if(!Preferences.print_duplex){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutOneSide.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutOneSide.findViewById(R.id.checkOneSide).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutOneSide.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutOneSide.findViewById(R.id.checkOneSide).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else{

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutTwoSide.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutTwoSide.findViewById(R.id.checkTwoSide).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutTwoSide.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutTwoSide.findViewById(R.id.checkTwoSide).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }

        layoutOneSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.print_duplex = false;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOneSide)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOneSide)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout, R.id.checkOneSide);
            }
        });
        layoutTwoSide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.print_duplex = true;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkTwoSide)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkTwoSide)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout, R.id.checkTwoSide);
            }
        });

        Button accept = (Button) dialog.findViewById(R.id.btn_acceptSide);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    private static void changeNoSelect(final Context context, LinearLayout mainLayout, int id){
        RelativeLayout layoutOneSide = (RelativeLayout) mainLayout.findViewById(R.id.layoutOneSide);
        RelativeLayout layoutTwoSide = (RelativeLayout) mainLayout.findViewById(R.id.layoutTwoSide);

        if(id!=R.id.checkOneSide) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutOneSide.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutOneSide.findViewById(R.id.checkOneSide).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutOneSide.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutOneSide.findViewById(R.id.checkOneSide).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkTwoSide) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutTwoSide.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutTwoSide.findViewById(R.id.checkTwoSide).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutTwoSide.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutTwoSide.findViewById(R.id.checkTwoSide).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
    }
}

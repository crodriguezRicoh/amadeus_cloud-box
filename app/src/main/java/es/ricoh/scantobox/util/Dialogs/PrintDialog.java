package es.ricoh.scantobox.util.Dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import es.ricoh.scantobox.R;

/**
 * Printing dialog.
 *
 * @author Manuel Madera
 */
public class PrintDialog extends ProgressDialog {


    /** Dialog buttons */
    private Button mStop;

    /**
     * Creates a new InfoDialog with the given message and error code
     * @param context
     */
    public PrintDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_print);
        mStop = (Button) findViewById(R.id.btnStop);
    }

    @Override
    public void setMessage(CharSequence message) {
        TextView txtMessage = (TextView) findViewById(R.id.txtDialogMessage);
        txtMessage.setText(message);
    }


    public void setOnClickListener(View.OnClickListener listener) {
        mStop.setOnClickListener(listener);
    }

    public void setCancelable(boolean enable) {
        if (enable)
            mStop.setVisibility(View.VISIBLE);
        else
            mStop.setVisibility(View.INVISIBLE);
    }
}

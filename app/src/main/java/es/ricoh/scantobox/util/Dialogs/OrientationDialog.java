package es.ricoh.scantobox.util.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.util.Constants;
import es.ricoh.scantobox.util.Preferences;

/**
 * Created by cristian.rodriguez on 18/01/2017.
 */

public class OrientationDialog {

    public static Dialog createOrientationDialog(final Context context/*, final ScanSettingDataHolder scanSettingDataHolder*/) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_orientation_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final LinearLayout mainLayout = (LinearLayout) dialog.findViewById(R.id.linearLayoutOrientation);
        RelativeLayout layoutLegible = (RelativeLayout) mainLayout.findViewById(R.id.layoutOrientationLeg);
        RelativeLayout layoutNoLegible = (RelativeLayout) mainLayout.findViewById(R.id.layoutOrientationIleg);
        final ImageView imageHelpOrientation = (ImageView) mainLayout.findViewById(R.id.imageHelpOrientation);
        if(Preferences.orientation.equalsIgnoreCase(Constants.orientationL)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                imageHelpOrientation.setBackground(context.getResources().getDrawable(R.drawable.illust_orgorient_adf_01));
                layoutLegible.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLegible.findViewById(R.id.checkOrientationLeg).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                imageHelpOrientation.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.illust_orgorient_adf_01));
                layoutLegible.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLegible.findViewById(R.id.checkOrientationLeg).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }

        }else if(Preferences.orientation.equalsIgnoreCase(Constants.orientationNL)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                imageHelpOrientation.setBackground(context.getResources().getDrawable(R.drawable.illust_orgorient_adf_03));
                layoutNoLegible.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutNoLegible.findViewById(R.id.checkOrientationIleg).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                imageHelpOrientation.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.illust_orgorient_adf_03));
                layoutNoLegible.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutNoLegible.findViewById(R.id.checkOrientationIleg).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }

        layoutLegible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.orientation = Constants.orientationL;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageHelpOrientation.setBackground(context.getResources().getDrawable(R.drawable.illust_orgorient_adf_01));
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOrientationLeg)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    imageHelpOrientation.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.illust_orgorient_adf_01));
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOrientationLeg)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout, R.id.checkOrientationLeg);
            }
        });
        layoutNoLegible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.orientation = Constants.orientationNL;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    imageHelpOrientation.setBackground(context.getResources().getDrawable(R.drawable.illust_orgorient_adf_03));
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOrientationIleg)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    imageHelpOrientation.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.illust_orgorient_adf_03));
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOrientationIleg)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout, R.id.checkOrientationIleg);
            }
        });

        Button accept = (Button) dialog.findViewById(R.id.btn_acceptOrientation);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    private static void changeNoSelect(final Context context, LinearLayout mainLayout, int id){
        RelativeLayout layoutLegible = (RelativeLayout) mainLayout.findViewById(R.id.layoutOrientationLeg);
        RelativeLayout layoutNoLegible = (RelativeLayout) mainLayout.findViewById(R.id.layoutOrientationIleg);

        if(id!=R.id.checkOrientationLeg) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLegible.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLegible.findViewById(R.id.checkOrientationLeg).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutLegible.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLegible.findViewById(R.id.checkOrientationLeg).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkOrientationIleg) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutNoLegible.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutNoLegible.findViewById(R.id.checkOrientationIleg).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutNoLegible.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutNoLegible.findViewById(R.id.checkOrientationIleg).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
    }
}

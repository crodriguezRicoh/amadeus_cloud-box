package es.ricoh.scantobox.util;

import android.os.AsyncTask;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxUser;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.HashMap;

import es.ricoh.scantobox.activity.SplashActivity;
import es.ricoh.scantobox.util.Dialogs.BeanBoxApiException;

/**
 * Created by alex.arroyo on 24/02/2017.
 * <p>
 * BoxApiManager se encarga de gestionar los objetos BoxAPIConnection
 */
public class BoxApiManager implements Serializable {


    private static final Logger log = ALogger.getLogger(BoxApiManager.class);

    private static HashMap<String, String> hmApis;

    private static String filepath = "/mnt/hdd/" + SplashActivity.packageName + "/bam.srl";


    public static void addBoxUserApi(String username, BoxAPIConnection api) {
        log.debug("addBoxUserApi");

        if (hmApis == null) {
            hmApis = new HashMap<>();
        }

        if (api == null) {
            log.warn("Api is null");
            return;
        }

        if (username != null && !username.isEmpty()) {
            hmApis.put(username, api.save());
        } else {
            log.warn("Username is null or empty. User Api won't be stored");
            return;
        }

        save();
    }

    public static BoxAPIConnection getBoxApi(String username) {
        log.debug("getBoxApi(" + username + ")");

        if (hmApis == null) {
            log.debug("Api store is empty");
            return null;
        }

        if (username == null || username.isEmpty()) {
            log.debug("Username is null or empty");
            return null;
        } else {
            if (hmApis.containsKey(username)) {

                String savedApiState = hmApis.get(username);
                BoxAPIConnection api = null;
                try {
                    api = new BoxAPIConnection(Preferences.clientID, Preferences.clientSecret);
                    api.restore(savedApiState);
                } catch (Exception e) {
                    log.error("An error occurred while restoring Api state of user " + username);
                    log.error(Utils.printStackTraceToString(e));
                    return null;
                }
                return api;
            } else {
                log.debug("Api store doesn't contain usename: " + username);
                return null;
            }
        }
    }

    public static boolean containsUserApi(String username) {
        log.debug("containsUserApi(" + username + ")");

        if (hmApis == null) {
            log.debug("containsUserApi = False: Api store is empty");
            return false;
        } else if (username == null || username.isEmpty()) {
            log.debug("containsUserApi = False: Username is null or empty");
            return true;
        } else if (hmApis.containsKey(username)) {
            log.debug("containsUserApi = True");
            return true;
        } else {
            log.debug("containsUserApi = False");
            return false;
        }
    }

    public static void removeUserApi(String username) {
        log.debug("removeUserApi");
        if (hmApis != null && hmApis.containsKey(username)) {
            hmApis.remove(username);
        }

        save();
    }

    public static boolean isUserApiValid(String username) {
        log.debug("isUserApiValid");

        boolean isApiValid = false;
        try {
            BoxAPIConnection api = BoxApiManager.getBoxApi(username);
            if (api == null) {
                log.debug("isUserApiValid: api is null");
                removeUserApi(username);    //  Si la api no es válida, la eliminamos para evitar problemas
                isApiValid =  false;
            } else {

                RefreshApiTask refreshApi = new RefreshApiTask(); //  TODO: nos sirve con esta comprobación?
                isApiValid = refreshApi.execute(api).get();
                if (isApiValid) {
                    addBoxUserApi(username, api);   //  Si la api es valida, sobreescribimos la que teníamos por la refrescada
                } else {
                    removeUserApi(username);    //  Si la api no es válida, la eliminamos para evitar problemas
                }
            }

        } catch (Exception e) {
            isApiValid = false;
            removeUserApi(username);    //  Si la api no es válida, la eliminamos para evitar problemas
            log.error(Utils.printStackTraceToString(e));
        } finally {
            log.debug("isUserApiValid = " + isApiValid);
            return isApiValid;
        }
    }


    static class RefreshApiTask extends AsyncTask<BoxAPIConnection, Void, Boolean> {

        @Override
        protected Boolean doInBackground(BoxAPIConnection... params) {
            log.debug("RefreshApi AsyncTask");
            BoxAPIConnection api = params[0];

            if (api == null) {
                log.debug("RefreshApi: api is null");
                return false;
            }

            int nRetry = 0;
            while (nRetry < 3) {

                log.debug("-> retry " + nRetry);

                try {
                    api.refresh();
                    log.debug("RefreshApi: api refreshed");
                    return true;
                } catch (BoxAPIException e) {
                    //  TODO: for test
                    log.error("********* BoxAPIException *********");
                    log.error("*** Cause: " + e.getCause());

                    BeanBoxApiException bean = Utils.getBoxAPIExceptionInfo(e);
                    log.debug("status: " + bean.status);
                    log.debug("code: " + bean.code);
                    log.debug("message: " + bean.message);

                    log.error(Utils.printStackTraceToString(e));

                    if (e.getCause() instanceof IOException) {
                        nRetry++;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            log.error(Utils.printStackTraceToString(ie));
                        }
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    log.error("********* EXCEPTION *********");
                    log.error(Utils.printStackTraceToString(e));
                    return false;
                }

            }
            return false;

        }
    }

//    static class RefreshApi extends AsyncTask<BoxAPIConnection, Void, Boolean> {
//
//        @Override
//        protected Boolean doInBackground(BoxAPIConnection... params) {
//            log.debug("RefreshApi AsyncTask");
//            BoxAPIConnection api = params[0];
//
//            if (api == null) {
//                log.debug("RefreshApi: api is null");
//                return false;
//            }
//
//            try {
//                api.refresh();
//                log.debug("RefreshApi: api refreshed");
//                return true;
//            } catch (Exception e) {
//                log.error(Utils.printStackTraceToString(e));
//                return false;
//            }
//        }
//    }


    public static String getBoxLogin(String username) {
        log.debug("getBoxLogin");

        try {
            GetLoginTask getLogin = new GetLoginTask();
            String strLogin = getLogin.execute(username).get();
            if (strLogin == null) {
                return "";
            } else {
                return strLogin;
            }
        } catch (Exception e) {
            log.error(Utils.printStackTraceToString(e));
            return "";
        }
    }

    static class GetLoginTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String username = params[0];

            if (username == null || username.isEmpty()) {
                return "";
            } else {
                if (!containsUserApi(username)) {
                    return "";
                } else {
                    BoxAPIConnection api = getBoxApi(username);

                    if (api != null) {
                        try {
                            BoxUser user = BoxUser.getCurrentUser(api);
                            BoxUser.Info info = user.getInfo();
                            if (info.getLogin() != null) {
                                return info.getLogin();
                            } else {
                                return "";
                            }
                        } catch (Exception e) {
                            log.error(Utils.printStackTraceToString(e));
                            return "";
                        }

                    } else {
                        return "";
                    }
                }
            }
        }
    }

    private static void save() {
        log.debug("BoxApiManager.save()");
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(new File(filepath)));
            out.writeObject(hmApis);    //  TODO: guardar de algún modo más seguro
        } catch (FileNotFoundException e) {
            log.error("File not found");
        } catch (IOException e) {
            log.error("IO Exception");
        } catch (Exception e) {
            log.error(Utils.printStackTraceToString(e));
        } finally {
            try {
                out.close();
            } catch (Exception e) {
            }
        }
    }

    public static void load() {
        log.debug("BoxApiManager.load()");
        ObjectInputStream input = null;

        try {
            input = new ObjectInputStream(new FileInputStream(new File(filepath)));
            hmApis = (HashMap<String, String>) input.readObject();
            hmApis.remove(Constants.GUEST_USER); //  Eliminamos si existe el usuario temporal usado cuando no funciona el Xlet y no se puede recuperar el username de SLNX
        } catch (FileNotFoundException e) {
            log.error("File not found");
        } catch (IOException e) {
            log.error("IO Exception");
        } catch (Exception e) {
            log.error(Utils.printStackTraceToString(e));
        } finally {
            try {
                input.close();
            } catch (Exception e) {
            }
        }
    }

}

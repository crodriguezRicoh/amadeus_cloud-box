/**
 * Copyright (C) 2016 Ricoh Spain IT Services.
 * All rights reserved.
 */
package es.ricoh.scantobox.util.Dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import es.ricoh.scantobox.R;


/**
 * Waiting dialog.
 *
 * @author Manuel Madera
 */
public class WaitDialog extends ProgressDialog {
    public WaitDialog(Context context) {
        super(context);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_wait);
    }

    @Override
    public void setMessage(CharSequence message) {
        TextView txtMessage = (TextView) findViewById(R.id.txtDialogMessage);
        txtMessage.setText(message);
    }

}

package es.ricoh.scantobox.util;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxAPIRequest;
import com.box.sdk.BoxAPIResponse;
import com.box.sdk.BoxFile;
import com.box.sdk.ProgressListener;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.text.DecimalFormat;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.activity.PrintActivity;
import es.ricoh.scantobox.activity.SplashActivity;
import es.ricoh.scantobox.beans.BeanLockTimeOut;
import es.ricoh.scantobox.util.Dialogs.BeanBoxApiException;


/**
 * Created by cristian.rodriguez on 27/01/2017.
 */

public class Utils {
    private static final Logger log = ALogger.getLogger(Utils.class);
    public static final String INTENT_ACTION_LOCK_LOGOUT = "jp.co.ricoh.isdk.sdkservice.auth.LOCK_LOGOUT";
    public static final String INTENT_ACTION_UNLOCK_LOGOUT = "jp.co.ricoh.isdk.sdkservice.auth.UNLOCK_LOGOUT";
    public static final String INTENT_ACTION_LOCK_SYSTEM_RESET = "jp.co.ricoh.isdk.sdkservice.system.SystemManager.LOCK_SYSTEM_RESET";
    public static final String INTENT_ACTION_UNLOCK_SYSTEM_RESET = "jp.co.ricoh.isdk.sdkservice.system.SystemManager.UNLOCK_SYSTEM_RESET";

    public static String printStackTraceToString(Exception ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));

        String error = "Error: " + errors.toString();

        return error;
    }

    public static void Lock_logout(Context context, String packageName){

        //Toast.makeText(context, "LOCK_LOGOUT", Toast.LENGTH_LONG).show();

        log.debug("Lock_logout - Context: " + context);
        log.debug("Lock_logout - PackageName: " + packageName);

        Intent intentLock = new Intent(INTENT_ACTION_LOCK_LOGOUT);
        intentLock.putExtra("PACKAGE_NAME", packageName);
        context.sendBroadcast(intentLock);

        Intent intentLockSystem = new Intent(INTENT_ACTION_LOCK_SYSTEM_RESET);
        intentLockSystem.putExtra("PACKAGE_NAME", packageName);
        context.sendBroadcast(intentLockSystem);

    }

    public static void UnLock_logout(Context context, String packageName){

        //Toast.makeText(context, "UN_LOCK_LOGOUT", Toast.LENGTH_LONG).show();

        log.debug("UnLock_logout - Context: " + context);
        log.debug("UnLock_logout - PackageName: " + packageName);

        Intent intentUnLockLogout = new Intent(INTENT_ACTION_UNLOCK_LOGOUT);
        intentUnLockLogout.putExtra("PACKAGE_NAME", packageName);
        context.sendBroadcast(intentUnLockLogout);

        Intent intentUnLockSystem = new Intent(INTENT_ACTION_UNLOCK_SYSTEM_RESET);
        intentUnLockSystem.putExtra("PACKAGE_NAME", packageName);
        context.sendBroadcast(intentUnLockSystem);
    }

    public static BeanBoxApiException getBoxAPIExceptionInfo(BoxAPIException ex) {

        log.debug("getBoxAPIExceptionInfo");

        BeanBoxApiException bean = null;
        try {
            bean = new BeanBoxApiException();
            bean.status = ex.getResponseCode();
            //  Si el status es 0, significa que la BoxException ha derivado de otra excepción no relacionada con BOX y que no tendrá code ni message, por eso lo devolvemos vacío
            if (bean.status == 0) {
                return bean;
            }

            JSONObject json = new JSONObject(ex.getResponse());
            bean.code = json.getString("code");
            bean.message = json.getString("message");
        } catch (JSONException e) {
            log.error(printStackTraceToString(e));
        } catch (Exception e) {
            log.error(printStackTraceToString(e));
        }

        return bean;
    }

    public static String formatFileSize(long size) {
        String hrSize = null;
        DecimalFormat dec = null;
        try {
            if (size < 0) {
                size = 0;
            }

            double k = size / 1024.0;
            double m = ((size / 1024.0) / 1024.0);

            dec = new DecimalFormat("0.00");

            if (m > 1) {
                hrSize = dec.format(m).concat(" MB");
            } else {
                hrSize = dec.format(k).concat(" KB");
            }
        } catch (Exception e) {
            hrSize = "0,00 KB";
        }
        return hrSize;
    }

    public static String formatFileSize(int size) {

        String hrSize = null;
        DecimalFormat dec = null;

        try {
            if (size < 0) {
                size = 0;
            }

            double k = size / 1024.0;
            double m = ((size / 1024.0) / 1024.0);

            dec = new DecimalFormat("0.00");

            if (m > 1) {
                hrSize = dec.format(m).concat(" MB");
            } else {
                hrSize = dec.format(k).concat(" KB");
            }
        } catch (Exception e) {
            hrSize = "0,00 KB";
        }
        return hrSize;
    }

}

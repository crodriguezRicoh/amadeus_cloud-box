/**
 * Copyright (C) 2016 Ricoh Spain IT Services.
 * All rights reserved.
 */
package es.ricoh.scantobox.util.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.log4j.Logger;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Utils;
import jp.co.ricoh.ssdk.function.Const;


/**
 * This class defines the info dialogs displayed in this application.
 *
 * @author Manuel Madera
 */
public class InfoDialog extends AlertDialog {

    /** Dialog buttons */
    private Button mAccept;

    /**
     * Creates a new InfoDialog with the given message and error code
     * @param context
     */
    public InfoDialog(Context context) {
        super(context);
    }
    private static Logger log = ALogger.getLogger(InfoDialog.class);
    private final static String PREFIX = "activity:InfoDialog:";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);

            setContentView(R.layout.dialog_custom);
            mAccept = (Button) findViewById(R.id.btnDialogC);
        }catch (Exception ex){
            log.error(Const.TAG + " " + PREFIX + "onCreate: " + Utils.printStackTraceToString(ex));
        }

    }

    public void setMessage(CharSequence message) {
        try {
            TextView txtMessage = (TextView) findViewById(R.id.txtDialogMessage);
            txtMessage.setText(message);
        }catch (Exception ex){
            log.error(Const.TAG + " " + PREFIX + "setMessage: " + Utils.printStackTraceToString(ex));
        }
    }

    public void setErrorCode(CharSequence errorCode) {
        TextView txtErrorCode = (TextView) findViewById(R.id.txtDialogErrorCode);
        txtErrorCode.setText(errorCode);
    }

    public void viewInfoIcon(boolean view) {
        ImageView icon = (ImageView) findViewById(R.id.iconDialog);
        if (view == true)
            icon.setVisibility(View.VISIBLE);
        else
            icon.setVisibility(View.GONE);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        mAccept.setOnClickListener(listener);
    }

    public void setButtonLabel(String label) {
        mAccept.setText(label);
    }
}

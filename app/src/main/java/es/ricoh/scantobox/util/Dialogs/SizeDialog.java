package es.ricoh.scantobox.util.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.util.Constants;
import es.ricoh.scantobox.util.Preferences;

/**
 * Created by cristian.rodriguez on 18/01/2017.
 */

public class SizeDialog {

    public static Dialog createSizeDialog(final Context context) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_size_select);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final LinearLayout mainLayout1 = (LinearLayout) dialog.findViewById(R.id.linearLayoutSizes1);
        //final LinearLayout mainLayout2 = (LinearLayout) dialog.findViewById(R.id.linearLayoutSizes2);
        RelativeLayout layoutAuto = (RelativeLayout) mainLayout1.findViewById(R.id.layoutAuto);
        RelativeLayout layoutA4 = (RelativeLayout) mainLayout1.findViewById(R.id.layoutA4);
        RelativeLayout layoutA4L = (RelativeLayout) mainLayout1.findViewById(R.id.layoutA4L);
        /*RelativeLayout layoutLetter = (RelativeLayout) mainLayout1.findViewById(R.id.layoutLetter);
        RelativeLayout layoutLetterL = (RelativeLayout) mainLayout1.findViewById(R.id.layoutLetterL);
        RelativeLayout layoutLegal = (RelativeLayout) mainLayout2.findViewById(R.id.layoutLegal);
        RelativeLayout layoutLegalL = (RelativeLayout) mainLayout2.findViewById(R.id.layoutLegalL);
        RelativeLayout layoutOficio = (RelativeLayout) mainLayout2.findViewById(R.id.layoutOficio);
        RelativeLayout layoutA3 = (RelativeLayout) mainLayout2.findViewById(R.id.layoutA3);*/

        //TODO comprobar los tamaños disponibles para imprimir, para scan serian válidos todos
        if(Preferences.size.equalsIgnoreCase(Constants.sizeAuto)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutAuto.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutAuto.findViewById(R.id.checkAuto).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutAuto.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutAuto.findViewById(R.id.checkAuto).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.size.equalsIgnoreCase(Constants.sizeA4)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutA4.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutA4.findViewById(R.id.checkA4).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutA4.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutA4.findViewById(R.id.checkA4).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.size.equalsIgnoreCase(Constants.sizeA4L)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutA4L.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutA4L.findViewById(R.id.checkA4L).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutA4L.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutA4L.findViewById(R.id.checkA4L).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }/*else if(Preferences.size.equalsIgnoreCase(Constants.sizeLetter)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLetter.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLetter.findViewById(R.id.checkLetter).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutLetter.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLetter.findViewById(R.id.checkLetter).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.size.equalsIgnoreCase(Constants.sizeLetterL)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLetterL.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLetterL.findViewById(R.id.checkLetterL).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutLetterL.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLetterL.findViewById(R.id.checkLetterL).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.size.equalsIgnoreCase(Constants.sizeLegal)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLegal.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLegal.findViewById(R.id.checkLegal).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutLegal.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLegal.findViewById(R.id.checkLegal).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.size.equalsIgnoreCase(Constants.sizeLegalL)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLegalL.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLegalL.findViewById(R.id.checkLegalL).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutLegalL.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutLegalL.findViewById(R.id.checkLegalL).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.size.equalsIgnoreCase(Constants.sizeOficio)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutOficio.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutOficio.findViewById(R.id.checkOficio).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutOficio.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutOficio.findViewById(R.id.checkOficio).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }else if(Preferences.size.equalsIgnoreCase(Constants.sizeA3)){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutA3.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutA3.findViewById(R.id.checkA3).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            } else {
                layoutA3.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                layoutA3.findViewById(R.id.checkA3).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
            }
        }*/

        layoutAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.size = Constants.sizeAuto;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkAuto)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkAuto)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout1/*, mainLayout2*/, R.id.checkAuto);
            }
        });
        layoutA4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.size = Constants.sizeA4;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkA4)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkA4)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout1/*, mainLayout2*/, R.id.checkA4);
            }
        });
        layoutA4L.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.size = Constants.sizeA4L;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkA4L)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkA4L)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout1/*, mainLayout2*/, R.id.checkA4L);
            }
        });
        /*layoutLetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.size = Constants.sizeLetter;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLetter)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLetter)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout1, mainLayout2, R.id.checkLetter);
            }
        });
        layoutLetterL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.size = Constants.sizeLetterL;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLetterL)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLetterL)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout1, mainLayout2, R.id.checkLetterL);
            }
        });
        layoutLegal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.size = Constants.sizeLegal;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLegal)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLegal)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout1, mainLayout2, R.id.checkLegal);
            }
        });
        layoutLegalL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.size = Constants.sizeLegalL;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLegalL)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkLegalL)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout1, mainLayout2, R.id.checkLegalL);
            }
        });
        layoutOficio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.size = Constants.sizeOficio;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOficio)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkOficio)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout1, mainLayout2, R.id.checkOficio);
            }
        });
        layoutA3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.size = Constants.sizeA3;
                RelativeLayout l = (RelativeLayout) view;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    l.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkA3)).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                } else {
                    l.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_w));
                    (l.findViewById(R.id.checkA3)).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_w));
                }

                changeNoSelect(context, mainLayout1, mainLayout2, R.id.checkA3);
            }
        });*/


        Button accept = (Button) dialog.findViewById(R.id.btn_acceptColor);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

    private static void changeNoSelect(final Context context, LinearLayout mainLayout1/*, LinearLayout mainLayout2*/, int id){
        RelativeLayout layoutAuto = (RelativeLayout) mainLayout1.findViewById(R.id.layoutAuto);
        RelativeLayout layoutA4 = (RelativeLayout) mainLayout1.findViewById(R.id.layoutA4);
        RelativeLayout layoutA4L = (RelativeLayout) mainLayout1.findViewById(R.id.layoutA4L);
        /*RelativeLayout layoutLetter = (RelativeLayout) mainLayout1.findViewById(R.id.layoutLetter);
        RelativeLayout layoutLetterL = (RelativeLayout) mainLayout1.findViewById(R.id.layoutLetterL);
        RelativeLayout layoutLegal = (RelativeLayout) mainLayout2.findViewById(R.id.layoutLegal);
        RelativeLayout layoutLegalL = (RelativeLayout) mainLayout2.findViewById(R.id.layoutLegalL);
        RelativeLayout layoutOficio = (RelativeLayout) mainLayout2.findViewById(R.id.layoutOficio);
        RelativeLayout layoutA3 = (RelativeLayout) mainLayout2.findViewById(R.id.layoutA3);*/

        if(id!=R.id.checkAuto) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutAuto.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutAuto.findViewById(R.id.checkAuto).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutAuto.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutAuto.findViewById(R.id.checkAuto).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkA4) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutA4.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutA4.findViewById(R.id.checkA4).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutA4.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutA4.findViewById(R.id.checkA4).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkA4L) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutA4L.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutA4L.findViewById(R.id.checkA4L).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutA4L.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutA4L.findViewById(R.id.checkA4L).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        /*if(id!=R.id.checkLetter) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLetter.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLetter.findViewById(R.id.checkLetter).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutLetter.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLetter.findViewById(R.id.checkLetter).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkLetterL) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLetterL.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLetterL.findViewById(R.id.checkLetterL).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutLetterL.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLetterL.findViewById(R.id.checkLetterL).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkLegal) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLegal.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLegal.findViewById(R.id.checkLegal).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutLegal.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLegal.findViewById(R.id.checkLegal).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkLegalL) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutLegalL.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLegalL.findViewById(R.id.checkLegalL).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutLegalL.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutLegalL.findViewById(R.id.checkLegalL).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkOficio) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutOficio.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutOficio.findViewById(R.id.checkOficio).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutOficio.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutOficio.findViewById(R.id.checkOficio).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }
        if(id!=R.id.checkA3) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layoutA3.setBackground(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutA3.findViewById(R.id.checkA3).setBackground(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            } else {
                layoutA3.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bt_com_01_n));
                layoutA3.findViewById(R.id.checkA3).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_act_radio_n));
            }
        }*/
    }
}

package es.ricoh.scantobox.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.beans.BeanBoxItem;

/**
 * Created by alex.arroyo on 27/02/2017.
 */
public class BoxItemAdapter extends ArrayAdapter<BeanBoxItem> {

    Context context;
    int layoutResourceId;
    List<BeanBoxItem> data = null;
    private int selectedIndex = -1;

    public BoxItemAdapter(Context context, int layoutResourceId, List<BeanBoxItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    public void setSelectedIndex(int index){
        selectedIndex = index;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        BoxItemHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new BoxItemHolder();
            holder.imgIcon = (ImageView) row.findViewById(R.id.itemImgIcon);
            holder.txtTitle = (TextView) row.findViewById(R.id.itemTxtTitle);
//            holder.txtSize = (TextView) row.findViewById(R.id.itemTxtSize);

            row.setTag(holder);
        } else {
            holder = (BoxItemHolder) row.getTag();
        }

        BeanBoxItem item = data.get(position);  //  TODO: for test
        holder.txtTitle.setText(item.itemName);
//        holder.txtSize.setText(item.size);
        holder.imgIcon.setImageResource(item.icon);

        return row;
    }

    static class BoxItemHolder {
        ImageView imgIcon;
        TextView txtTitle;
//        TextView txtSize;
    }
}

package es.ricoh.scantobox.util;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * Created by cristian.rodriguez
 * Checked: OK!
 */
public class ALogger {
    public static Logger getLogger(Class c) {

        File dir = new File("/mnt/hdd/es.ricoh.scantobox/log/");
        if(!dir.exists())
            dir.mkdirs();

        File file = new File("/mnt/hdd/es.ricoh.scantobox/log/scantobox.log");

        final LogConfigurator logConfigurator = new LogConfigurator();
        logConfigurator.setFileName("/mnt/hdd/es.ricoh.scantobox/log/scantobox.log");
        logConfigurator.setRootLevel(Level.ALL);
        logConfigurator.setLevel("org.apache", Level.ALL);
        logConfigurator.setUseFileAppender(true);
        logConfigurator.setFilePattern("%d %-5p [%c{2}]-[%L] %m%n");
        logConfigurator.setMaxFileSize(1024 * 1024 * 5);
        logConfigurator.setMaxBackupSize(1);
        logConfigurator.setImmediateFlush(true);
        logConfigurator.configure();
        Logger log = Logger.getLogger(c);

        return log;
    }
}

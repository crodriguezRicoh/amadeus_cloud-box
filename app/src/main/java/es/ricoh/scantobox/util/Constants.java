package es.ricoh.scantobox.util;

/**
 * Created by cristian.rodriguez
 * Checked: OK!
 */
public class Constants {

    public static String sizeAuto = "Auto";
    public static String sizeA4 = "A4";
    public static String sizeA4L = "A4L";
    public static String sizeLetter = "Letter";
    public static String sizeLetterL = "LetterL";
    public static String sizeLegal = "Legal";
    public static String sizeLegalL = "LegalL";
    public static String sizeOficio = "Oficio";
    public static String sizeA3 = "A3";

    public static String densityAuto = "Auto";
    public static String densityLighter = "Lighter";
    public static String densityDark = "Dark";

    public static String orientationL = "Legible";
    public static String orientationNL = "NoLegible";

    public static String fileTypePDF = "PDF";
    public static String fileTypeJpegTiff = "JpegTiff";

    public static String boxItemTypePDF = "PDF";
    public static String boxItemTypeTIFF = "TIFF";
    public static String boxItemTypeFolder = "FOLDER";

    public final static String GUEST_USER = "GUEST_USER";

}

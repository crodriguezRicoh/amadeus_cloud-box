package es.ricoh.scantobox.util;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import es.ricoh.scantobox.R;

/**
 * Created by cristian.rodriguez
 * Checked: OK!
 */
public class CustomToast {

    public static void showCustomAlert(Context context, String msg)
    {
        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View toastRoot = inflate.inflate(R.layout.toast, null);

        ((TextView)toastRoot.findViewById(R.id.texto)).setText(msg);
        Toast toast = new Toast(context);

        // Set layout to toast
        toast.setView(toastRoot);
        toast.setGravity(Gravity.CENTER_HORIZONTAL,0, -220);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();

    }

    public static void showCustomDownAlert(Context context, String msg)
    {
        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View toastRoot = inflate.inflate(R.layout.toast, null);

        ((TextView)toastRoot.findViewById(R.id.texto)).setText(msg);
        Toast toast = new Toast(context);

        // Set layout to toast
        toast.setView(toastRoot);
        //toast.setGravity(Gravity.CENTER_HORIZONTAL,0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();

    }
}

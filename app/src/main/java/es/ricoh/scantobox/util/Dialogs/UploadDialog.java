package es.ricoh.scantobox.util.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import es.ricoh.scantobox.R;

/**
 * Created by cristian.rodriguez on 18/01/2017.
 */

public class UploadDialog {

    static TextView _upload_fileName;
    static TextView _upload_size;
    static TextView _upload_status;
    static ProgressBar _progress_upload;
    static TextView _upload_porciento;

    public static Dialog createUploadConfirmationDialog(final Context context) {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_upload);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final RelativeLayout mainLayout = (RelativeLayout) dialog.findViewById(R.id.upload_infoBox);

        Button cancel = (Button) dialog.findViewById(R.id.btn_cancelUpload);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        _upload_fileName  = (TextView) mainLayout.findViewById(R.id.upload_fileName);
        _upload_size  = (TextView) mainLayout.findViewById(R.id.upload_size);
        _upload_status  = (TextView) mainLayout.findViewById(R.id.upload_status);
        _progress_upload  = (ProgressBar) mainLayout.findViewById(R.id.progress_upload);
        _upload_porciento  = (TextView) mainLayout.findViewById(R.id.upload_porciento);

        _progress_upload.setMax(100);

        return dialog;
    }

    public static void updateFileNameAndSize(String upload_fileName, Long upload_size){
        _upload_fileName.setText(upload_fileName);
        _upload_size.setText(String.valueOf(upload_size) + "Kb");
    }

    public static void updateStatus(String upload_status){
        _upload_status.setText(upload_status);
    }

    public static void updateProgressAndPercent(int progress_upload, int upload_porciento){
        _progress_upload.setProgress(progress_upload);
        _upload_porciento.setText(String.valueOf(upload_porciento) + "%");
    }
}

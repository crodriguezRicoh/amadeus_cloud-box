package es.ricoh.scantobox.util;


public class Preferences {

    //scan
    public static boolean isColor = false;
    public static int resolutionDPI = 200;
    public static String size = Constants.sizeA4L;
    public static String density = Constants.densityAuto;
    public static String orientation = Constants.orientationNL;
    public static boolean duplex = false;
    public static String fileType = Constants.fileTypePDF;

    //print
    public static boolean print_isColor = false;
    public static int print_resolutionDPI = 300;
    public static boolean print_duplex = true;

    //general
    public static String doctypeId = "";
    public static String doctypeDescription = "";
    public static String basename = "";
    public static String serialNumber = "";

    //Settings
    public static String clientID = "";
    public static String clientSecret = "";
    public static String redirectURL = "";
    public static String oauthURL = "";
}

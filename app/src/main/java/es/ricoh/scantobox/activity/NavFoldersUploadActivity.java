package es.ricoh.scantobox.activity;

import android.app.Activity;
import android.content.Context;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.beans.BeanBoxItem;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.BoxApiManager;
import es.ricoh.scantobox.util.BoxItemAdapter;
import es.ricoh.scantobox.util.Constants;
import es.ricoh.scantobox.util.Dialogs.BeanBoxApiException;
import es.ricoh.scantobox.util.Dialogs.DialogUtil;
import es.ricoh.scantobox.util.Utils;


public class NavFoldersUploadActivity extends Activity {

    public static final int REQUEST_CODE_UPLOAD_ACTIVITY = 1;
    private static final Logger log = ALogger.getLogger(NavFoldersUploadActivity.class);
    private Button btnBack;
    private Button btnUpload;
    private RelativeLayout btnFolderUp;
    private TextView txtBreadcrumbs;
    private TextView txtLoggedUser;
    private String parentFolderID = "0";
    private String selectedItemID = "0";

    private static NavFoldersUploadActivity mContext;
    private ListView listViewItems;
    BoxItemAdapter adapter = null;
    private ArrayList<File> filesToUpload;

    private Dialog progressDialog = null;
    private Dialog dialogErrorUser = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.info("***** NavFoldersUploadActivity onCreate *****");

        setContentView(R.layout.activity_nav_folders_upload);

        mContext = this;

        filesToUpload = (ArrayList<File>) getIntent().getSerializableExtra("filesToUpload");


        btnBack = (Button) findViewById(R.id.btnBackFromNavFoldersUpload);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                log.debug("***** onClick BackFromScanToMain *****");
                finish();
            }
        });


        btnFolderUp = (RelativeLayout) findViewById(R.id.btnFolderUp);
        btnFolderUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedItemID = parentFolderID;
                showFolderItems(parentFolderID);
            }
        });


        btnUpload = (Button) findViewById(R.id.btnNextFromNavFoldersUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                log.debug("***** onClick btnUpload *****");

                Intent intent = new Intent(NavFoldersUploadActivity.this, UploadActivity.class);
                intent.putExtra("itemID", selectedItemID);
                intent.putExtra("filesToUpload", filesToUpload);
                startActivity(intent);
            }
        });

        txtBreadcrumbs = (TextView) findViewById(R.id.txtBreadCrumbsUpload);
        txtLoggedUser = (TextView) findViewById(R.id.txtLoggedUser);
        txtLoggedUser.setText(MainActivity.loginUser);

    }

    protected void onResume() {
        super.onResume();

        log.info("***** NavFoldersUploadActivity onResume *****");

        if (SplashActivity.currentUser == null || SplashActivity.currentUser.isEmpty()) {
            log.warn("Current user is = " + SplashActivity.currentUser);
            log.warn("Forcing to finish");
            showErrorMsg();

        } else if (filesToUpload == null || filesToUpload.isEmpty()) {
            log.warn("Files to upload is empty or null");
            log.warn("Forcing to finish");
            showErrorMsg();

        } else {
            parentFolderID = "0";
            selectedItemID = "0";

            showFolderItems(selectedItemID);
        }

    }


    //  Rellena el listView con los Items de la carpeta "folderID" (carpeta raíz si folderID es "")
    private void showFolderItems(final String folderID) {

        //  Nada mas entrar mostrar diálogo "Cargando..." mientras recuperamos los items de boxAPI
        progressDialog = DialogUtil.createProcessWaitDialog(this, R.layout.dlg_cargando_boxapi);
        DialogUtil.showDialog(progressDialog, 540, 290);

        new Thread(new Runnable() {

            String strBreadcrumbs = "";

            @Override
            public void run() {

                List boxItems = null;
                BoxFolder folder = null;
                boolean allOK = false;

                try {
                    GetFolderItemsTask folderItems = new GetFolderItemsTask();
                    folder = folderItems.execute(folderID).get();

                    if (folder != null) {

                        if (folder.getInfo().getParent() == null) {
                            parentFolderID = "0";
                        } else {
                            parentFolderID = folder.getInfo().getParent().getID();
                        }
                        log.debug("parentFolderID: " + parentFolderID);
                        strBreadcrumbs = getFullPath(folder.getInfo());

                        boxItems = new ArrayList<BeanBoxItem>();

                        for (BoxItem.Info itemInfo : folder) {

                            if (itemInfo instanceof BoxFolder.Info) {
                                BoxFolder.Info folderInfo = (BoxFolder.Info) itemInfo;
                                // Do something with the folder.
                                boxItems.add(new BeanBoxItem(R.drawable.icon_folder, folderInfo.getID(), folderInfo.getName(), Constants.boxItemTypeFolder));
                            }
                        }

                        adapter = new BoxItemAdapter(mContext, R.layout.listview_boxitem_row, boxItems);
                        allOK = true;
                    }

                } catch (Exception e) {
                    allOK = false;
                    Utils.printStackTraceToString(e);
                }


                final boolean finalAllOK = allOK;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (finalAllOK) {

                            // Put the data into the list
                            listViewItems = (ListView)findViewById(R.id.listViewItemsUpload);
                            listViewItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    BeanBoxItem selectedItem = (BeanBoxItem) parent.getItemAtPosition(position);

                                    if (selectedItem == null) {
                                        selectedItemID = null;

                                    } else if (selectedItem.type.equalsIgnoreCase(Constants.boxItemTypeFolder)) {
                                        //  Si el item seleccionado es FOLDER
                                        selectedItemID = selectedItem.itemId;
                                        showFolderItems(selectedItem.itemId);
                                    }

                                }
                            });
                            listViewItems.setAdapter(adapter);
                            txtBreadcrumbs.setText(strBreadcrumbs);
                            progressDialog.dismiss();   //  TODO: pdte controlar que si hay algun error no se quede el progressDialog abierto
                        } else {
                            progressDialog.dismiss();
                            //  Mostramos un mensaje de error y volvemos al splash
                            showErrorMsg();
                        }
                    }
                });
            }
        }).start();

    }


    class GetFolderItemsTask extends AsyncTask<String, Void, BoxFolder> {

        @Override
        protected BoxFolder doInBackground(String... params) {

            BoxFolder folder = null;

            int nRetry = 0;
            while (nRetry < 3) {
                log.debug("-> retry " + nRetry);

                try {
                    log.info("GetFolderItems()");
                    String folderID = params[0];
                    if (folderID == null) {
                        //  Do nothing
                    } else if (folderID.isEmpty()) {
                        //  Obtenemos la carpeta raíz
                        folder = BoxFolder.getRootFolder(BoxApiManager.getBoxApi(SplashActivity.currentUser));
                    } else {
                        folder = new BoxFolder(BoxApiManager.getBoxApi(SplashActivity.currentUser), folderID);
                    }

                    break;

                } catch (BoxAPIException e) {
                    //  TODO: for test
                    log.error("********* BoxAPIException *********");
                    log.error("*** Cause: " + e.getCause());

                    BeanBoxApiException bean = Utils.getBoxAPIExceptionInfo(e);
                    log.debug("status: " + bean.status);
                    log.debug("code: " + bean.code);
                    log.debug("message: " + bean.message);

                    log.error(Utils.printStackTraceToString(e));

                    if (e.getCause() instanceof IOException) {
                        nRetry++;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            log.error(Utils.printStackTraceToString(ie));
                        }
                    } else {
                        break;
                    }
                } catch (Exception e) {
                    log.error(Utils.printStackTraceToString(e));
                    break;
                }
            }

            return folder;
        }
    }

    private String getFullPath(BoxItem.Info info) {
        try {
            log.debug("getFullPath()");
            List<BoxFolder.Info> filePathList = info.getPathCollection();
            StringBuffer fullPathStringBuffer = new StringBuffer("");
            for (BoxFolder.Info pf : filePathList) {
                fullPathStringBuffer.append("/").append(pf.getName());
            }
            fullPathStringBuffer.append("/" + info.getName());
            log.info("************* " + fullPathStringBuffer.toString());

            //  TODO: si la longitud del path es mayor que el ancho de la pantalla, poner "..." o algo similar
            return fullPathStringBuffer.toString();
        } catch(Exception e) {
            log.error(Utils.printStackTraceToString(e));
            return "";
        }
    }

    public static Context getAppContext() {
        return mContext;
    }

    public void showErrorMsg(){

        String msgError = mContext.getResources().getString(R.string.txt_error_box_user_msg);

        dialogErrorUser = DialogUtil.createErrorDialog(mContext, msgError);
        dialogErrorUser.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                mContext.gotoSplash();
            }
        });
        dialogErrorUser.show();
    }

    public void gotoSplash(){
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (dialogErrorUser != null) {
            dialogErrorUser.dismiss();
        }
    }

    @Override
    protected void onRestart() {
        log.debug("***** NavFoldersUploadActivity onRestart *****");
        super.onRestart();

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (dialogErrorUser != null) {
            dialogErrorUser.dismiss();
        }
    }

    protected void onDestroy() {
        log.debug("***** NavFoldersUploadActivity onDestroy *****");
        super.onDestroy();

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (dialogErrorUser != null) {
            dialogErrorUser.dismiss();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        }
        return false;
    }

}
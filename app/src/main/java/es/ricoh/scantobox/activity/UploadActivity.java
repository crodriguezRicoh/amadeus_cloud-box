package es.ricoh.scantobox.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFolder;
import com.box.sdk.ProgressListener;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.beans.BeanUploadTask;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.BoxApiManager;
import es.ricoh.scantobox.util.Dialogs.BeanBoxApiException;
import es.ricoh.scantobox.util.Dialogs.DialogUtil;
import es.ricoh.scantobox.util.Preferences;
import es.ricoh.scantobox.util.Utils;


/**
 * Created by cristian.rodriguez on 15/03/2017.
 * Checked: OK!
 */

public class UploadActivity extends Activity {

    public static final int REQUEST_CODE_UPLOAD_ACTIVITY = 95;
    private static final Logger log = ALogger.getLogger(UploadActivity.class);
    private static Context context;
    private static UploadActivity mContext;

    private TextView upload_status;
    private TextView upload_porciento;
    private TextView upload_size;
    private TextView upload_fileName;
    private TextView upload_response;
    private TextView upload_response_result;
    private Button btnBack;
    private Button btnFinish;

    String itemID;
    private TextView txtLoggedUser;
    private ArrayList<File> filesToUpload;
    UploadFileTask uploadFileTask = null;
    public static boolean isActive = true;

    static Dialog dialogResultUpload = null;
    private Dialog dialogErrorUser = null;

    public TimerTask lockTimeOutTask;
    public FileInputStream fis = null;
    public BoxFolder folder = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.info("***** UploadActivity onCreate *****");

        try {
            setContentView(R.layout.activity_upload);
            mContext = this;
            UploadActivity.context = getApplicationContext();
            isActive = true;

            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            itemID = extras.getString("itemID");
            filesToUpload = (ArrayList<File>) getIntent().getSerializableExtra("filesToUpload");

            btnBack = (Button) findViewById(R.id.btnBackFromUpload);
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    log.debug("***** onClick BackFromScanToMain *****");
                    Utils.UnLock_logout(context, getPackageName());
                    if(lockTimeOutTask != null) {
                        lockTimeOutTask.cancel();
                    }
                    finish();
                }
            });

            btnFinish = (Button) findViewById(R.id.btnNextFromUpload);
            btnFinish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    log.debug("***** onClick BackFromScanToMain *****");
                    Utils.UnLock_logout(context, getPackageName());
                    if(lockTimeOutTask != null) {
                        lockTimeOutTask.cancel();
                    }
                    gotoMain();
                }
            });
            btnFinish.setEnabled(false);

            upload_status = (TextView) findViewById(R.id.upload_status);
            upload_status.setText(context.getResources().getString(R.string.txt_upload_proceso1));
            upload_porciento = (TextView) findViewById(R.id.upload_porciento);
            upload_porciento.setText("0%");
            upload_size = (TextView) findViewById(R.id.upload_size);
            upload_size.setText("0 Kb");
            upload_fileName = (TextView) findViewById(R.id.upload_fileName);
            upload_fileName.setText("");
            upload_response = (TextView) findViewById(R.id.upload_response);
            upload_response.setText("");
            upload_response_result = (TextView) findViewById(R.id.upload_response_result);
            upload_response_result.setText("");

            txtLoggedUser = (TextView) findViewById(R.id.txtLoggedUser);
            txtLoggedUser.setText(MainActivity.loginUser);

            uploadFileTask = new UploadFileTask();
            uploadFileTask.execute(filesToUpload);

            lockTimeOutTask = new TimerTask() {
                @Override
                public void run() {
                    log.debug("Lanzando lockTimeOutTask");
                    Utils.UnLock_logout(mContext, getPackageName());
                }
            };

        } catch (NullPointerException ex) {
            log.error(Utils.printStackTraceToString(ex));
            finish();
        }
    }

    public void showMSG_Upload_Process(){

        String msgError = mContext.getResources().getString(R.string.txt_error_upload_msg);

        dialogResultUpload = DialogUtil.createErrorDialog(mContext, msgError);
        dialogResultUpload.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //mContext.gotoSplash();
            }
        });
        dialogResultUpload.show();

    }

    public void gotoMain(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void gotoSplash(){
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void showErrorMsg(){

        String msgError = mContext.getResources().getString(R.string.txt_error_box_user_msg);

        dialogErrorUser = DialogUtil.createErrorDialog(mContext, msgError);
        dialogErrorUser.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                mContext.gotoSplash();
            }
        });
        dialogErrorUser.show();
    }

    private class UploadFileTask extends AsyncTask<ArrayList<File>, BeanUploadTask, Boolean> {

        UploadActivity activity;
        boolean procesoUpload;
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.progress_upload);

        int totalSize = 0;
        int currentSize = 0;
        int accSize = 0;
        int maxSizeUploaded = 0;

        int nRetry = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            procesoUpload = true;
            activity = UploadActivity.this;

            upload_response.setText("");
            upload_status.setText(context.getResources().getString(R.string.txt_upload_proceso1));
            upload_porciento.setText("0%");
            upload_size.setText("");
            upload_fileName.setText("");

            //UnLock APP
            Utils.Lock_logout(activity, getPackageName());
        }

        @Override
        protected Boolean doInBackground(ArrayList<File>... params) {

            log.debug("***** Lanzando proceso UPLOAD TASK *****");

            final BeanUploadTask bUpTask = new BeanUploadTask();

            try {
                log.info("UploadFiles()");
                ArrayList<File> files = params[0];

                folder = new BoxFolder(BoxApiManager.getBoxApi(SplashActivity.currentUser), itemID);

                //Contamos el tamaño total de los ficheros
                for (File file: files) {
                    totalSize += file.length();
                }

                log.debug("Total SIZE: " + totalSize);

                //Total ficheros
                bUpTask.numFiles = files.size();

                int posFichero = 1;

                String timeStamp = getTimeStamp();

                for (File file: files) {

                    if(!isActive){
                        return false;
                    }

                    nRetry = 0;
                    while (nRetry < 3) {

                        procesoUpload = true;

                        log.debug("-> retry " + nRetry);
                        //FileInputStream fis = null;

                        try {

                            bUpTask.upload_fileName = file.getName();
                            bUpTask.upload_status = context.getResources().getString(R.string.txt_upload_proceso2);
                            bUpTask.fileIndex = posFichero;

                            final ProgressListener listener = new ProgressListener() {
                                public void onProgressChanged(long numBytes, long totalBytes) {
                                    //log.debug("numBytes: " + numBytes);
                                    //log.debug("totalBytes: " + totalBytes);

                                    currentSize = (int)numBytes;

                                    currentSize += accSize;

                                    if(maxSizeUploaded < currentSize) {
                                        maxSizeUploaded = currentSize;
                                    }

                                    publishProgress(bUpTask);

                                }};

                            accSize = currentSize;

                            fis = new FileInputStream(file);

                            if(file.getName().contains(".tif")) {
                                folder.uploadFile(fis, timeStamp + ".tiff", file.length(), listener);
                            }else if(file.getName().contains(".pdf")){
                                folder.uploadFile(fis, timeStamp + ".pdf", file.length(), listener);
                            }else {
                                folder.uploadFile(fis, timeStamp + "_" + file.getName(), file.length(), listener);
                            }

                            //folder.uploadFile(fis, file.getName(), file.length(), listener);
                            break;

                        } catch (BoxAPIException e) {

                            procesoUpload = false;
                            currentSize = 0;

                            //  TODO: for test
                            log.error("********* BoxAPIException *********");
                            log.error("*** Cause: " + e.getCause());

                            BeanBoxApiException bean = Utils.getBoxAPIExceptionInfo(e);
                            log.debug("status: " + bean.status);
                            log.debug("code: " + bean.code);
                            log.debug("message: " + bean.message);

                            log.error(Utils.printStackTraceToString(e));

                            if (e.getCause() instanceof IOException) {
                                nRetry++;
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException ie) {
                                    log.error(Utils.printStackTraceToString(ie));
                                }
                            } else {
                                break;
                            }

                        } catch (Exception e) {
                            log.error(Utils.printStackTraceToString(e));
                            procesoUpload = false;
                            break;
                        }finally {
                            if(fis != null) {
                                fis.close();
                            }
                        }
                    }

                    posFichero ++;

                    if(file.getName().contains(".tif")) {
                        bUpTask.upload_response += timeStamp + ".tiff" + "\n";
                    }else if(file.getName().contains(".pdf")){
                        bUpTask.upload_response += timeStamp + ".pdf" + "\n";
                    }else {
                        bUpTask.upload_response += timeStamp + "_" + file.getName() + "\n";
                    }

                    if(procesoUpload){

                        /*if(file.getName().contains(".tif")) {
                            bUpTask.upload_response += timeStamp + ".tiff" +"\t>\t"+ context.getResources().getString(R.string.txt_upload_proceso_correcto) +"\n";
                        }else if(file.getName().contains(".pdf")){
                            bUpTask.upload_response += timeStamp + ".pdf" +"\t>\t"+ context.getResources().getString(R.string.txt_upload_proceso_correcto) +"\n";
                        }else {
                            bUpTask.upload_response += timeStamp + "_" + file.getName() +"\t>\t"+ context.getResources().getString(R.string.txt_upload_proceso_correcto) +"\n";
                        }*/

                        bUpTask.upload_response_result += ">\t"+ context.getResources().getString(R.string.txt_upload_proceso_correcto) +"\n";

                    }else{

                        /*if(file.getName().contains(".tif")) {
                            bUpTask.upload_response += timeStamp + ".tiff" +"\t>\t"+ context.getResources().getString(R.string.txt_upload_proceso_error) +"\n";
                        }else if(file.getName().contains(".pdf")){
                            bUpTask.upload_response += timeStamp + ".pdf" +"\t>\t"+ context.getResources().getString(R.string.txt_upload_proceso_error) +"\n";
                        }else {
                            bUpTask.upload_response += timeStamp + "_" + file.getName() +"\t>\t"+ context.getResources().getString(R.string.txt_upload_proceso_error) +"\n";
                        }*/

                        bUpTask.upload_response_result += ">\t"+ context.getResources().getString(R.string.txt_upload_proceso_error) +"\n";
                    }

                    publishProgress(bUpTask);
                }

                return true;

            } catch (BoxAPIException e) {

                procesoUpload = false;

                //  TODO: for test
                log.error("********* BoxAPIException *********");
                log.error("*** Cause: " + e.getCause());

                BeanBoxApiException bean = Utils.getBoxAPIExceptionInfo(e);
                log.debug("status: " + bean.status);
                log.debug("code: " + bean.code);
                log.debug("message: " + bean.message);

                log.error(Utils.printStackTraceToString(e));
            } catch(Exception e) {
                procesoUpload = false;
                log.error(Utils.printStackTraceToString(e));
            }

            log.debug("Proceso UPLOAD TASK ha finalizado!");

            return true;
        }

        @Override
        protected void onProgressUpdate(BeanUploadTask... bUpTask) {
            super.onProgressUpdate(bUpTask);

            try {

                float process = ((float) maxSizeUploaded / (float)totalSize) * 100;

                //File INDEX
                upload_fileName.setText(bUpTask[0].fileIndex + " " + context.getResources().getString(R.string.txt_of) + " " + bUpTask[0].numFiles);

                //Total FILES Size
                upload_size.setText(Utils.formatFileSize(totalSize));

                //ProgressBar Size and current Size
                progressBar.setMax((int)totalSize);
                progressBar.setProgress(maxSizeUploaded);

                //Porciento
                upload_porciento.setText(Double.toString(Math.floor(process)) + "%");

                //Status
                upload_status.setText(bUpTask[0].upload_status + " (" + Utils.formatFileSize(maxSizeUploaded) + ")");

                //File RESULT Response
                upload_response.setText(bUpTask[0].upload_response);
                upload_response_result.setText(bUpTask[0].upload_response_result);

            }catch (Exception ex){
                procesoUpload = false;
                log.error(Utils.printStackTraceToString(ex));
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            log.info("***** POST EXECUTE *****");

            try {
                if (!procesoUpload) {
                    upload_status.setText(context.getResources().getString(R.string.txt_upload_proceso_error));
                    showMSG_Upload_Process();
                    btnFinish.setEnabled(true);

                } else {
                    upload_status.setText(context.getResources().getString(R.string.txt_upload_proceso3));
                    //upload_porciento.setText("100%");
                    btnFinish.setEnabled(true);
                }

            }catch (Exception ex){
                log.warn("ATENCIÓN: No se puede mostrar el popUp de descarga, porque el proceso ha sido cancelado....");
            }finally {

                if(fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try{
                    Timer timer = new Timer();
                    timer.schedule(activity.lockTimeOutTask, 30000);
                }catch (Exception ex){
                    Utils.printStackTraceToString(ex);
                }
            }
        }
    }

    public static String getTimeStamp() {

        GregorianCalendar gc = new GregorianCalendar();
        String retValue = "";

        retValue += gc.get(Calendar.YEAR);

        int i = gc.get(Calendar.MONTH) + 1;
        retValue += (i < 10 ? "0" + i : Integer.toString(i));

        i = gc.get(Calendar.DAY_OF_MONTH);
        retValue += (i < 10 ? "0" + i : Integer.toString(i));

        retValue += "-";

        i = gc.get(Calendar.HOUR_OF_DAY);
        retValue += (i < 10 ? "0" + i : Integer.toString(i));

        i = gc.get(Calendar.MINUTE);
        retValue += (i < 10 ? "0" + i : Integer.toString(i));

        i = gc.get(Calendar.SECOND);
        retValue += (i < 10 ? "0" + i : Integer.toString(i));

        /*i = gc.get(Calendar.MILLISECOND);
        if(i < 10){
            retValue += (i < 10 ? "00" + i : Integer.toString(i));
        }else{
            retValue += (i < 100 ? "0" + i : Integer.toString(i));
        }*/

        retValue += "_" + Preferences.serialNumber;

        log.debug("Generando TIMESTAMP: " + retValue);

        return retValue;

    }

    void deleteInside(File f) {

        //TODO Borramos todos los ficheros escaneados...
        log.debug("Lanzando borrado recursivo...");

        if (f.exists()) {
            if (f.isDirectory()) {
                for (File child : f.listFiles()) {
                    child.delete();
                }
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        }
        return false;
    }

    @Override
    protected void onResume() {
        log.debug("***** PrintActivity onResume *****");
        super.onResume();

        if (SplashActivity.currentUser == null || SplashActivity.currentUser.isEmpty()) {
            log.warn("Current user is = " + SplashActivity.currentUser);
            log.warn("Forcing to finish");
            showErrorMsg();
        }

        isActive = true;
    }

    @Override
    protected void onPause() {
        log.debug("***** PrintActivity onPause *****");
        super.onPause();

        if (dialogErrorUser != null) {
            dialogErrorUser.dismiss();
        }

        if (dialogResultUpload != null) {
            dialogResultUpload.dismiss();
        }

        Utils.UnLock_logout(context, getPackageName());
        if(lockTimeOutTask != null) {
            lockTimeOutTask.cancel();
        }

        log.debug("---> DETENIENDO LA TASK... <---");
        isActive = false;
        uploadFileTask.cancel(true);

        if(fis != null) {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        folder = null;
    }

    @Override
    protected void onDestroy() {
        log.debug("***** PrintActivity onDestroy *****");
        super.onDestroy();

        if (dialogErrorUser != null) {
            dialogErrorUser.dismiss();
        }

        if (dialogResultUpload != null) {
            dialogResultUpload.dismiss();
        }
    }

    public static Context getAppContext() {
        return UploadActivity.context;
    }

}

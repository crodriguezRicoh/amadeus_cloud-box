package es.ricoh.scantobox.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.log4j.Logger;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.BoxApiManager;
import es.ricoh.scantobox.util.Dialogs.DialogUtil;
import es.ricoh.scantobox.util.Utils;

public class MainActivity extends Activity {

    public static final int REQUEST_CODE_MAIN_ACTIVITY = 1;
    private static final Logger log = ALogger.getLogger(MainActivity.class);
    private RelativeLayout btnScan;
    private RelativeLayout btnPrint;
    private Button btnLogout;
    private TextView txtLoggedUser;
    Intent intent;
    Dialog dialogConfig = null;
    Dialog dialogLogoutConfirmation = null;
    public static String loginUser = "";
    private static MainActivity mContext;

    private Dialog dialogErrorUser = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.debug("***** MainActivity onCreate *****");

        try{
            setContentView(R.layout.activity_main);
            mContext = this;

            try {
                getWindow().addFlags(WindowManager.LayoutParams.class.getField("FLAG_NEEDS_MENU_KEY").getInt(null));
            } catch (IllegalAccessException e) {
                log.error(Utils.printStackTraceToString(e));
            } catch (NoSuchFieldException e) {
                log.error(Utils.printStackTraceToString(e));
            }

            txtLoggedUser = (TextView) findViewById(R.id.txtLoggedUser);

            btnScan = (RelativeLayout) findViewById(R.id.btnMainScan);
            btnScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    log.debug("***** MainActivity onClick SCAN *****");

                    intent = new Intent(MainActivity.this, ScanActivity.class);
                    startActivity(intent);

                }
            });

            btnPrint = (RelativeLayout) findViewById(R.id.btnMainPrint);
            btnPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    log.debug("***** MainActivity onClick PRINT *****");

                    intent = new Intent(MainActivity.this, NavFoldersPrintActivity.class);
                    startActivity(intent);
                }
            });

            btnLogout = (Button) findViewById(R.id.btnLogout);
            btnLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    log.debug("***** MainActivity onClick LOGOUT *****");
                    showdialogLogoutConfirmation();
                }
            });

        } catch (Exception ex) {
            log.error(Utils.printStackTraceToString(ex));
            finish();
        }
    }

    public void showErrorMsg(){

        String msgError = mContext.getResources().getString(R.string.txt_error_box_user_msg);

        dialogErrorUser = DialogUtil.createErrorDialog(mContext, msgError);
        dialogErrorUser.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                mContext.gotoSplash();
            }
        });
        dialogErrorUser.show();
    }

    public void gotoSplash(){
        /*Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/

        finish();
    }

    private void showdialogLogoutConfirmation() {
        log.debug("showdialogLogoutConfirmation");

        dialogLogoutConfirmation = new Dialog(mContext);
        dialogLogoutConfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLogoutConfirmation.setContentView(R.layout.dialog_logout_confirmation);
        dialogLogoutConfirmation.setCancelable(false);
        dialogLogoutConfirmation.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        Button btnCancel = (Button) dialogLogoutConfirmation.findViewById(R.id.btn_logout_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                log.debug("btnCancel onClick");
                dialogLogoutConfirmation.dismiss();
            }
        });

        Button btnAccept = (Button) dialogLogoutConfirmation.findViewById(R.id.btn_logout_accept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                log.debug("btnAccept onClick");
                BoxApiManager.removeUserApi(SplashActivity.currentUser);
                dialogLogoutConfirmation.dismiss();
                finish();
            }
        });

        dialogLogoutConfirmation.show();
    }

    protected void onResume() {
        super.onResume();
        log.debug("***** MainActivity onResume *****");

        if (SplashActivity.currentUser == null || SplashActivity.currentUser.isEmpty()) {
            log.warn("Current user is = " + SplashActivity.currentUser);
            log.warn("Forcing to finish");
            //finish();
            showErrorMsg();
        }

        loginUser = BoxApiManager.getBoxLogin(SplashActivity.currentUser);
        txtLoggedUser.setText(loginUser);
    }

    protected void onDestroy() {
        super.onDestroy();
        log.debug("***** MainActivity onDestroy *****");
        loginUser = "";

        if (dialogErrorUser != null) {
            dialogErrorUser.dismiss();
        }
        
        if (dialogLogoutConfirmation != null) {
            dialogLogoutConfirmation.dismiss();
        }

        if (dialogConfig != null) {
            dialogConfig.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        log.debug("***** MainActivity onPause *****");

        if (dialogErrorUser != null) {
            dialogErrorUser.dismiss();
        }

        if (dialogLogoutConfirmation != null) {
            dialogLogoutConfirmation.dismiss();
        }

        if (dialogConfig != null) {
            dialogConfig.dismiss();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                DialogUtil.createAboutDialog(this, SplashActivity.about).show();
                return true;

            case R.id.config:
                dialogConfig = DialogUtil.createLoginDialog(this, 1);
                dialogConfig.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void launchConfig() {
        Intent intent = new Intent(MainActivity.this, ConfigActivity.class);
        startActivityForResult(intent, ConfigActivity.REQUEST_CODE_CONFIG_ACTIVITY);

        if (dialogConfig != null) {
            dialogConfig.dismiss();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        }
        return false;
    }
}
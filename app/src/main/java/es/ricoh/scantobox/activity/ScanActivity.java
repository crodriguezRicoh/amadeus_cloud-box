package es.ricoh.scantobox.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimerTask;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.application.PrintServiceAttributeListenerImpl;
import es.ricoh.scantobox.application.PrintSettingDataHolder;
import es.ricoh.scantobox.application.PrintSettingSupportedHolder;
import es.ricoh.scantobox.application.PrintStateMachine;
import es.ricoh.scantobox.application.ScanProcess;
import es.ricoh.scantobox.application.ScanStateMachine;
import es.ricoh.scantobox.application.ScanToBoxApplication;
import es.ricoh.scantobox.beans.BeanDocType;
import es.ricoh.scantobox.beans.BeanResponseWS;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Constants;
import es.ricoh.scantobox.util.Dialogs.DensityDialog;
import es.ricoh.scantobox.util.Dialogs.DialogUtil;
import es.ricoh.scantobox.util.Dialogs.DoctypeDialog;
import es.ricoh.scantobox.util.Dialogs.OrientationDialog;
import es.ricoh.scantobox.util.Dialogs.SideDialog;
import es.ricoh.scantobox.util.Dialogs.SizeDialog;
import es.ricoh.scantobox.util.Preferences;
import es.ricoh.scantobox.util.Utils;
import jp.co.ricoh.ssdk.function.Const;
import jp.co.ricoh.ssdk.function.application.ScanSettingDataHolder;
import jp.co.ricoh.ssdk.function.application.SystemStateMonitor;
import jp.co.ricoh.ssdk.function.common.SmartSDKApplication;
import jp.co.ricoh.ssdk.function.common.impl.AsyncConnectState;
import jp.co.ricoh.ssdk.function.print.PrintFile;
import jp.co.ricoh.ssdk.function.print.PrintService;
import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttribute;
import jp.co.ricoh.ssdk.function.print.attribute.standard.Copies;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PaperTray;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintColor;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintResolution;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintSide;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterState;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterStateReasons;
import jp.co.ricoh.ssdk.function.print.event.PrintServiceAttributeListener;
import jp.co.ricoh.ssdk.function.scan.ScanService;
import jp.co.ricoh.ssdk.function.scan.attribute.ScanServiceAttributeSet;
import jp.co.ricoh.ssdk.function.scan.attribute.standard.OccuredErrorLevel;
import jp.co.ricoh.ssdk.function.scan.attribute.standard.ScannerState;
import jp.co.ricoh.ssdk.function.scan.attribute.standard.ScannerStateReason;
import jp.co.ricoh.ssdk.function.scan.attribute.standard.ScannerStateReasons;
import jp.co.ricoh.ssdk.function.scan.event.ScanServiceAttributeEvent;
import jp.co.ricoh.ssdk.function.scan.event.ScanServiceAttributeListener;

/**
 * Created by cristian.rodriguez on 27/01/2017.
 * Checked: OK!
 */

public class ScanActivity extends Activity {

    private static final Logger log = ALogger.getLogger(ScanActivity.class);
    private static Context context;

    public static ScanActivity scanActivity;

    private final static String ALERT_DIALOG_APP_TYPE_SCANNER = "SCANNER";

    //Application object
    private static ScanToBoxApplication mApplication;

    //Broadcast receiver to accept intents from setting dialog
    private BroadcastReceiver mReceiver;

    //Scan start button
    private RelativeLayout mButtonStart;

    //Scan service attribute listener
    private ScanServiceAttributeListener mScanServiceAttrListener;

    //Scan setting
    private ScanSettingDataHolder mScanSettingDataHolder;

    //State machine
    private ScanStateMachine mStateMachine;

    //Task to connect with scan service
    private ScanServiceInitTask mScanServiceInitTask;

    //Scan service state display label
    private TextView text_state;

    //Flag to indicate if system warning screen is displayed
    private volatile boolean mAlertDialogDisplayed = false;

    //Level of the currently occurring error
    private OccuredErrorLevel mLastErrorLevel = null;

    //Asynchronous task to request to display system warning screen
    private AlertDialogDisplayTask mAlertDialogDisplayTask = null;

    //Return request from energy saving task
    private ReturnRequestFromEnergySavingTask mReturnRequestFromEnergySavingTask = null;

    //During onResume and onPause, it should be true.
    private boolean mIsForeground = false;

    /**
     * Request locking system auto reset flag from state machine.
     * When request to lock system auto reset, it is changed to true.
     * When lock is required and MainAcitivity is in foreground, set system auto reset of SDKService.
     */
    private boolean mRequestedSystemResetLock = false;

    //Define the prefix for log information with abbreviation package and class name
    private final static String PREFIX = "activity:ScanActivity:";

    public static ScanActivity mContext;
    private TextView txt_orientation;
    private ImageView img_orientation;
    private TextView txt_duplex;
    private ImageView img_duplex;
    private TextView txt_color;
    private TextView txt_filetype;
    private TextView txt_resolution;
    private TextView txt_paperSize;
    private TextView txt_density;
    private TextView txtLoggedUser;
    private int previewRetry = 0;
    private int tmpPages = 1;

    public static String finalNameDate = "";
    public static ArrayList<String> finalNames;
    public static int printCopias = 0;

    private Dialog dialogErrorPreview = null;
    private Dialog dialogErrorUser = null;

    public TimerTask lockTimeOutTask;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.info("***** ScanActivity onCreate *****");

        try {
            setContentView(R.layout.activity_scan);
            mContext = this;
            ScanActivity.context = getApplicationContext();
            finalNameDate = "";

            Preferences.isColor = false;
            Preferences.resolutionDPI = 200;
            Preferences.size = Constants.sizeA4L;
            Preferences.density = Constants.densityAuto;
            Preferences.orientation = Constants.orientationNL;
            Preferences.duplex = false;
            Preferences.fileType = Constants.fileTypePDF;

            //(1)
            mApplication = (ScanToBoxApplication) getApplication();
            mScanServiceAttrListener = new ScanServiceAttributeListenerImpl(new Handler());
            mScanSettingDataHolder = mApplication.getScanSettingDataHolder();
            mStateMachine = mApplication.getStateMachine();
            mStateMachine.registActivity(this);
            text_state = (TextView) findViewById(R.id.text_state);

            //(2)
            IntentFilter filter = new IntentFilter();
            filter.addAction(DialogUtil.INTENT_ACTION_SUB_ACTIVITY_RESUMED);
            filter.addAction(ScanStateMachine.INTENT_ACTION_LOCK_SYSTEM_RESET);
            filter.addAction(ScanStateMachine.INTENT_ACTION_UNLOCK_SYSTEM_RESET);

            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (DialogUtil.INTENT_ACTION_SUB_ACTIVITY_RESUMED.equals(action)) {
                        startAlertDialogDisplayTask();
                    } else if (ScanStateMachine.INTENT_ACTION_LOCK_SYSTEM_RESET.equals(action)) {
                        mRequestedSystemResetLock = true;
                        processSystemResetLock();
                    } else if (ScanStateMachine.INTENT_ACTION_UNLOCK_SYSTEM_RESET.equals(action)) {
                        mRequestedSystemResetLock = false;
                        processSystemResetUnlock();
                    }
                }
            };
            registerReceiver(mReceiver, filter);

            //(3)
            mButtonStart = (RelativeLayout) findViewById(R.id.btn_start);
            mButtonStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    log.debug("***** onClick START *****");
                    //  Rreseteamos contador de reintentos y nº paginas
                    previewRetry = 0;
                    tmpPages = 1;

                    File f = new File("/mnt/hdd/" + mApplication.getApplicationContext().getPackageName() + "/scans/");
                    deleteRecursive(f);

                    mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.REQUEST_JOB_START);
                }
            });

            //(4)
            mButtonStart.setEnabled(true);

            //(5)
            if (mScanServiceInitTask != null) {
                mScanServiceInitTask.cancel(false);
            }
            mScanServiceInitTask = new ScanServiceInitTask();
            mScanServiceInitTask.execute();

            // send event
            mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.ACTIVITY_CREATED);

            // set application state to Normal
            mApplication.setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);

            txt_orientation = (TextView) findViewById(R.id.txt_orientation);
            img_orientation = (ImageView) findViewById(R.id.img_orientation);
            txt_duplex = (TextView) findViewById(R.id.txt_duplex);
            img_duplex = (ImageView) findViewById(R.id.img_duplex);
            txt_color = (TextView) findViewById(R.id.txt_color);
            txt_filetype = (TextView) findViewById(R.id.txt_filetype);
            txt_resolution = (TextView) findViewById(R.id.txt_resolution);
            txt_paperSize = (TextView) findViewById(R.id.txt_paperSize);
            txt_density = (TextView) findViewById(R.id.txt_density);

            txtLoggedUser = (TextView) findViewById(R.id.txtLoggedUser);
            txtLoggedUser.setText(MainActivity.loginUser);

            lockTimeOutTask = new TimerTask() {
                @Override
                public void run() {
                    log.debug("Lanzando lockTimeOutTask");
                    Utils.UnLock_logout(mContext, getPackageName());
                }
            };

            changeUI();

        }catch(NullPointerException ex){
            log.error(Utils.printStackTraceToString(ex));
            finish();
        }
    }

    public void pushBtnBackFromScan(View v){
        finish();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        log.debug("***** ScanActivity onActivityResult *****");

        String msgError = getResources().getString(R.string.txt_error_box_user_msg);

        //(1)
        if(requestCode == es.ricoh.scantobox.activity.PreviewActivity.REQUEST_CODE_PREVIEW_ACTIVITY) {
            if (resultCode == RESULT_OK) {

                log.debug("Resultado OK");
                /*int pages = 1;
                pages = intent.getIntExtra("pages", 1);
                dialogFinalizar fin = new dialogFinalizar();
                fin.execute(pages);*/
                callUploadActivity();
//            } else if (resultCode == RESULT_CANCELED) {
            } else if (resultCode == -9) {
                log.debug("Resultado CANCELED");
                File f = new File("/mnt/hdd/" + mApplication.getApplicationContext().getPackageName() + "/scans/");
                deleteRecursive(f);

            } else {

                log.debug("(1) Resultado KO");

                if (previewRetry < 2) {
                    //Toast.makeText(ScanActivity.this, "previewRetry: " + previewRetry, Toast.LENGTH_LONG).show();   //  TODO: for test only!
                    log.debug("-> previewRetry: " + previewRetry);
                    previewRetry++;
                    callPreviewActivity(tmpPages);

                } else {
                    log.debug("(2) Resultado KO");
                    File f = new File("/mnt/hdd/" + mApplication.getApplicationContext().getPackageName() + "/scans/");
                    deleteRecursive(f);

                    //Error, si el preview no puede ser lanzado por algun motivo...
                    dialogErrorPreview = es.ricoh.scantobox.util.Dialogs.DialogUtil.createErrorDialog(this, msgError);
                    dialogErrorPreview.show();
                }
            }
        }
    }

    public void gotoSplash(){
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void showErrorMsg(){

        String msgError = mContext.getResources().getString(R.string.txt_error_box_user_msg);

        dialogErrorUser = DialogUtil.createErrorDialog(mContext, msgError);
        dialogErrorUser.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                mContext.gotoSplash();
            }
        });
        dialogErrorUser.show();
    }

    public void callPreviewActivity(int pages) {
        log.debug("callPreviewActivity");
        tmpPages = pages;

        Intent intent = new Intent(ScanActivity.mContext, PreviewActivity.class);
        log.debug("Create intent");
        intent.putExtra("pages", tmpPages);
        log.debug("Put extras");

        startActivityForResult(intent, PreviewActivity.REQUEST_CODE_PREVIEW_ACTIVITY);

        log.debug("START ACTIVITY FOR RESULT EXECUTED...");
    }

    public void callUploadActivity(){

        log.debug("Cargando todos los ficheros...");

        ArrayList<File> files = new ArrayList<File>();
        File[] filesAux;

        File folder = new File("/mnt/hdd/" + getApplicationContext().getPackageName() + "/scans/");

        if(Preferences.fileType.equalsIgnoreCase(Constants.fileTypePDF)) {
                filesAux = folder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    try {
                        return name.contains(".pdf");
                    } catch (Exception e) {
                        return false;
                    }
                }
            });
        }else{
            if(Preferences.isColor) {
                filesAux = folder.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        try {
                            return name.contains(".jpg");
                        } catch (Exception e) {
                            return false;
                        }
                    }
                });
            }else{
                filesAux = folder.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        try {
                            return name.contains(".tif");
                        } catch (Exception e) {
                            return false;
                        }
                    }
                });
            }
        }

        //Add files to ArrayList
        for (File f : filesAux) {
            log.debug("Adding: " + f.getAbsolutePath());
            files.add(f);
        }

        Intent intent = new Intent(this, NavFoldersUploadActivity.class);
        intent.putExtra("filesToUpload", files);
        this.startActivityForResult(intent, NavFoldersUploadActivity.REQUEST_CODE_UPLOAD_ACTIVITY);
    }

    void deleteRecursive(File f) {

        //TODO Borramos todos los ficheros escaneados...
        log.debug("Lanzando borrado recursivo...");

        if(f.exists()) {
            if (f.isDirectory()) {
                for (File child : f.listFiles()){
                    deleteRecursive(child);
                }
            }
            f.delete();
        }
    }

    public void changeColor(View v){
        //Check dialog showing or not
        if (mApplication.ismIsWinShowing()) {
            return;
        }
        //If no dialog displays, set flag to true
        mApplication.setmIsWinShowing(true);

        Dialog dialog = DialogUtil.createColorSettingDialog(ScanActivity.this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //Close dialog, reset flag to false
                mApplication.setmIsWinShowing(false);

                changeUI();
            }
        });
        dialog.show();
    }

    public void changeRes(View v){
        //Check dialog showing or not
        if (mApplication.ismIsWinShowing()) {
            return;
        }
        //If no dialog displays, set flag to true
        mApplication.setmIsWinShowing(true);

        Dialog dialog = DialogUtil.createResolutionSettingDialog(ScanActivity.this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //Close dialog, reset flag to false
                mApplication.setmIsWinShowing(false);

                changeUI();
            }
        });
        dialog.show();
    }

    public void changeSize(View v){
        //Check dialog showing or not
        if (mApplication.ismIsWinShowing()) {
            return;
        }
        //If no dialog displays, set flag to true
        mApplication.setmIsWinShowing(true);

        Dialog dialog = SizeDialog.createSizeDialog(ScanActivity.this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //Close dialog, reset flag to false
                mApplication.setmIsWinShowing(false);

                changeUI();
            }
        });
        dialog.show();
    }

    public void changeDensity(View v){
        //Check dialog showing or not
        if (mApplication.ismIsWinShowing()) {
            return;
        }
        //If no dialog displays, set flag to true
        mApplication.setmIsWinShowing(true);

        Dialog dialog = DensityDialog.createDensityDialog(ScanActivity.this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //Close dialog, reset flag to false
                mApplication.setmIsWinShowing(false);

                changeUI();
            }
        });
        dialog.show();
    }

    public void changeOrientation(View v){
        //Check dialog showing or not
        if (mApplication.ismIsWinShowing()) {
            return;
        }
        //If no dialog displays, set flag to true
        mApplication.setmIsWinShowing(true);

        Dialog dialog = OrientationDialog.createOrientationDialog(ScanActivity.this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //Close dialog, reset flag to false
                mApplication.setmIsWinShowing(false);

                changeUI();
            }
        });
        dialog.show();
    }

    public void changeFileType(View v){
        //Check dialog showing or not
        if (mApplication.ismIsWinShowing()) {
            return;
        }
        //If no dialog displays, set flag to true
        mApplication.setmIsWinShowing(true);

        Dialog dialog = DialogUtil.createFiletypeSettingDialog(ScanActivity.this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //Close dialog, reset flag to false
                mApplication.setmIsWinShowing(false);

                changeUI();
            }
        });
        dialog.show();
    }

    public void changeSide(View v){
        //Check dialog showing or not
        if (mApplication.ismIsWinShowing()) {
            return;
        }
        //If no dialog displays, set flag to true
        mApplication.setmIsWinShowing(true);

        Dialog dialog = SideDialog.createSideDialog(ScanActivity.this/*, mScanSettingDataHolder*/);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //Close dialog, reset flag to false
                mApplication.setmIsWinShowing(false);

                changeUI();
            }
        });
        dialog.show();
    }

    private void changeUI(){

        if(Preferences.orientation.equals(Constants.orientationL)){
            txt_orientation.setText(R.string.txt_legible_scan);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                img_orientation.setBackground(context.getResources().getDrawable(R.drawable.icon_setting_readable_01));
            } else {
                img_orientation.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_setting_readable_01));
            }
        }

        if(Preferences.orientation.equals(Constants.orientationNL)){
            txt_orientation.setText(R.string.txt_nolegible_scan);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                img_orientation.setBackground(context.getResources().getDrawable(R.drawable.icon_setting_readable_02));
            } else {
                img_orientation.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_setting_readable_02));
            }
        }

        if(Preferences.duplex){
            txt_duplex.setText(R.string.txt_duplexOn_scan);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                img_duplex.setBackground(context.getResources().getDrawable(R.drawable.icon_setting_paper_duplex02));
            } else {
                img_duplex.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_setting_paper_duplex02));
            }
        }

        if(!Preferences.duplex){
            txt_duplex.setText(R.string.txt_duplexOff_scan);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                img_duplex.setBackground(context.getResources().getDrawable(R.drawable.icon_setting_paper_duplex01));
            } else {
                img_duplex.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_setting_paper_duplex01));
            }
        }

        if(Preferences.fileType.equalsIgnoreCase(Constants.fileTypePDF)){
            txt_filetype.setText(R.string.txt_filetype_pdf_scan);
        }else{
            txt_filetype.setText(R.string.txt_filetype_jpeg_scan);
        }

        if(Preferences.isColor){
            txt_color.setText(R.string.txt_scolor_color);
        }else{
            txt_color.setText(R.string.txt_scolor_BW);
        }

        if(Preferences.resolutionDPI == 200){
            txt_resolution.setText(R.string.txt_200dpi_scan);
        }else if(Preferences.resolutionDPI == 300){
            txt_resolution.setText(R.string.txt_300dpi_scan);
        }else if(Preferences.resolutionDPI == 400){
            txt_resolution.setText(R.string.txt_400dpi_scan);
        }else if(Preferences.resolutionDPI == 600) {
            txt_resolution.setText(R.string.txt_600dpi_scan);
        }

        if(Preferences.size.equals(Constants.sizeAuto)){
            txt_paperSize.setText(R.string.txt_psAuto_scan);
        }else if(Preferences.size.equals(Constants.sizeA4)){
            txt_paperSize.setText(R.string.txt_psA4P_scan);
        }else if(Preferences.size.equals(Constants.sizeA4L)) {
            txt_paperSize.setText(R.string.txt_psA4L_scan);
        }else if(Preferences.size.equals(Constants.sizeLetter)) {
            txt_paperSize.setText(R.string.txt_psLetter_scan);
        }else if(Preferences.size.equals(Constants.sizeLetterL)) {
            txt_paperSize.setText(R.string.txt_psLetterL_scan);
        }else if(Preferences.size.equals(Constants.sizeLegal)) {
            txt_paperSize.setText(R.string.txt_psLegal_scan);
        }else if(Preferences.size.equals(Constants.sizeLegalL)) {
            txt_paperSize.setText(R.string.txt_psLegalL_scan);
        }else if(Preferences.size.equals(Constants.sizeOficio)) {
            txt_paperSize.setText(R.string.txt_psOficio_scan);
        }else if(Preferences.size.equals(Constants.sizeA3)) {
            txt_paperSize.setText(R.string.txt_psA3_scan);
        }

        if(Preferences.density.equals(Constants.densityAuto)) {
            txt_density.setText(R.string.txt_densityAuto_scan);
        }else if(Preferences.density.equals(Constants.densityDark)) {
            txt_density.setText(R.string.txt_densityDark_scan);
        }else if(Preferences.density.equals(Constants.densityLighter)) {
            txt_density.setText(R.string.txt_densityLighter_scan);
        }
    }
    /**
     * The listener class to monitor scan service attribute changes.
     * [Processes]
     *   (1) Rewrites the scan service state display label accordingly to the scan service state.
     *   (2) Requests to display/update/hide error screens.
     */
    class ScanServiceAttributeListenerImpl implements ScanServiceAttributeListener {

        /**
         * UI thread handler
         */
        private Handler mHandler;

        ScanServiceAttributeListenerImpl(Handler handler) {
            mHandler = handler;
        }

        @Override
        public void attributeUpdate(final ScanServiceAttributeEvent event) {
            ScannerState state = (ScannerState)event.getAttributes().get(ScannerState.class);
            ScannerStateReasons stateReasons = (ScannerStateReasons)event.getAttributes().get(ScannerStateReasons.class);
            OccuredErrorLevel errorLevel = (OccuredErrorLevel) event.getAttributes().get(OccuredErrorLevel.class);

            String stateLabel = "";

            //(1)
            switch(state) {
                case IDLE :
                    Log.i(Const.TAG, PREFIX + "ScannerState : IDLE");
                    stateLabel = getString(R.string.txid_scan_t_state_ready);
                    break;
                case MAINTENANCE:
                    Log.i(Const.TAG, PREFIX + "ScannerState : MAINTENANCE");
                    stateLabel = getString(R.string.txid_scan_t_state_maintenance);
                    break;
                case PROCESSING:
                    Log.i(Const.TAG, PREFIX + "ScannerState : PROCESSING");
                    stateLabel = getString(R.string.txid_scan_t_state_scanning);
                    break;
                case STOPPED:
                    Log.i(Const.TAG, PREFIX + "ScannerState : STOPPED");
                    stateLabel = getString(R.string.txid_scan_t_state_stopped);
                    break;
                case UNKNOWN:
                    Log.i(Const.TAG, PREFIX + "ScannerState : UNKNOWN");
                    stateLabel = getString(R.string.txid_scan_t_state_unknown);
                    break;
                default:
                    Log.i(Const.TAG, PREFIX + "ScannerState : never reach here ...");
                /* never reach here */
                    break;
            }

            if( stateReasons != null ) {
                Set<ScannerStateReason> reasonSet = stateReasons.getReasons();
                for(ScannerStateReason reason : reasonSet) {
                    switch(reason) {
                        case COVER_OPEN:
                            stateLabel = getString(R.string.txid_scan_t_state_reason_cover_open);
                            break;
                        case MEDIA_JAM:
                            stateLabel = getString(R.string.txid_scan_t_state_reason_media_jam);
                            break;
                        case PAUSED:
                            stateLabel = getString(R.string.txid_scan_t_state_reason_paused);
                            break;
                        case OTHER:
                            stateLabel = getString(R.string.txid_scan_t_state_reason_other);
                            break;
                        default:
                            /* never reach here */
                            break;
                    }
                }
            }

            final String result = stateLabel;
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    text_state.setText(result);
                }
            });

            //(2)
            if (OccuredErrorLevel.ERROR.equals(errorLevel)
                    || OccuredErrorLevel.FATAL_ERROR.equals(errorLevel)) {

                String stateString = makeAlertStateString(state);
                String reasonString = makeAlertStateReasonString(stateReasons);

                if (mLastErrorLevel == null) {
                    // Normal -> Error
                    if (isForegroundApp(getPackageName())) {
                        mApplication.displayAlertDialog(ALERT_DIALOG_APP_TYPE_SCANNER, stateString, reasonString);
                        mAlertDialogDisplayed = true;
                    }
                } else {
                    // Error -> Error
                    if (mAlertDialogDisplayed) {
                        mApplication.updateAlertDialog(ALERT_DIALOG_APP_TYPE_SCANNER, stateString, reasonString);
                    }
                }
                mLastErrorLevel = errorLevel;

            } else {
                if (mLastErrorLevel != null) {
                    // Error -> Normal
                    if (mAlertDialogDisplayed) {
                        String activityName = getTopActivityClassName(getPackageName());
                        if (activityName == null) {
                            activityName = ScanActivity.class.getName();
                        }
                        mApplication.hideAlertDialog(ALERT_DIALOG_APP_TYPE_SCANNER, activityName);
                        mAlertDialogDisplayed = false;
                    }
                }
                mLastErrorLevel = null;
            }
        }
    }

    /**
     * Connects with the scan service asynchronously.
     * [Processes]
     *   (1) Sets the listener to receive scan service events.
     *       This task repeats until the machine becomes available or cancel button is touched.
     *   (2) Confirms the asynchronous connection.
     *       This task repeats until the connection is confirmed or cancel button is touched.
     *   (3) After the machine becomes available and connection is confirmed,
     *       obtains job setting values.
     */
    class ScanServiceInitTask extends AsyncTask<Void, Void, Integer> {

        AsyncConnectState addListenerResult = null;
        AsyncConnectState getAsyncConnectStateResult = null;

        @Override
        protected Integer doInBackground(Void... params) {

            final ScanService scanService = mApplication.getScanService();

            //(1)
            while (true) {
                if(isCancelled()) {
                    return -1;
                }
                addListenerResult = scanService.addScanServiceAttributeListener(mScanServiceAttrListener);

                if (addListenerResult == null) {
                    sleep(100);
                    continue;
                }

                if (addListenerResult.getState() == AsyncConnectState.STATE.CONNECTED) {
                    break;
                }

                if (addListenerResult.getErrorCode() == AsyncConnectState.ERROR_CODE.NO_ERROR){
                    // do nothing
                } else if (addListenerResult.getErrorCode() == AsyncConnectState.ERROR_CODE.BUSY) {
                    sleep(10000);
                } else if (addListenerResult.getErrorCode() == AsyncConnectState.ERROR_CODE.TIMEOUT){
                    // do nothing
                } else if (addListenerResult.getErrorCode() == AsyncConnectState.ERROR_CODE.INVALID){
                    return 0;
                } else {
                    // unknown state
                    return 0;
                }
            }

            if (addListenerResult.getState() != AsyncConnectState.STATE.CONNECTED) {
                return 0;
            }

            //(2)
            while (true) {
                if(isCancelled()) {
                    return -1;
                }
                getAsyncConnectStateResult = scanService.getAsyncConnectState();

                if (getAsyncConnectStateResult == null) {
                    sleep(100);
                    continue;
                }

                if (getAsyncConnectStateResult.getState() == AsyncConnectState.STATE.CONNECTED) {
                    break;
                }

                if (getAsyncConnectStateResult.getErrorCode() == AsyncConnectState.ERROR_CODE.NO_ERROR){
                    // do nothing
                } else if (getAsyncConnectStateResult.getErrorCode() == AsyncConnectState.ERROR_CODE.BUSY) {
                    sleep(10000);
                } else if (getAsyncConnectStateResult.getErrorCode() == AsyncConnectState.ERROR_CODE.TIMEOUT){
                    // do nothing
                } else if (getAsyncConnectStateResult.getErrorCode() == AsyncConnectState.ERROR_CODE.INVALID){
                    return 0;
                } else {
                    // unknown state
                    return 0;
                }
            }

            //(3)
            if (addListenerResult.getState() == AsyncConnectState.STATE.CONNECTED
                    && getAsyncConnectStateResult.getState() == AsyncConnectState.STATE.CONNECTED) {
                mScanSettingDataHolder.init(scanService);
            }

            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (addListenerResult==null) {
                Log.d(Const.TAG, PREFIX + "addScanServiceAttributeListener:null");
            } else {
                Log.d(Const.TAG, PREFIX + "addScanServiceAttributeListener:" + addListenerResult.getState() + "," + addListenerResult.getErrorCode());
            }
            if (getAsyncConnectStateResult==null) {
                Log.d(Const.TAG, PREFIX + "getAsyncConnectState:null");
            } else {
                Log.d(Const.TAG, PREFIX + "getAsyncConnectState:" + getAsyncConnectStateResult.getState() + "," + getAsyncConnectStateResult.getErrorCode());
            }

            if (result!=0) {
                /* canceled. */
                return;
            }

            if (addListenerResult.getState() == AsyncConnectState.STATE.CONNECTED
                    && getAsyncConnectStateResult.getState() == AsyncConnectState.STATE.CONNECTED) {
                // connection succeeded.
                mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.ACTIVITY_BOOT_COMPLETED);
            }
            else {
                // the connection is invalid.
                mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.ACTIVITY_BOOT_FAILED);
            }
        }

        /**
         * sleep for the whole of the specified interval
         */
        private void sleep(long time) {
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                Log.d(Const.TAG, PREFIX + e.toString());
            }
        }
    }


    /**
     * Starts the alert dialog display task.
     */
    private void startAlertDialogDisplayTask() {
        if (mAlertDialogDisplayTask != null) {
            mAlertDialogDisplayTask.cancel(false);
        }
        mAlertDialogDisplayTask = new AlertDialogDisplayTask();
        mAlertDialogDisplayTask.execute();
    }

    /**
     * Stop the alert dialog display task.
     */
    private void stopAlertDialogDisplayTask() {
        if (mAlertDialogDisplayTask != null) {
            mAlertDialogDisplayTask.cancel(false);
            mAlertDialogDisplayTask = null;
        }
    }

    /**
     * The asynchronous task to judge to display system warning screen and to request to display the screen if necessary.
     */
    class AlertDialogDisplayTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            ScanServiceAttributeSet attributes = mApplication.getScanService().getAttributes();
            OccuredErrorLevel errorLevel = (OccuredErrorLevel) attributes.get(OccuredErrorLevel.class);

            if (OccuredErrorLevel.ERROR.equals(errorLevel)
                    || OccuredErrorLevel.FATAL_ERROR.equals(errorLevel)) {
                ScannerState state = (ScannerState) attributes.get(ScannerState.class);
                ScannerStateReasons stateReasons = (ScannerStateReasons) attributes.get(ScannerStateReasons.class);

                String stateString = makeAlertStateString(state);
                String reasonString = makeAlertStateReasonString(stateReasons);
                if (isCancelled()) {
                    return null;
                }

                mApplication.displayAlertDialog(ALERT_DIALOG_APP_TYPE_SCANNER, stateString, reasonString);

                mAlertDialogDisplayed = true;
                mLastErrorLevel = errorLevel;
            }
            return null;
        }

    }

    /**
     * Starts the return request from energy saving task.
     */
    private void startReturnRequestFromEnergySavingTask() {
        if (mReturnRequestFromEnergySavingTask != null) {
            mReturnRequestFromEnergySavingTask.cancel(true);
        }
        mReturnRequestFromEnergySavingTask = new ReturnRequestFromEnergySavingTask();
        mReturnRequestFromEnergySavingTask.execute();
    }

    /**
     * Stop the return request from energy saving task.
     */
    private void stopReturnRequestFromEnergySavingTask() {
        if (mReturnRequestFromEnergySavingTask != null) {
            mReturnRequestFromEnergySavingTask.cancel(true);
            mReturnRequestFromEnergySavingTask = null;
        }
    }

    /**
     * This is an asynchronous task to check the machine's power mode and to request to recover from Energy Saver mode if necessary.
     * This task only requests to recover from Energy Saver mode.
     * To check if the machine has recovered from Energy Saver mode, enable to check power mode notification.
     */
    class ReturnRequestFromEnergySavingTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean needsRequest;

            int powerMode = mApplication.getSystemStateMonitor().getPowerMode();
            Log.d(Const.TAG, PREFIX + "getPowerMode=" + powerMode);

            switch (powerMode) {
                case SystemStateMonitor.POWER_MODE_ENGINE_OFF:
                case SystemStateMonitor.POWER_MODE_OFF_MODE:
                case SystemStateMonitor.POWER_MODE_UNKNOWN:
                    needsRequest = true;
                    break;
                default:
                    needsRequest = false;
                    break;
            }

            Boolean result = Boolean.FALSE;
            if (needsRequest) {
                result = mApplication.controllerStateRequest(SmartSDKApplication.REQUEST_CONTROLLER_STATE_FUSING_UNIT_OFF);
            }
            return result;
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        }
        return false;
    }

    /**
     * Called when the activity is resumed.
     * Checks error occurrence asynchronously and switches to a system warning screen if necessary.
     * Checks machine's power mode asynchronously and requests system state if necessary.
     */
    @Override
    protected void onResume() {
        log.debug("***** ScanActivity onResume *****");
        super.onResume();

        if (SplashActivity.currentUser == null || SplashActivity.currentUser.isEmpty()) {
            log.warn("Current user is = " + SplashActivity.currentUser);
            log.warn("Forcing to finish");
            showErrorMsg();
        }

        printCopias = 0;
        finalNames = new ArrayList<String>();

        mStateMachine.registActivity(this);

        // スキャンステートマシンからシステムリセット抑止を要求されている場合、SDKServiceへロックを設定します
        mIsForeground = true;
        processSystemResetLock();
        startAlertDialogDisplayTask();
        startReturnRequestFromEnergySavingTask();

        changeUI();
    }

    /**
     * Called when the activity is stopped.
     * If the system warning screen display task is in process, the task is cancelled.
     */
    @Override
    protected void onPause() {
        log.debug("***** ScanActivity onPause *****");
        super.onPause();

        if (dialogErrorUser != null) {
            dialogErrorUser.dismiss();
        }

        if (dialogErrorPreview != null) {
            dialogErrorPreview.dismiss();
        }

        try {

            //Cancelamos la TASK de procesado
            ScanProcess.stopProcess();

            mStateMachine.cancelScanJob();

            // システムリセットロックを解除します
            mIsForeground = false;
            processSystemResetUnlock();
            stopAlertDialogDisplayTask();

        }catch (Exception ex){
            log.error("Ha ocurrido un error...");
            Utils.printStackTraceToString(ex);
        }
    }

    /**
     * Called when the activity is destroyed.
     * [Processes]
     *   (1) Send G_Scan destoyed event to the state machine.
     *       If scanning is in process, scanning is cancelled.
     *   (2) Removes the event listener and the broadcast receiver from the service.
     *   (3) If asynchronous task is in process, the task is cancelled.
     *   (4) Initializes the data saved to the application.
     *   (5) Discards references
     */
    @Override
    protected void onDestroy() {
        log.debug("***** ScanActivity onDestroy *****");
        super.onDestroy();

        try {
            mApplication.setmIsWinShowing(false);

            if (dialogErrorUser != null) {
                dialogErrorUser.dismiss();
            }

            if (dialogErrorPreview != null) {
                dialogErrorPreview.dismiss();
            }

            //Cancelamos el SCAN...
            //mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.REQUEST_JOB_STOP);
            //mApplication.getScanJob().cancelScanJob();
            //mStateMachine.cancelScanJob();

            File f = new File("/mnt/hdd/" + mApplication.getApplicationContext().getPackageName() + "/scans/");
            deleteRecursive(f);

            //アクティビティ終了時は、ロック設定状態に関わらずシステムリセットのロックを解除します
            processSystemResetUnlock();

            //(1)
            mStateMachine.procScanEvent(ScanStateMachine.ScanEvent.ACTIVITY_DESTROYED);


            //(2)
            ScanService scanService = mApplication.getScanService();
            try {
                scanService.removeScanServiceAttributeListener(mScanServiceAttrListener);
            } catch (IllegalStateException e) {
            /* the listener is not registered. */
                log.error(Utils.printStackTraceToString(e));
            }
            unregisterReceiver(mReceiver);

            //(3)
            stopAlertDialogDisplayTask();
            stopReturnRequestFromEnergySavingTask();
            if (mScanServiceInitTask != null) {
                mScanServiceInitTask.cancel(false);
                mScanServiceInitTask = null;
            }

            // set application state to Normal
            mApplication.setAppState(ScanActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);

            //(4)
            mApplication.init();

            //(5)
            mApplication = null;
            mReceiver = null;
            mButtonStart = null;
            mScanSettingDataHolder = null;
            mStateMachine = null;
            mScanServiceAttrListener = null;

        } catch (Exception ex){
            log.error(Utils.printStackTraceToString(ex));
            return;
        }
    }

    /**
     * Locking system auto reset.
     * When job is executing and MainAcitivity is in foreground, set system auto reset of SDKService.
     */
    private void processSystemResetLock() {
        if (mIsForeground && mRequestedSystemResetLock) {
            mApplication.lockSystemReset();
        }
    }

    /**
     * Clear locking system auto reset.
     * To avoid the continuation of locked state without intention, Clear locking
     * even it is not requested.
     */
    private void processSystemResetUnlock() {
        mApplication.unlockSystemReset();
    }

    /**
     * Creates the state string to be passed to system warning screen display request.
     *
     * @param state State of scan service
     * @return State string
     */
    public String makeAlertStateString(ScannerState state) {
        String stateString = "";
        if (state != null) {
            stateString = state.toString();
        }
        return stateString;
    }

    /**
     * Creates the state string to be passed to system warning screen display request.
     *
     * @param state State of scan service
     * @return State string
     */
    public String makeAlertStateString(PrinterState state) {
        String stateString = "";
        if (state != null) {
            stateString = state.toString();
        }
        return stateString;
    }

    /**
     * Creates the state reason string to be passed to the system warning screen display request.
     * If multiple state reasons exist, only the first state reason is passed.
     *
     * @param stateReasons Scan service state reason
     * @return State reason string
     */
    public String makeAlertStateReasonString(ScannerStateReasons stateReasons) {
        String reasonString = "";
        if (stateReasons != null) {
            Object[] reasonArray = stateReasons.getReasons().toArray();
            if (reasonArray != null && reasonArray.length > 0) {
                reasonString = reasonArray[0].toString();
            }
        }
        return reasonString;
    }

    /**
     * Creates the state reason string to be passed to the system warning screen display request.
     * If multiple state reasons exist, only the first state reason is passed.
     *
     * @param stateReasons Scan service state reason
     * @return State reason string
     */
    public String makeAlertStateReasonString(PrinterStateReasons stateReasons) {
        String reasonString = "";
        if (stateReasons != null) {
            Object[] reasonArray = stateReasons.getReasons().toArray();
            if (reasonArray != null && reasonArray.length > 0) {
                reasonString = reasonArray[0].toString();
            }
        }
        return reasonString;
    }

    /**
     * Obtains whether or not the specified application is in the foreground state.
     *
     * @param packageName Application package name
     * @return If the application is in the foreground state, true is returned.
     */
    public boolean isForegroundApp(String packageName) {
        boolean result = false;
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> list = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : list) {
            if (packageName.equals(info.processName)) {
                result = (info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND);
                break;
            }
        }
        return result;
    }

    /**
     * Obtains the top class in the activity stack of the specified application.
     *
     * @param packageName Application package name
     * @return The name of the FQCN class name of the top class. If the name cannot be obtained, null is returned.
     */
    public String getTopActivityClassName(String packageName) {
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(30);
        for (ActivityManager.RunningTaskInfo info : list) {
            if (packageName.equals(info.topActivity.getPackageName())) {
                return info.topActivity.getClassName();
            }
        }
        return null;
    }

    public static Context getAppContext() {
        return ScanActivity.context;
    }

    public static ScanToBoxApplication getmApplication (){
        return mApplication;

    }
}

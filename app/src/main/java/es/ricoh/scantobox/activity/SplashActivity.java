package es.ricoh.scantobox.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.box.sdk.BoxAPIConnection;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.BoxApiManager;
import es.ricoh.scantobox.util.Constants;
import es.ricoh.scantobox.util.Dialogs.DialogUtil;
import es.ricoh.scantobox.util.Preferences;
import es.ricoh.scantobox.util.Utils;
import jp.co.ricoh.ssdk.wrapper.common.InvalidResponseException;
import jp.co.ricoh.ssdk.wrapper.common.Request;
import jp.co.ricoh.ssdk.wrapper.common.RequestQuery;
import jp.co.ricoh.ssdk.wrapper.common.Response;
import jp.co.ricoh.ssdk.wrapper.rws.property.DeviceProperty;
import jp.co.ricoh.ssdk.wrapper.rws.property.GetDeviceInfoResponseBody;


public class SplashActivity extends Activity {

    private static final Logger log = ALogger.getLogger(SplashActivity.class);
    public static String versionName;
    public static String about;
    private boolean mMultipleRunning = false;
    public static boolean is305 = false;
    private Button btnAcceder;
    private static Context context;
    public static String currentUser = null;
    public static String packageName = "";
    WebView web;

    private Dialog dialogConfig = null;
    private Dialog auth_dialog = null;
    private Dialog dialogWaiting = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getNumActivities(getPackageName()) > 1) {
            //Another instance of the app is already running
            mMultipleRunning = true;
            finish();
            return;
        }
        context = this;

        log.debug("***** SplashActivity onCreate *****");

        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException ex) {
            log.error(Utils.printStackTraceToString(ex));
        }

        about = this.getResources().getString(R.string.app_name) + " - v" + versionName + " Build on (27/01/2017)";
        packageName = getPackageName();

        readSerialNumber();

        File dir = new File("/mnt/hdd/" + getPackageName() + "/");
        if (!dir.exists()) {
            dir.mkdir();
        }

        BoxApiManager.load();

        setContentView(R.layout.activity_splash);

        btnAcceder = (Button) findViewById(R.id.btnAcceder);
        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showLoginOrMain();
            }
        });

        try {
            getWindow().addFlags(WindowManager.LayoutParams.class.getField("FLAG_NEEDS_MENU_KEY").getInt(null));
        } catch (IllegalAccessException e) {
            log.error(Utils.printStackTraceToString(e));
        } catch (NoSuchFieldException e) {
            log.error(Utils.printStackTraceToString(e));
        }

    }

    protected void onResume() {
        super.onResume();

        log.debug("***** SplashActivity onResume *****");

        if (mMultipleRunning) {
            return;
        }

        //Unlock APP
        Utils.UnLock_logout(this, getPackageName());

        TextView titulo = (TextView) findViewById(R.id.titulo);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.zoom);
        titulo.startAnimation(animation);

        try {
            BoxApiManager.removeUserApi(Constants.GUEST_USER);   //  Nos aseguramos de borrar la sesión del usuario INVITADO por si se hubiese quedado abierta

            File dir = new File("/mnt/hdd/" + getPackageName() + "/");
            File config = new File(dir, "config.properties");

            File userFile = new File(dir, "s2bXlet");

            currentUser = readUsername(userFile);
            log.info("Current user: " + currentUser);

            //  USUARIO INVITADO: si detectamos que se ha autenticado como Invitado, dejamos el currentLoggedUser vacío para que siempre se pida el loggin y no se almacene la sessión de Box
            //  En teoria SLNX estará configurado para que no se muestre la opción de acceder como Invitado, pero lo tenemos en cuenta por si se diera el caso evitar posibles problemas
            if (currentUser != null && currentUser.equalsIgnoreCase("Stream Line Guest")) {
                log.info("Guest user detected");
                currentUser = Constants.GUEST_USER;
            }

            if (!config.exists()) {

                log.info("El fichero de configuración no existe o está vacío");
                (findViewById(R.id.configurado)).setVisibility(View.VISIBLE);
                (findViewById(R.id.btnAcceder)).setVisibility(View.INVISIBLE);

            } else {

                //  Cargamos la configuración
                loadProperties(config);

                //  Verificamos que el numero de serie esté rellenado en las preferencias
                if (Preferences.serialNumber == null || Preferences.serialNumber.isEmpty()) {
                    readSerialNumber();
                }

                if (checkBoxPreferences()) {
                    log.info("Aplicación con fichero de configuración activo");
                    (findViewById(R.id.configurado)).setVisibility(View.INVISIBLE);
                    (findViewById(R.id.btnAcceder)).setVisibility(View.VISIBLE);

                    showLoginOrMain();
                } else {
                    log.info("Todos los parámetros de configuración de Box deben estar rellenados");
                    (findViewById(R.id.configurado)).setVisibility(View.VISIBLE);
                    (findViewById(R.id.btnAcceder)).setVisibility(View.INVISIBLE);
                }

            }

        } catch (Exception e) {
            log.error(Utils.printStackTraceToString(e));
        }
    }

    //  Esta función lleva a MainActivity si la api es válida, o en caso contrario muestra el LoginDialog
    private void showLoginOrMain() {
        log.debug("showLoginOrMain");

        //TODO: No funciona, no se muestra nada...
        /*dialogWaiting = DialogUtil.createProcessWaitDialog(this, R.layout.dlg_cargando_boxapi);
        DialogUtil.showDialog(dialogWaiting, 540, 290);*/

        //  TODO: controlar que si no hay conexión de red, cuando intenta ir al main se queda

        if (currentUser == null) {
            //  Si el fichero de usuario no existe, lo más probable es que el Xlet AuthListener no esté instalado o tenga algún problema
            //  En ese caso siempre pediremos el login de Box y dejaremos constancia del problema en los logs
            log.warn("No se ha podido obtener el usuario autenticado en SLNX. Revise el estado de la aplicación Xlet");
            currentUser = Constants.GUEST_USER;
        }

        log.debug("Entry showLoginOrMain 001");
        if (BoxApiManager.containsUserApi(currentUser)) {

            log.debug("Entry showLoginOrMain 002");
            if (BoxApiManager.isUserApiValid(currentUser)) {
                //  Si la sesión que teníamos guardada todavía sigue abierta y se puede refrescar, vamos a la pantalla principal
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
            } else {
                showLoginDialog(currentUser);
            }

        } else {
            //  En caso contrario, mostramos el diálogo de Login
            showLoginDialog(currentUser);
        }

        //dialogWaiting.dismiss();
    }


    //  Esta función lleva a MainActivity si la api es válida, o en caso contrario muestra el LoginDialog
//    private void showLoginOrMain() {
//        log.debug("showLoginOrMain");
//
//        //  TODO: controlar que si no hay conexión de red, cuando intenta ir al main se queda
//
//        if (currentUser == null) {
//            //  Si el fichero de usuario no existe, lo más probable es que el Xlet AuthListener no esté instalado o tenga algún problema
//            //  En ese caso siempre pediremos el login de Box y dejaremos constancia del problema en los logs
//            log.warn("No se ha podido obtener el usuario autenticado en SLNX. Revise el estado de la aplicación Xlet");
//            currentUser = Constants.GUEST_USER;
//        }
//
//        log.debug("Entry showLoginOrMain 001");
//        if (BoxApiManager.containsUserApi(currentUser)) {
//
//            new UserApiValidTask().execute(1);
//
//        } else {
//            //  En caso contrario, mostramos el diálogo de Login
//            showLoginDialog(currentUser);
//        }
//    }

    private class UserApiValidTask extends AsyncTask<Integer, Boolean, Boolean> {

        SplashActivity activity;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            activity = SplashActivity.this;

            dialogWaiting = DialogUtil.createProcessWaitDialog(activity, R.layout.dlg_cargando_boxapi);
            DialogUtil.showDialog(dialogWaiting, 540, 290);
        }

        @Override
        protected Boolean doInBackground(Integer... params) {
            log.debug("***** Lanzando proceso UserApiValidTask *****");

            try {

                return BoxApiManager.isUserApiValid(currentUser);

            } catch (Exception ex){
                log.error(Utils.printStackTraceToString(ex));
            }

            log.debug("Proceso UserApiValidTask finalizado!");

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            log.info("POST EXECUTE");
            try{

                if (result) {
                    //  Si la sesión que teníamos guardada todavía sigue abierta y se puede refrescar, vamos a la pantalla principal
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    showLoginDialog(currentUser);
                }

            }catch (Exception ex){
                showLoginDialog(currentUser);

            }finally {
                if(dialogWaiting != null){
                    dialogWaiting.dismiss();
                }
            }
        }
    }

    private String readUsername(File userFile) {
        log.debug("readUsername");

        if (userFile == null || !userFile.exists()) {
            log.warn("File is null or doesn't exist");
            return null;
        } else {
            BufferedReader br = null;
            String username = null;
            try {
                br = new BufferedReader(new FileReader(userFile));
                String line;

                if ((line = br.readLine()) != null) {
                    username = line;
                }

            } catch (Exception e) {
                log.error(Utils.printStackTraceToString(e));
                return null;
            } finally {
                try {
                    br.close();
                } catch (Exception e) {
                    log.error(Utils.printStackTraceToString(e));
                    return null;
                }

                return username;
            }
        }
    }

    private void showLoginDialog(final String username) {
        log.debug("showLoginDialog");

        auth_dialog = new Dialog(SplashActivity.this);
        auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        auth_dialog.setContentView(R.layout.auth_dialog);
        web = (WebView)auth_dialog.findViewById(R.id.webv);
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setSaveFormData(false);   //Para que no muestre sugerencias en el campo de usuario
        //web.loadUrl(OAUTH_URL + "?redirect_uri=" + REDIRECT_URI + "&response_type=code&client_id=" + CLIENT_ID + "&box_login=" + boxLogin);
        web.loadUrl(Preferences.oauthURL + "?redirect_uri=" + Preferences.redirectURL + "&response_type=code&client_id=" + Preferences.clientID);
        web.setWebViewClient(new WebViewClient() {

            boolean authComplete = false;
            Intent resultIntent = new Intent();

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                super.onPageStarted(view, url, favicon);
            }

            String authCode;
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (url.contains("?code=") && !authComplete) {
                    Uri uri = Uri.parse(url);
                    authCode = uri.getQueryParameter("code");
                    authComplete = true;
                    resultIntent.putExtra("code", authCode);
                    SplashActivity.this.setResult(Activity.RESULT_OK, resultIntent);
                    setResult(Activity.RESULT_CANCELED, resultIntent);

                    auth_dialog.dismiss();

                    GetBoxApi boxApi = new GetBoxApi();
                    BoxAPIConnection api = null;
                    try {
                        log.info("Obtaining Boxapi....");
                        api = boxApi.execute(authCode).get();

                        if (api != null) {
                            BoxApiManager.addBoxUserApi(username, api);

                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else {
                            log.warn("Api is null");
                        }

                    } catch (InterruptedException e) {
                        log.debug(Utils.printStackTraceToString(e));
                    } catch (ExecutionException e) {
                        log.debug(Utils.printStackTraceToString(e));
                    }

                } else if (url.contains("error=access_denied")) {
                    log.debug("ACCESS_DENIED_HERE");
                    resultIntent.putExtra("code", authCode);
                    authComplete = true;
                    setResult(Activity.RESULT_CANCELED, resultIntent);
//                    Toast.makeText(getApplicationContext(), "Error Occured", Toast.LENGTH_SHORT).show();

                    auth_dialog.dismiss();
                }
            }
        });

        //  TODO: pdte controlar si hay algún error, no dejar el dialog abierto

        auth_dialog.show();
        auth_dialog.setCancelable(true);
    }

    class GetBoxApi extends AsyncTask<String, Void, BoxAPIConnection> {

        @Override
        protected BoxAPIConnection doInBackground(String... params) {
            String authCode = params[0];

            BoxAPIConnection api = null;
            try {
                api = new BoxAPIConnection(Preferences.clientID, Preferences.clientSecret, authCode);
                return api;
            } catch (Exception e) {
                log.error("GetBoxApi: An error occurred while obtaining BoxApiConnection");
                log.error(Utils.printStackTraceToString(e));
                return null;
            }
        }
    }

    public void loadProperties(File config) {

        log.debug("***** Cargando properties *****");

        log.debug("Cargando configuración del fichero de propiedades.");
        Properties prop = new Properties();
        InputStreamReader input = null;

        try {
            input = new InputStreamReader(new FileInputStream(config));
            prop.load(input);

            String clientID = prop.getProperty("clientID");
            String clientSecret = prop.getProperty("clientSecret");
            String redirectURL = prop.getProperty("redirectURL");
            String oauthURL = prop.getProperty("oauthURL");
//            String serialNumber = prop.getProperty("serialNumber");

            log.debug("Encontradas: " + prop.size() + " properties...");

            Preferences.clientID = clientID;
            Preferences.clientSecret = clientSecret;
            Preferences.redirectURL = redirectURL;
            Preferences.oauthURL = oauthURL;
//            Preferences.serialNumber = serialNumber;

        } catch (IOException ex) {
            log.error(Utils.printStackTraceToString(ex));

        } catch (Exception e) {
            log.error(Utils.printStackTraceToString(e));

        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    log.error(Utils.printStackTraceToString(e));
                }
            }
        }
    }

    public void launchConfig() {
        log.debug("launchConfig");

        Intent intent = new Intent(SplashActivity.this, ConfigActivity.class);
        startActivityForResult(intent, ConfigActivity.REQUEST_CODE_CONFIG_ACTIVITY);

        if (dialogConfig != null) {
            dialogConfig.dismiss();
        }

        if (auth_dialog != null) {
            auth_dialog.dismiss();
        }

        if (dialogWaiting != null) {
            dialogWaiting.dismiss();
        }
    }

    @Override
    protected void onPause() {
        log.debug("***** SplashActivity onPause *****");
        super.onPause();

        if (dialogConfig != null) {
            dialogConfig.dismiss();
        }

        if (auth_dialog != null) {
            auth_dialog.dismiss();
        }

        if (dialogWaiting != null) {
            dialogWaiting.dismiss();
        }
    }

    @Override
    protected void onRestart() {
        log.debug("***** SplashActivity onRestart *****");
        super.onRestart();

        if (dialogConfig != null) {
            dialogConfig.dismiss();
        }

        if (auth_dialog != null) {
            auth_dialog.dismiss();
        }

        if (dialogWaiting != null) {
            dialogWaiting.dismiss();
        }
    }

    protected void onDestroy() {
        super.onDestroy();

        if (mMultipleRunning) {
            return;
        }
    }

    private boolean checkBoxPreferences() {
        log.debug("checkBoxPreferences");
        try {

            return !Preferences.clientID.isEmpty() && !Preferences.clientSecret.isEmpty() && !Preferences.redirectURL.isEmpty() && !Preferences.oauthURL.isEmpty();

        } catch(Exception e) {
            log.debug(Utils.printStackTraceToString(e));
            return false;
        }
    }

    public static class GetSerialNumberTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            RequestQuery query = new RequestQuery();
            query.put("getMethod", "deviceDescription");

            Request req = new Request();
            req.setQuery(query);

            DeviceProperty info = new DeviceProperty();

            try{
                Response<GetDeviceInfoResponseBody> response = info.getDeviceInfo(req);
                return response.getBody().getDeviceDescription().getSerialNumber();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InvalidResponseException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private void readSerialNumber() {
        GetSerialNumberTask getSerialNumber = new GetSerialNumberTask();
        try {
            String serialNumber = getSerialNumber.execute().get();
            log.debug("Numero de serie: " + serialNumber);
            Preferences.serialNumber = serialNumber;

        } catch (Exception ex) {
            log.error("Error al leer el Número de serie del equipo:");
            log.error(Utils.printStackTraceToString(ex));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:

                DialogUtil.createAboutDialog(this, about).show();

                return true;

            case R.id.config:

                dialogConfig = DialogUtil.createLoginDialog(this, 0);
                dialogConfig.show();
                return true;

            default:

                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Obtains the number of activities in the activity stack of the specified application.
     *
     * @param packageName Application package name
     * @return The number of activitys. If the number cannot be obtained, 0 is returned.
     */
    private int getNumActivities(String packageName) {
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(30);
        for (ActivityManager.RunningTaskInfo info : list) {
            if (packageName.equals(info.topActivity.getPackageName())) {
                return info.numActivities;
            }
        }
        return 0;
    }

}
package es.ricoh.scantobox.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxAPIRequest;
import com.box.sdk.BoxAPIResponse;
import com.box.sdk.BoxFile;
import com.box.sdk.ProgressListener;

import org.apache.commons.net.io.Util;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.application.PrintServiceAttributeListenerImpl;
import es.ricoh.scantobox.application.PrintSettingDataHolder;
import es.ricoh.scantobox.application.PrintSettingSupportedHolder;
import es.ricoh.scantobox.application.PrintStateMachine;
import es.ricoh.scantobox.application.ScanToBoxApplication;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.BoxApiManager;
import es.ricoh.scantobox.util.Dialogs.BeanBoxApiException;
import es.ricoh.scantobox.util.Dialogs.DialogUtil;
import es.ricoh.scantobox.util.Dialogs.SideDialog;
import es.ricoh.scantobox.util.MyURLTemplate;
import es.ricoh.scantobox.util.Preferences;
import es.ricoh.scantobox.util.Utils;
import jp.co.ricoh.ssdk.function.Const;
import jp.co.ricoh.ssdk.function.application.SystemStateMonitor;
import jp.co.ricoh.ssdk.function.common.SmartSDKApplication;
import jp.co.ricoh.ssdk.function.common.impl.AsyncConnectState;
import jp.co.ricoh.ssdk.function.print.PrintFile;
import jp.co.ricoh.ssdk.function.print.PrintService;
import jp.co.ricoh.ssdk.function.print.attribute.PrintRequestAttribute;
import jp.co.ricoh.ssdk.function.print.attribute.standard.Copies;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PaperTray;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintColor;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintResolution;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrintSide;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterState;
import jp.co.ricoh.ssdk.function.print.attribute.standard.PrinterStateReasons;
import jp.co.ricoh.ssdk.function.print.event.PrintServiceAttributeListener;

/**
 * Created by cristian.rodriguez on 10/03/2017.
 * Checked: OK!
 */

public class PrintActivity extends Activity {

    public static final int REQUEST_CODE_PRINT_ACTIVITY = 3;
    private static final Logger log = ALogger.getLogger(PrintActivity.class);
    private static Context context;

    //Application object
    private static ScanToBoxApplication mApplication;

    //Scan start button
    private RelativeLayout mButtonStart;

    //Return request from energy saving task
    private ReturnRequestFromEnergySavingTask mReturnRequestFromEnergySavingTask = null;

    //During onResume and onPause, it should be true.
    private boolean mIsForeground = false;

    /**
     * Request locking system auto reset flag from state machine.
     * When request to lock system auto reset, it is changed to true.
     * When lock is required and MainAcitivity is in foreground, set system auto reset of SDKService.
     */
    private boolean mRequestedSystemResetLock = false;

    //Define the prefix for log information with abbreviation package and class name
    private final static String PREFIX = "activity:PrintActivity:";

    //Print settings.
    private PrintSettingDataHolder mHolder = new PrintSettingDataHolder();

    //Listener to receive print service attribute event.
    private PrintServiceAttributeListenerImpl mServiceAttributeListener;

    private static PrintActivity mContext;

    private TextView txt_duplex;
    private ImageView img_duplex;
    private TextView txt_color;
    private TextView print_download_status;
    private TextView print_porciento;
    private TextView print_size;
    private TextView print_fileName;
    private Button btnBack;
    TextView numCopies;
    int numCopias = 1;
    File fileOut;
    String itemID;
    private TextView txtLoggedUser;
    File dir;
    DownloadFileTask downloadFileTask = null;
    public static boolean isActive = true;

    private Dialog mProgressDialog = null;
    static Dialog dialogResultDownload = null;
    static Dialog dialogResultPrint = null;
    private Dialog dialogErrorUser = null;
    public TimerTask lockTimeOutTask;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.info("***** PrintActivity onCreate *****");

        try {
            setContentView(R.layout.activity_print);
            mContext = this;
            PrintActivity.context = getApplicationContext();
            numCopias = 1;
            isActive = true;

            Preferences.print_isColor = false;
            Preferences.print_duplex = true;

            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            itemID = extras.getString("itemID");

            //(1)
            mApplication = (ScanToBoxApplication) getApplication();

            dir = new File("/mnt/hdd/" + mApplication.getApplicationContext().getPackageName() + "/downloads/");
            deleteInside(dir);

            //(3)
            mButtonStart = (RelativeLayout) findViewById(R.id.btn_start);
            mButtonStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    log.debug("***** PrintActivity onClick START *****");

                    Utils.UnLock_logout(context, getPackageName());
                    if(lockTimeOutTask != null) {
                        lockTimeOutTask.cancel();
                    }

                    DialogPrint printDialog = new DialogPrint();
                    printDialog.execute(1);
                }
            });

            //(4)
            mButtonStart.setEnabled(false);

            btnBack = (Button) findViewById(R.id.btnBackFromScan);
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    log.debug("***** PrintActivity onClick BACK *****");

                    Utils.UnLock_logout(context, getPackageName());
                    if(lockTimeOutTask != null) {
                        lockTimeOutTask.cancel();
                    }

                    finish();
                }
            });

            // set application state to Normal
            mApplication.setAppState(PrintActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);

            txt_duplex = (TextView) findViewById(R.id.txt_duplex);
            img_duplex = (ImageView) findViewById(R.id.img_duplex);
            txt_color = (TextView) findViewById(R.id.txt_color);
            numCopies = (TextView) findViewById(R.id.txt_numCopies);
            numCopies.setText(String.valueOf(numCopias));

            print_download_status = (TextView) findViewById(R.id.print_download_status);
            print_download_status.setText(context.getResources().getString(R.string.txt_print_proceso1));
            print_porciento = (TextView) findViewById(R.id.print_porciento);
            print_porciento.setText("0%");
            print_size = (TextView) findViewById(R.id.print_size);
            print_size.setText("");
            print_fileName = (TextView) findViewById(R.id.print_fileName);
            print_fileName.setText("");

            txtLoggedUser = (TextView) findViewById(R.id.txtLoggedUser);
            txtLoggedUser.setText(MainActivity.loginUser);

            initialize();
            initializeListener();
            changeUI();

            lockTimeOutTask = new TimerTask() {
                @Override
                public void run() {
                    log.debug("Lanzando lockTimeOutTask");
                    Utils.UnLock_logout(mContext, getPackageName());
                }
            };

            downloadFileTask = new DownloadFileTask();
            downloadFileTask.execute(1);

        } catch (NullPointerException ex) {
            log.error(Utils.printStackTraceToString(ex));
            finish();
        }
    }

    public void copy_plus(View v){
        if(numCopias < 99) {
            numCopias++;
            numCopies.setText(String.valueOf(numCopias));
        }
    }

    public void copy_min(View v){
        if(numCopias > 1) {
            numCopias--;
            numCopies.setText(String.valueOf(numCopias));
        }
    }

    public static void showMSG_Print_Process(boolean result){

        String msgSuccess = mContext.getResources().getString(R.string.txt_success_print);
        String msgError = mContext.getResources().getString(R.string.txt_error_print_process);

        if(result) {
            dialogResultPrint = DialogUtil.createSuccessDialog(mContext, msgSuccess);
            dialogResultPrint.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface paramDialogInterface) {
                    mContext.gotoSplash();
                }
            });
            dialogResultPrint.show();

        }else{
            dialogResultPrint = DialogUtil.createErrorDialog(mContext, msgError);
            dialogResultPrint.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface paramDialogInterface) {
                    //mContext.gotoSplash();
                }
            });
            dialogResultPrint.show();
        }
    }

    public static void showMSG_Download_Process(){

        String msgError = mContext.getResources().getString(R.string.txt_error_download_msg);

        dialogResultDownload = DialogUtil.createErrorDialog(mContext, msgError);
        dialogResultDownload.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //mContext.gotoSplash();
            }
        });
        dialogResultDownload.show();

    }

    public void gotoMain(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void gotoSplash(){
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void showErrorMsg(){

        String msgError = mContext.getResources().getString(R.string.txt_error_box_user_msg);

        dialogErrorUser = DialogUtil.createErrorDialog(mContext, msgError);
        dialogErrorUser.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                mContext.gotoSplash();
            }
        });
        dialogErrorUser.show();
    }

    private class DownloadFileTask extends AsyncTask<Integer, Integer, Integer> {

        PrintActivity activity;
        boolean procesoDownload;
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.progress);
        int nRetry = 0;
        int maxSizeDownloaded = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            procesoDownload = false;
            activity = PrintActivity.this;

            print_download_status.setText(context.getResources().getString(R.string.txt_print_proceso1));
            print_porciento.setText("0%");
            print_size.setText("");
            print_fileName.setText("");

            //Lock APP
            Utils.Lock_logout(activity, getPackageName());
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            int pages = params[0];

            log.debug("***** Lanzando proceso DIALOG_DOWNLOAD *****");
            FileOutputStream stream = null;

            try {

                BoxFile file = new BoxFile(BoxApiManager.getBoxApi(SplashActivity.currentUser), itemID);
                BoxFile.Info info = null;

                nRetry = 0;
                while (nRetry < 3) {

                    log.debug("-> retry " + nRetry);

                    try {
                        info = file.getInfo();

                        log.debug("File Size: " + info.getSize());
                        if(info.getSize() <= 0){
                            procesoDownload = false;
                        }else{
                            procesoDownload = true;
                        }

                        break;
                    } catch (BoxAPIException e) {

                        //  TODO: for test
                        log.error("********* BoxAPIException *********");
                        log.error("*** Cause: " + e.getCause());

                        BeanBoxApiException bean = Utils.getBoxAPIExceptionInfo(e);
                        log.debug("status: " + bean.status);
                        log.debug("code: " + bean.code);
                        log.debug("message: " + bean.message);

                        log.error(Utils.printStackTraceToString(e));

                        if (e.getCause() instanceof IOException) {
                            nRetry++;
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ie) {
                                log.error(Utils.printStackTraceToString(ie));
                            }
                        } else {
                            break;
                        }
                    } catch (Exception e) {
                        log.error(Utils.printStackTraceToString(e));
                        break;
                    }
                }

                if(procesoDownload){

                    procesoDownload = false;
                    nRetry = 0;
                    while (nRetry < 3) {

                        log.debug("-> retry " + nRetry);

                        try {

                            dir.mkdir();
                            deleteInside(dir);

                            fileOut = new File("/mnt/hdd/" + mApplication.getApplicationContext().getPackageName() + "/downloads/" + info.getName());

                            stream = new FileOutputStream(fileOut);

                            final ProgressListener listener = new ProgressListener() {
                                public void onProgressChanged(long numBytes, long totalBytes) {
                                    //log.debug("numBytes: " + numBytes);
                                    //log.debug("totalBytes: " + totalBytes);

                                    progressBar.setMax((int)totalBytes);

                                    if(maxSizeDownloaded < numBytes) {
                                        maxSizeDownloaded = (int)numBytes;
                                    }

                                    publishProgress(maxSizeDownloaded, (int)totalBytes);
                                }};

                            int BUFFER_SIZE = 8192;
                            MyURLTemplate CONTENT_URL_TEMPLATE = new MyURLTemplate("files/%s/content");
                            URL url = CONTENT_URL_TEMPLATE.build(BoxApiManager.getBoxApi(SplashActivity.currentUser).getBaseURL(), itemID);
                            BoxAPIRequest request = new BoxAPIRequest(BoxApiManager.getBoxApi(SplashActivity.currentUser), url, "GET");
                            BoxAPIResponse response = request.send();
                            InputStream input = response.getBody(listener);

                            byte[] buffer = new byte[BUFFER_SIZE];
                            try {
                                int n = input.read(buffer);
                                while (n != -1) {
                                    stream.write(buffer, 0, n);
                                    n = input.read(buffer);

                                    if(!isActive){
                                        deleteInside(dir);
                                        break;
                                    }
                                }

                                procesoDownload = true;
                                break;

                            } catch (IOException e) {
                                throw new BoxAPIException("Couldn't connect to the Box API due to a network error.", e);
                            } finally {
                                //response.disconnect();
                                if(stream != null) {
                                    stream.close();
                                }
                            }

                        } catch (BoxAPIException e) {

                            //  TODO: for test
                            log.error("********* BoxAPIException *********");
                            log.error("*** Cause: " + e.getCause());

                            BeanBoxApiException bean = Utils.getBoxAPIExceptionInfo(e);
                            log.debug("status: " + bean.status);
                            log.debug("code: " + bean.code);
                            log.debug("message: " + bean.message);

                            log.error(Utils.printStackTraceToString(e));

                            if (e.getCause() instanceof IOException) {
                                nRetry++;
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException ie) {
                                    log.error(Utils.printStackTraceToString(ie));
                                }
                            } else {
                                break;
                            }
                        } catch (Exception e) {
                            log.error(Utils.printStackTraceToString(e));
                            break;
                        }
                    }
                }

            } catch (BoxAPIException e) {
                //  TODO: for test
                log.error("********* BoxAPIException *********");
                log.error("*** Cause: " + e.getCause());

                BeanBoxApiException bean = Utils.getBoxAPIExceptionInfo(e);
                log.debug("status: " + bean.status);
                log.debug("code: " + bean.code);
                log.debug("message: " + bean.message);

                log.error(Utils.printStackTraceToString(e));
            } catch (Exception ex){
                log.error(Utils.printStackTraceToString(ex));
            }

            log.debug("Proceso DIALOG_DOWNLOAD finalizado!");

            return 0;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            try {

                float process = ((float)values[0] / (float)values[1]) * 100;

                //FileName
                print_fileName.setText(fileOut.getName());

                //Print Size
                print_size.setText(Utils.formatFileSize(values[1]));

                //ProgressBar
                progressBar.setProgress(values[0]);

                //Status
                print_download_status.setText(context.getResources().getString(R.string.txt_print_proceso2) + " (" + Utils.formatFileSize(values[0]) + ")");

                //Porciento
                print_porciento.setText(Double.toString(Math.floor(process)) + "%");

            }catch (Exception ex){
                procesoDownload = false;
                log.error(Utils.printStackTraceToString(ex));
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            log.info("POST EXECUTE");

            try {
                if (!procesoDownload) {
                    print_download_status.setText(context.getResources().getString(R.string.txt_print_proceso_error));
                    showMSG_Download_Process();

                } else {
                    print_download_status.setText(context.getResources().getString(R.string.txt_print_proceso3));
                    print_porciento.setText("100%");
                    mButtonStart.setEnabled(true);
                }

            }catch (Exception ex){
                log.warn("ATENCIÓN: No se puede mostrar el popUp de descarga, porque el proceso ha sido cancelado....");
            }finally {
                try{

                    Timer timer = new Timer();
                    timer.schedule(activity.lockTimeOutTask, 30000);

                }catch (Exception ex){
                    Utils.printStackTraceToString(ex);
                }
            }


        }
    }

    private class DialogPrint extends AsyncTask<Integer, Integer, Integer> {

        PrintActivity activity;
        boolean proceso;
        String msgError = getResources().getString(R.string.txt_error_print);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            proceso = true;
            activity = PrintActivity.this;
        }

        @Override
        protected Integer doInBackground(Integer... params) {

            log.debug("***** Lanzando proceso DIALOG_PRINT *****");

            try{
                ArrayList<File> files = new ArrayList<File>();

                //Clean and add
                files.clear();
                files.add(fileOut);

                PrintStateMachine stateMachine = mApplication.getPrintStateMachine();
                stateMachine.setContext(PrintActivity.this);
                PrintSettingDataHolder mHolder = mApplication.getSettingDataHolder();

                Set<Class<? extends PrintRequestAttribute>> categories;
                PrintSettingSupportedHolder supportedHolder =
                        ((ScanToBoxApplication) getApplication()).getSettingSupportedDataHolders();
                categories = supportedHolder.getSelectableCategories();

                //Apply PRINT settings
                Copies copias = new Copies(numCopias);
                mHolder.setSelectedCopiesValue(copias);

                if (categories.contains(PrintColor.class)) {
                    mHolder.setSelectedPrintColorValue(PrintColor.MONOCHROME);
                    if (Preferences.print_isColor) {
                        List<PrintColor> printColorList = getSelectablePrintColorList(context);
                        if (printColorList != null) {
                            if (printColorList.contains(PrintColor.COLOR)) {
                                mHolder.setSelectedPrintColorValue(PrintColor.COLOR);
                            }
                        }
                    }
                } else {
                    mHolder.setSelectedPrintColorValue(null);
                }

                if (categories.contains(PrintSide.class)) {
                    mHolder.setSelectedPrintSideValue(null);

                    List<PrintSide> printSideList = getSelectablePrintSideList(context);
                    if (printSideList != null) {
                        if (Preferences.print_duplex) {
                            if (printSideList.contains(PrintSide.DUPLEX_TOP_TO_TOP))
                                mHolder.setSelectedPrintSideValue(PrintSide.DUPLEX_TOP_TO_TOP);
                            else if (printSideList.contains(PrintSide.DUPLEX_TOP_TO_BOTTOM))
                                mHolder.setSelectedPrintSideValue(PrintSide.DUPLEX_TOP_TO_BOTTOM);
                        } else {
                            if (printSideList.contains(PrintSide.ONE_SIDED))
                                mHolder.setSelectedPrintSideValue(PrintSide.ONE_SIDED);
                        }
                    }
                } else {
                    mHolder.setSelectedPrintSideValue(null);
                }

                if(fileOut.getName().substring(fileOut.getName().lastIndexOf("."), fileOut.getName().length()).contains("tif")){
                    mHolder.setSelectedPDL(PrintFile.PDL.TIFF);
                }else{
                    mHolder.setSelectedPDL(PrintFile.PDL.PDF);
                }

                //Check dialog showing or not
                if (mApplication.ismIsWinShowing()) {
                    proceso = false;
                    return 0;
                }
                //If no dialog displays, set flag to true
                mApplication.setmIsWinShowing(true);

                if (mHolder == null) {
                    //Close dialog, reset flag to false
                    mApplication.setmIsWinShowing(false);
                    proceso = false;
                    return 0;
                }

                mApplication.clearPrintFileNames();
                for (File f : files) {
                    mApplication.addPrintFileName(f.getAbsolutePath());
                }

                log.info("Comenzando impresión");
                mApplication.startPrint();

            }catch (Exception ex){
                log.error(Utils.printStackTraceToString(ex));
                proceso = false;
            }

            log.debug("Proceso DIALOG_PRINT finalizado!");

            return 0;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            if(values[0]==1) {
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            if (!proceso) {
                mProgressDialog = es.ricoh.scantobox.util.Dialogs.DialogUtil.createErrorDialog(activity, msgError);
                mProgressDialog.show();
            }
        }
    }

    void deleteInside(File f) {

        //TODO Borramos todos los ficheros escaneados...
        log.debug("Lanzando borrado recursivo...");

        if (f.exists()) {
            if (f.isDirectory()) {
                for (File child : f.listFiles()) {
                    child.delete();
                }
            }
        }
    }

    public void changeColor(View v) {
        //Check dialog showing or not
        if (mApplication.ismIsWinShowing()) {
            return;
        }
        //If no dialog displays, set flag to true
        mApplication.setmIsWinShowing(true);

        Dialog dialog = DialogUtil.createColorPrintSettingDialog(PrintActivity.this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //Close dialog, reset flag to false
                mApplication.setmIsWinShowing(false);

                changeUI();
            }
        });
        dialog.show();
    }

    public void changeSide(View v) {
        //Check dialog showing or not
        if (mApplication.ismIsWinShowing()) {
            return;
        }
        //If no dialog displays, set flag to true
        mApplication.setmIsWinShowing(true);

        Dialog dialog = SideDialog.createPrintSideDialog(PrintActivity.this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface paramDialogInterface) {
                //Close dialog, reset flag to false
                mApplication.setmIsWinShowing(false);

                changeUI();
            }
        });
        dialog.show();
    }

    private void changeUI() {

        if (Preferences.print_duplex) {
            txt_duplex.setText(R.string.txt_duplexOn_scan);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                img_duplex.setBackground(context.getResources().getDrawable(R.drawable.icon_setting_paper_duplex02));
            } else {
                img_duplex.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_setting_paper_duplex02));
            }
        }else{
            txt_duplex.setText(R.string.txt_duplexOff_scan);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                img_duplex.setBackground(context.getResources().getDrawable(R.drawable.icon_setting_paper_duplex01));
            } else {
                img_duplex.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.icon_setting_paper_duplex01));
            }
        }

        if (Preferences.print_isColor) {
            txt_color.setText(R.string.txt_scolor_color);
        } else {
            txt_color.setText(R.string.txt_scolor_BW);
        }
    }

    /**
     * Starts the return request from energy saving task.
     */
    private void startReturnRequestFromEnergySavingTask() {
        if (mReturnRequestFromEnergySavingTask != null) {
            mReturnRequestFromEnergySavingTask.cancel(true);
        }
        mReturnRequestFromEnergySavingTask = new ReturnRequestFromEnergySavingTask();
        mReturnRequestFromEnergySavingTask.execute();
    }

    /**
     * Stop the return request from energy saving task.
     */
    private void stopReturnRequestFromEnergySavingTask() {
        if (mReturnRequestFromEnergySavingTask != null) {
            mReturnRequestFromEnergySavingTask.cancel(true);
            mReturnRequestFromEnergySavingTask = null;
        }
    }

    /**
     * This is an asynchronous task to check the machine's power mode and to request to recover from Energy Saver mode if necessary.
     * This task only requests to recover from Energy Saver mode.
     * To check if the machine has recovered from Energy Saver mode, enable to check power mode notification.
     */
    class ReturnRequestFromEnergySavingTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean needsRequest;

            int powerMode = mApplication.getSystemStateMonitor().getPowerMode();
            Log.d(Const.TAG, PREFIX + "getPowerMode=" + powerMode);

            switch (powerMode) {
                case SystemStateMonitor.POWER_MODE_ENGINE_OFF:
                case SystemStateMonitor.POWER_MODE_OFF_MODE:
                case SystemStateMonitor.POWER_MODE_UNKNOWN:
                    needsRequest = true;
                    break;
                default:
                    needsRequest = false;
                    break;
            }

            Boolean result = Boolean.FALSE;
            if (needsRequest) {
                result = mApplication.controllerStateRequest(SmartSDKApplication.REQUEST_CONTROLLER_STATE_FUSING_UNIT_OFF);
            }
            return result;
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        }
        return false;
    }

    /**
     * Called when the activity is resumed.
     * Checks error occurrence asynchronously and switches to a system warning screen if necessary.
     * Checks machine's power mode asynchronously and requests system state if necessary.
     */
    @Override
    protected void onResume() {
        log.debug("***** PrintActivity onResume *****");
        super.onResume();

        if (SplashActivity.currentUser == null || SplashActivity.currentUser.isEmpty()) {
            log.warn("Current user is = " + SplashActivity.currentUser);
            log.warn("Forcing to finish");
            //finish();// TODO: reemplazar por showErrorMsg() que vuelva a Splash
            showErrorMsg();
        }

        isActive = true;
        mIsForeground = true;
        processSystemResetLock();
        startReturnRequestFromEnergySavingTask();

        changeUI();
    }


    /**
     * Called when the activity is stopped.
     * If the system warning screen display task is in process, the task is cancelled.
     */
    @Override
    protected void onPause() {
        log.debug("***** PrintActivity onPause *****");
        super.onPause();

        log.debug("---> DETENIENDO LA TASK... <---");
        isActive = false;
        downloadFileTask.cancel(true);

        Utils.UnLock_logout(context, getPackageName());
        if(lockTimeOutTask != null) {
            lockTimeOutTask.cancel();
        }

        if (dialogErrorUser != null) {
            dialogErrorUser.dismiss();
        }

        if (dialogResultDownload != null) {
            dialogResultDownload.dismiss();
        }

        if (dialogResultPrint != null) {
            dialogResultPrint.dismiss();
        }

        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }

        // システムリセットロックを解除します
        mIsForeground = false;
        processSystemResetUnlock();
    }

    /**
     * Called when the activity is destroyed.
     * [Processes]
     * (1) Send G_Scan destoyed event to the state machine.
     * If scanning is in process, scanning is cancelled.
     * (2) Removes the event listener and the broadcast receiver from the service.
     * (3) If asynchronous task is in process, the task is cancelled.
     * (4) Initializes the data saved to the application.
     * (5) Discards references
     */
    @Override
    protected void onDestroy() {
        log.debug("***** PrintActivity onDestroy *****");
        super.onDestroy();

        try {

            if (dialogErrorUser != null) {
                dialogErrorUser.dismiss();
            }

            if (dialogResultDownload != null) {
                dialogResultDownload.dismiss();
            }

            if (dialogResultPrint != null) {
                dialogResultPrint.dismiss();
            }

            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }

            deleteInside(dir);

            Utils.UnLock_logout(context, getPackageName());
            if(lockTimeOutTask != null) {
                lockTimeOutTask.cancel();
            }

            mApplication.setmIsWinShowing(false);

            processSystemResetUnlock();

            // set application state to Normal
            mApplication.setAppState(PrintActivity.class.getName(), SmartSDKApplication.APP_STATE_NORMAL, SmartSDKApplication.APP_STATE_NORMAL_MSG, SmartSDKApplication.APP_TYPE_SCANNER);

            //(4)
            mApplication.init();

            //(5)
            mApplication = null;
            mButtonStart = null;

            if (mServiceAttributeListener != null) {
                PrintService service = ((ScanToBoxApplication) getApplication()).getPrintService();
                service.removePrintServiceAttributeListener(mServiceAttributeListener);
            }
        } catch (NullPointerException ex) {
            return;
        }
    }

    /**
     * Locking system auto reset.
     * When job is executing and MainAcitivity is in foreground, set system auto reset of SDKService.
     */
    private void processSystemResetLock() {
        if (mIsForeground && mRequestedSystemResetLock) {
            mApplication.lockSystemReset();
        }
    }

    /**
     * Clear locking system auto reset.
     * To avoid the continuation of locked state without intention, Clear locking
     * even it is not requested.
     */
    private void processSystemResetUnlock() {
        mApplication.unlockSystemReset();
    }

    /**
     * Creates the state string to be passed to system warning screen display request.
     *
     * @param state State of scan service
     * @return State string
     */
    public String makeAlertStateString(PrinterState state) {
        String stateString = "";
        if (state != null) {
            stateString = state.toString();
        }
        return stateString;
    }


    /**
     * Creates the state reason string to be passed to the system warning screen display request.
     * If multiple state reasons exist, only the first state reason is passed.
     *
     * @param stateReasons Scan service state reason
     * @return State reason string
     */
    public String makeAlertStateReasonString(PrinterStateReasons stateReasons) {
        String reasonString = "";
        if (stateReasons != null) {
            Object[] reasonArray = stateReasons.getReasons().toArray();
            if (reasonArray != null && reasonArray.length > 0) {
                reasonString = reasonArray[0].toString();
            }
        }
        return reasonString;
    }

    /**
     * Obtains whether or not the specified application is in the foreground state.
     *
     * @param packageName Application package name
     * @return If the application is in the foreground state, true is returned.
     */
    public boolean isForegroundApp(String packageName) {
        boolean result = false;
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> list = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : list) {
            if (packageName.equals(info.processName)) {
                result = (info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND);
                break;
            }
        }
        return result;
    }

    /**
     * Obtains the top class in the activity stack of the specified application.
     *
     * @param packageName Application package name
     * @return The name of the FQCN class name of the top class. If the name cannot be obtained, null is returned.
     */
    public String getTopActivityClassName(String packageName) {
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(30);
        for (ActivityManager.RunningTaskInfo info : list) {
            if (packageName.equals(info.topActivity.getPackageName())) {
                return info.topActivity.getClassName();
            }
        }
        return null;
    }

    public static Context getAppContext() {
        return PrintActivity.context;
    }

    public static ScanToBoxApplication getmApplication() {
        return mApplication;

    }


    /***************************Print service ***************************************/
    /**
     * Initialize the application.
     * [Processes]
     * (1)Create the listener that receives events from PrintService.
     * (2)Create and start the PrintService initialize task.
     * (3)Post the initial event to state machine.
     */
    private void initializeListener() {
        //(1)
        mServiceAttributeListener = new PrintServiceAttributeListenerImpl(this, new Handler());

        //(2)
        new PrintServiceInitTask().execute(mServiceAttributeListener);

        //(3)
        ((ScanToBoxApplication) getApplication()).getPrintStateMachine().procPrintEvent(
                PrintStateMachine.PrintEvent.CHANGE_APP_ACTIVITY_INITIAL);
    }

    /**
     * Initializes print setting values.
     * [Processes]
     * (1)Obtains the supported value of print file PDL
     * (2)Set the initial value to the available settings.
     */
    private void initPrintSetting() {
        //(1)
        Set<Class<? extends PrintRequestAttribute>> categories;
        PrintSettingSupportedHolder supportedHolder =
                ((ScanToBoxApplication) getApplication()).getSettingSupportedDataHolders();
        categories = supportedHolder.getSelectableCategories();

        //(2)
        if (categories.contains(Copies.class)) {
            int numCopies = Integer.parseInt(
                    supportedHolder.getSelectableCopiesRange().getMinValue());
            mHolder.setSelectedCopiesValue(new Copies(numCopies));
        } else {
            mHolder.setSelectedCopiesValue(null);
        }
        if (categories.contains(PrintColor.class)) {
            mHolder.setSelectedPrintColorValue(PrintColor.MONOCHROME);
            if (Preferences.print_isColor) {
                List<PrintColor> printColorList = getSelectablePrintColorList(context);
                if (printColorList != null) {
                    if (printColorList.contains(PrintColor.COLOR)) {
                        mHolder.setSelectedPrintColorValue(PrintColor.COLOR);
                    }
                }
            }
        } else {
            mHolder.setSelectedPrintColorValue(null);
        }
        if (categories.contains(PrintResolution.class)) {
            mHolder.setSelectedPrintResolutionValue(null);

            List<PrintResolution> printResolutionList = getSelectablePrintResolutionList(context);
            if (printResolutionList != null) {
                if (Preferences.print_resolutionDPI == 600) {
                    if (printResolutionList.contains(PrintResolution.DPI_600_600_2))
                        mHolder.setSelectedPrintResolutionValue(PrintResolution.DPI_600_600_2);
                    else if (printResolutionList.contains(PrintResolution.DPI_600_600_1))
                        mHolder.setSelectedPrintResolutionValue(PrintResolution.DPI_600_600_1);
                    else if (printResolutionList.contains(PrintResolution.DPI_600_600_4))
                        mHolder.setSelectedPrintResolutionValue(PrintResolution.DPI_600_600_4);
                    else if (printResolutionList.contains(PrintResolution.DPI_600_600_8))
                        mHolder.setSelectedPrintResolutionValue(PrintResolution.DPI_600_600_8);
                }
                if (Preferences.print_resolutionDPI == 400) {
                    if (printResolutionList.contains(PrintResolution.DPI_400_400_1))
                        mHolder.setSelectedPrintResolutionValue(PrintResolution.DPI_400_400_1);
                }
                if (Preferences.print_resolutionDPI == 300) {
                    if (printResolutionList.contains(PrintResolution.DPI_300_300_1))
                        mHolder.setSelectedPrintResolutionValue(PrintResolution.DPI_300_300_1);
                }
                if (Preferences.print_resolutionDPI == 200) {
                    if (printResolutionList.contains(PrintResolution.DPI_200_200_1))
                        mHolder.setSelectedPrintResolutionValue(PrintResolution.DPI_200_200_1);
                }
            }
        } else {
            mHolder.setSelectedPrintResolutionValue(null);
        }
        if (categories.contains(PrintSide.class)) {
            mHolder.setSelectedPrintSideValue(null);

            List<PrintSide> printSideList = getSelectablePrintSideList(context);
            if (printSideList != null) {
                if (Preferences.print_duplex) {
                    if (printSideList.contains(PrintSide.DUPLEX_TOP_TO_TOP))
                        mHolder.setSelectedPrintSideValue(PrintSide.DUPLEX_TOP_TO_TOP);
                    else if (printSideList.contains(PrintSide.DUPLEX_TOP_TO_BOTTOM))
                        mHolder.setSelectedPrintSideValue(PrintSide.DUPLEX_TOP_TO_BOTTOM);
                } else {
                    if (printSideList.contains(PrintSide.ONE_SIDED))
                        mHolder.setSelectedPrintSideValue(PrintSide.ONE_SIDED);
                }
            }
        } else {
            mHolder.setSelectedPrintSideValue(null);
        }
        if (categories.contains(PaperTray.class)) {
            mHolder.setSelectedPaperTrayValue(PaperTray.MAIN);
        } else {
            mHolder.setSelectedPaperTrayValue(null);
        }

        mApplication.setPrintSettingDataHolder(mHolder);
    }

    /**
     * Obtains the list of supported colors.
     *
     * @param context Context of the activity
     * @return List of supported PrintColor objects.
     */
    public static List<PrintColor> getSelectablePrintColorList(Context context) {
        ScanToBoxApplication app = (ScanToBoxApplication) context.getApplicationContext();
        PrintSettingSupportedHolder supportedSettings = app.getSettingSupportedDataHolders();

        return supportedSettings.getSelectablePrintColorList();
    }

    /**
     * Obtains the list of supported colors.
     *
     * @param context Context of the activity
     * @return List of supported PrintResolution objects.
     */
    public static List<PrintResolution> getSelectablePrintResolutionList(Context context) {
        ScanToBoxApplication app = (ScanToBoxApplication) context.getApplicationContext();
        PrintSettingSupportedHolder supportedSettings = app.getSettingSupportedDataHolders();

        return supportedSettings.getSelectablePrintResolutionList();
    }

    /**
     * Obtains the list of supported colors.
     *
     * @param context Context of the activity
     * @return List of supported PrintSide objects.
     */
    public static List<PrintSide> getSelectablePrintSideList(Context context) {
        ScanToBoxApplication app = (ScanToBoxApplication) context.getApplicationContext();
        PrintSettingSupportedHolder supportedSettings = app.getSettingSupportedDataHolders();

        return supportedSettings.getSelectablePrintSideList();
    }

    /**
     * Initializes the application.
     * StateMachine is initialized here.
     */
    private void initialize() {
        mApplication = (ScanToBoxApplication) getApplication();
        PrintStateMachine stateMachine = mApplication.getPrintStateMachine();
        stateMachine.setContext(this);
    }

    /**
     * Connects with the print service asynchronously.
     */
    class PrintServiceInitTask extends AsyncTask<PrintServiceAttributeListener, Void, Integer> {

        AsyncConnectState addListenerResult = null;
        AsyncConnectState getAsyncConnectStateResult = null;

        /**
         * Runs in the background on the UI thread.
         * [Processes]
         * (1) Sets the listener to receive print service events.
         * This task repeats until the machine becomes available or cancel button is touched.
         * (2) Confirms the asynchronous connection.
         * This task repeats until the connection is confirmed or cancel button is touched.
         * (3) After the machine becomes available and connection is confirmed,
         * obtains job setting values for PDF.
         */
        @Override
        protected Integer doInBackground(PrintServiceAttributeListener... listeners) {

            PrintService printService = ((ScanToBoxApplication) getApplication()).getPrintService();

            //(1)
            while (true) {
                if (isCancelled()) {
                    return -1;
                }
                addListenerResult = printService.addPrintServiceAttributeListener(listeners[0]);

                if (addListenerResult == null) {
                    sleep(100);
                    continue;
                }

                if (addListenerResult.getState() == AsyncConnectState.STATE.CONNECTED) {
                    break;
                }

                if (addListenerResult.getErrorCode() == AsyncConnectState.ERROR_CODE.NO_ERROR) {
                    // do nothing
                } else if (addListenerResult.getErrorCode() == AsyncConnectState.ERROR_CODE.BUSY) {
                    sleep(10000);
                } else if (addListenerResult.getErrorCode() == AsyncConnectState.ERROR_CODE.TIMEOUT) {
                    // do nothing
                } else if (addListenerResult.getErrorCode() == AsyncConnectState.ERROR_CODE.INVALID) {
                    return 0;
                } else {
                    // unknown state
                    return 0;
                }
            }

            if (addListenerResult.getState() != AsyncConnectState.STATE.CONNECTED) {
                return 0;
            }

            //(2)
            while (true) {
                if (isCancelled()) {
                    return -1;
                }
                getAsyncConnectStateResult = printService.getAsyncConnectState();

                if (getAsyncConnectStateResult == null) {
                    sleep(100);
                    continue;
                }

                if (getAsyncConnectStateResult.getState() == AsyncConnectState.STATE.CONNECTED) {
                    break;
                }

                if (getAsyncConnectStateResult.getErrorCode() == AsyncConnectState.ERROR_CODE.NO_ERROR) {
                    // do nothing
                } else if (getAsyncConnectStateResult.getErrorCode() == AsyncConnectState.ERROR_CODE.BUSY) {
                    sleep(10000);
                } else if (getAsyncConnectStateResult.getErrorCode() == AsyncConnectState.ERROR_CODE.TIMEOUT) {
                    // do nothing
                } else if (getAsyncConnectStateResult.getErrorCode() == AsyncConnectState.ERROR_CODE.INVALID) {
                    return 0;
                } else {
                    // unknown state
                    return 0;
                }
            }

            //(3)
            if (addListenerResult.getState() == AsyncConnectState.STATE.CONNECTED
                    && getAsyncConnectStateResult.getState() == AsyncConnectState.STATE.CONNECTED) {

                List<PrintFile.PDL> supportedPDL = printService.getSupportedPDL();
                if (supportedPDL == null) return null;

                for (PrintFile.PDL pdl : supportedPDL) {
                    if (pdl == PrintFile.PDL.PDF)
                        ((ScanToBoxApplication) getApplication()).
                                setPrintSettingSupportedHolder(new PrintSettingSupportedHolder(printService, pdl));
                }

            }

            return 0;
        }

        /**
         * Called after doInBackground().
         */
        @Override
        protected void onPostExecute(Integer result) {

            if (result != 0) {
                // canceled.
                return;
            }

            if (addListenerResult.getState() == AsyncConnectState.STATE.CONNECTED
                    && getAsyncConnectStateResult.getState() == AsyncConnectState.STATE.CONNECTED) {
                // connection succeeded.
                initPrintSetting();
                ((ScanToBoxApplication) getApplication()).getPrintStateMachine().procPrintEvent(
                        PrintStateMachine.PrintEvent.CHANGE_APP_ACTIVITY_STARTED);
            } else {
                // the connection is invalid.
                ((ScanToBoxApplication) getApplication()).getPrintStateMachine().procPrintEvent(
                        PrintStateMachine.PrintEvent.CHANGE_APP_ACTIVITY_START_FAILED);
            }
        }

        /**
         * Sleep for the whole of the specified interval
         */
        private void sleep(long time) {
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                log.debug(e.toString());
            }
        }
    }
}

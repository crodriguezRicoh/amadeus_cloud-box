package es.ricoh.scantobox.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Properties;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.application.FileChooser;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.CustomToast;
import es.ricoh.scantobox.util.Preferences;
import es.ricoh.scantobox.util.Utils;

/**
 * Created by cristian.rodriguez on 27/01/2017.
 * Checked: OK!
 */

public class ConfigActivity extends Activity {

    private static final Logger log = ALogger.getLogger(ConfigActivity.class);
    public static final int REQUEST_CODE_CONFIG_ACTIVITY = 1;

    private TextView pathProperties;
    private Button cargarProperties;
    private EditText clientID;
    private EditText clientSecret;
    private EditText redirectURL;
    private EditText oauthURL;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        log.debug("***** ConfigActivity onCreate *****");
        setContentView(R.layout.activity_config);

        //Recuperamos los componentes.
        cargarProperties = (Button) findViewById(R.id.cargarProperties);
        pathProperties = (TextView) findViewById(R.id.pathProperties);
        pathProperties.setKeyListener(null);
        clientID = (EditText) findViewById(R.id.text_clientid);
        clientSecret = (EditText) findViewById(R.id.text_clientsecret);
        redirectURL = (EditText) findViewById(R.id.text_redirecturl);
        oauthURL = (EditText) findViewById(R.id.text_oauthURL);

        //Mostramos los valores ya guardados.
        clientID.setText(Preferences.clientID);
        clientSecret.setText(Preferences.clientSecret);
        redirectURL.setText(Preferences.redirectURL);
        oauthURL.setText(Preferences.oauthURL);
    }

    public void onClickGuardar(View v) {

        log.debug("***** onClick guardar config *****");

        //Primero se guarda en las preferencias de memoria.
        Preferences.clientID = clientID.getText().toString();
        Preferences.clientSecret = clientSecret.getText().toString();
        Preferences.redirectURL = redirectURL.getText().toString();
        Preferences.oauthURL = oauthURL.getText().toString();

        //Segundo se guarda en el fichero.
        Properties prop = new Properties();
        OutputStreamWriter output = null;
        File dir = new File("/mnt/hdd/" + getPackageName() + "/");
        if (!dir.exists())
            dir.mkdirs();
        else {
            File f = new File("/mnt/hdd/" + getPackageName() + "/config.properties");
            if (f.exists())
                f.delete();
        }

        try {

            log.debug("Guardando config en fichero de properties");
            output = new OutputStreamWriter(new FileOutputStream("/mnt/hdd/" + getPackageName() + "/config.properties"));

            //Creamos las propiedades en el fichero.
            prop.setProperty("clientID", Preferences.clientID);
            prop.setProperty("clientSecret", Preferences.clientSecret);
            prop.setProperty("redirectURL", Preferences.redirectURL);
            prop.setProperty("oauthURL", Preferences.oauthURL);
            prop.setProperty("serialNumber", Preferences.serialNumber);

            log.debug("Guardando: Serial Number --> " + Preferences.serialNumber);

            //Guardamos en HDD
            prop.store(output, null);

        } catch (IOException io) {
            log.error(Utils.printStackTraceToString(io));
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    log.error(Utils.printStackTraceToString(e));
                }
            }
        }

        finish();
    }

    public void onClickCancelar(View v) {
        finish();
    }

    public void onClickGuardarLogs(View v) {

        log.debug("***** onClick Guardar Logs *****");

        File properties = new File("/mnt/hdd/" + getPackageName() + "/log/scantobox.log");
        File propertiesSD = new File("/mnt/sdcard/scantobox.log");

        try {
            copyFile(properties, propertiesSD);
            Toast.makeText(ConfigActivity.this, R.string.text_config_successSD, Toast.LENGTH_LONG).show();
            //CustomToast.showCustomDownAlert(ConfigActivity.this, getString(R.string.text_config_successSD));

        } catch (IOException e) {
            log.error(Utils.printStackTraceToString(e));
            Toast.makeText(ConfigActivity.this, R.string.text_config_errorSD, Toast.LENGTH_LONG).show();
            //CustomToast.showCustomDownAlert(ConfigActivity.this, getString(R.string.text_config_errorSD));
        }
    }

    public void copyFile(File sourceFile, File destFile) throws IOException {

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel origen = null;
        FileChannel destino = null;
        try {
            origen = new FileInputStream(sourceFile).getChannel();
            destino = new FileOutputStream(destFile).getChannel();

            long count = 0;
            long size = origen.size();
            while ((count += destino.transferFrom(origen, count, size - count)) < size) ;
        } finally {
            if (origen != null) {
                origen.close();
            }
            if (destino != null) {
                destino.close();
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        }
        return false;
    }

    public void buscarProperties(View v) {

        String state = Environment.getExternalStorageState();

        if (state.equals(Environment.MEDIA_MOUNTED)) {
            ArrayList<String> extensionList = new ArrayList<String>();
            extensionList.add(".properties");

            try {

                FileChooser fChooser = new FileChooser(this, extensionList);
                fChooser.setFileListener(new FileChooser.FileSelectedListener() {
                    @Override
                    public void fileSelected(File file) {

                        pathProperties.setText(file.toString());
                        cargarProperties.setEnabled(true);

                    }
                }).showDialog();
            } catch (Exception ex) {
                log.error(Utils.printStackTraceToString(ex));
            }
        } else {
            Toast.makeText(ConfigActivity.this, R.string.text_config_errorNoSD, Toast.LENGTH_LONG).show();
        }
    }

    public void cargarProperties(View v) {

        log.debug("***** Cargando properties *****");

        readFromPFilePreferences(getApplicationContext(), new File(pathProperties.getText().toString()));
    }

    public void readFromPFilePreferences(Context context, File config) {

        Properties prop = new Properties();
        InputStreamReader input = null;

        try {

            input = new InputStreamReader(new FileInputStream(config));
            prop.load(input);

            clientID.setText(prop.getProperty("clientID"));
            clientSecret.setText(prop.getProperty("clientSecret"));
            redirectURL.setText(prop.getProperty("redirectURL"));
            oauthURL.setText(prop.getProperty("oauthURL"));

            log.debug("Encontradas: " + prop.size() + " Properties");


        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(Utils.printStackTraceToString(ex));

            clientID.setText("");
            clientSecret.setText("");
            redirectURL.setText("");
            Toast.makeText(ConfigActivity.this, R.string.generic_error_readfile, Toast.LENGTH_LONG).show();

        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    log.error(Utils.printStackTraceToString(e));
                    e.printStackTrace();
                }
            }
        }
    }
}

package es.ricoh.scantobox.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.log4j.Logger;

import es.ricoh.scantobox.R;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Dialogs.DialogUtil;
import es.ricoh.scantobox.util.Preferences;
import es.ricoh.scantobox.util.Utils;
import uk.co.senab.photoview.IPhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;


/**
 * Created by cristian.rodriguez on 27/01/2017.
 * Checked: OK!
 */

public class PreviewActivity extends Activity {

    private static final Logger log = ALogger.getLogger(PreviewActivity.class);

    //Request code of preview activity
    public static final int REQUEST_CODE_PREVIEW_ACTIVITY = 2000;

    //Total number of thumbnails.
    private int mTotalPage;

    //Page number of the currently displayed thumbnail.
    private int mCurrentPage;

    //The text label to indicate the total page
    private TextView mTotalPageNoView;

    //The text label to indicate the current page
    private TextView mCurrentPageNoView;

    //Next page button
    private View mNextPageArrow;

    //Previout page button
    private View mPrevPageArrow;

    //Progress bar
    private ProgressBar mProgressBar;

    //Thumbnail image view
    private ImageView mPreviewImage;

    private int mPreviewImageWidth = 0;
    private int mPreviewImageHeight = 0;

    int position = 0;

    //Zoom in button
    private Button mZoomInBtn;

    //Zoom out button
    private Button mZoomOutBtn;

    PhotoViewAttacher mAttacher;

    private TextView txtLoggedUser;

    ThumbnailImageData data;

    Dialog dialog;

    //The task to load thumbnail image
    private LoadThumbnailImageTask mLoaderTask;

    protected void onCreate(Bundle savedInstanceState) {
        log.info("***** PreviewActivity onCreate *****");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_preview);

        //(1)
        final int pages = getIntent().getIntExtra("pages", 1);
        //BeanLote bLote = (BeanLote) getIntent().getSerializableExtra("lote");

        mTotalPageNoView = (TextView) findViewById(R.id.text_total_page);
        mCurrentPageNoView = (TextView) findViewById(R.id.text_cur_page);
        mNextPageArrow = findViewById(R.id.btn_page_next);
        mPrevPageArrow = findViewById(R.id.btn_page_prev);
        mZoomInBtn = (Button) findViewById(R.id.zoomIn);
        mZoomOutBtn = (Button) findViewById(R.id.zoomOut);

        mProgressBar = (ProgressBar) findViewById(R.id.preview_proc_bar);
        mProgressBar.setVisibility(View.INVISIBLE);
        mPreviewImage = (ImageView) findViewById(R.id.image_preview);
        mPreviewImage.setScaleType(ImageView.ScaleType.MATRIX);
        mAttacher = new PhotoViewAttacher(mPreviewImage, mZoomInBtn, mZoomOutBtn);

        //(2)
        mTotalPage = 1;
        mCurrentPage = 1;
        new GetTotalPageTask().execute(pages);


        //(3)
        Button btn_cancel = (Button)findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                log.debug("***** onClick Cancelar *****");
                cancelProcess(pages);
            }
        });

        //(4)
        Button btn_start = (Button)findViewById(R.id.btn_start);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                log.debug("***** onClick Aceptar *****");
                log.debug("Pasando al siguiente proceso.");
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("pages", pages);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        //(5)
        findViewById(R.id.btn_page_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentPage >= mTotalPage) {
                    return;
                }
                mCurrentPage++;
                updatePageNavigator();

                if (mLoaderTask != null) {
                    mLoaderTask.cancel(false);
                }
                mLoaderTask = new LoadThumbnailImageTask();
                mLoaderTask.execute(mCurrentPage);

                position ++;
            };
        });

        //(6)
        findViewById(R.id.btn_page_prev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentPage <= 1) {
                    return;
                }
                mCurrentPage--;
                updatePageNavigator();

                if (mLoaderTask != null) {
                    mLoaderTask.cancel(false);
                }
                mLoaderTask = new LoadThumbnailImageTask();
                mLoaderTask.execute(mCurrentPage);

                position --;
            };
        });

        mZoomOutBtn.getBackground().setAlpha(100);
        mZoomInBtn.setOnClickListener(getZoomInListener());
        mZoomOutBtn.setOnClickListener(getZoomOutListener());

        txtLoggedUser = (TextView) findViewById(R.id.txtLoggedUser);
        txtLoggedUser.setText(MainActivity.loginUser);

        dialog = DialogUtil.createPreviewInfoDialog(PreviewActivity.this);
        dialog.show();
    }

    public void cancelProcess(int pages){

        //Si el dialog está abierto lo cerramos.
        if(dialog != null){
            dialog.dismiss();
        }

        log.debug("Cancelando proceso de Preview");
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("pages", pages);
        setResult(-9, intent);  //Result CANCEL
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        }
        return false;
    }

    //Called when the activity is resumed.
    @Override
    protected void onResume() {
        log.info("***** PreviewActivity onResume *****");
        super.onResume();

        if (SplashActivity.currentUser == null || SplashActivity.currentUser.isEmpty()) {
            log.warn("Current user is = " + SplashActivity.currentUser);
            log.warn("Forcing to finish");
            finish();
        }

        sendBroadcast(new Intent(DialogUtil.INTENT_ACTION_SUB_ACTIVITY_RESUMED));
    }

    @Override
    protected void onDestroy(){
        log.info("***** PreviewActivity onDestroy *****");
        super.onDestroy();

        if(dialog != null){
            dialog.dismiss();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        synchronized (this) {
            mPreviewImageWidth = mPreviewImage.getWidth();
            mPreviewImageHeight = mPreviewImage.getHeight();
            notifyAll();
        }
    }

    //Updates the page labels and navigator images.
    private void updatePageNavigator() {
        mCurrentPageNoView.setText(Integer.toString(mCurrentPage));
        mTotalPageNoView.setText(Integer.toString(mTotalPage));
        mNextPageArrow.setVisibility((mCurrentPage < mTotalPage)? View.VISIBLE : View.INVISIBLE);
        mPrevPageArrow.setVisibility((mCurrentPage > 1)? View.VISIBLE : View.INVISIBLE);
    }


    //The asynchronous task to obtain the total number of pages and update the screen.
    private class GetTotalPageTask extends AsyncTask<Integer, Void, Integer> {

        @Override
        protected Integer doInBackground(Integer... params) {

            return params[0];
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (PreviewActivity.this.isFinishing()) {
                return;
            }

            mTotalPage = result;
            updatePageNavigator();

            mLoaderTask = new LoadThumbnailImageTask();
            mLoaderTask.execute(mCurrentPage);
        }
    }


    //This class stores preview image data.
    private class ThumbnailImageData {
        /** Bitmap image in which binary data is loaded */
        Bitmap bitmap;
    }

    /**
     * The asynchronous task to obtain and display the preview image of the specified page.
     * During the process, displays a progress bar.
     * Obtains the thumbnail image in byte array.
     * Obtains the ratio to fit within ImageView from the image size and then loads the image.
     */
    private class LoadThumbnailImageTask extends AsyncTask<Integer, Void, ThumbnailImageData> {

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ThumbnailImageData doInBackground(Integer... params) {
            int pageNo = params[0];

            if(data != null) {
                if (data.bitmap != null) {
                    data.bitmap.recycle();
                }
            }

            data = new ThumbnailImageData();

            boolean fileNotComplete = true;
            int errCount = 0;

            while (fileNotComplete && errCount < 50) { //cuando el fichero esta creado pero aun no se han terminado de escribir completamente
                try {

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;

                    //data.bitmap = BitmapFactory.decodeFile("/mnt/hdd/" + getApplicationContext().getPackageName() + "/scans/" + Preferences.basename + "_00" + pageNo + ".jpg", options);
                    data.bitmap = BitmapFactory.decodeFile("/mnt/hdd/" + getApplicationContext().getPackageName() + "/scans/" + Preferences.basename + String.format("%03d", pageNo) + ".jpg", options);

                    options.inSampleSize = calculateInSampleSize(options, 565, 400);
                    options.inJustDecodeBounds = false;

                    //data.bitmap = BitmapFactory.decodeFile("/mnt/hdd/" + getApplicationContext().getPackageName() + "/scans/" + Preferences.basename + "_00" + pageNo + ".jpg", options);
                    data.bitmap = BitmapFactory.decodeFile("/mnt/hdd/" + getApplicationContext().getPackageName() + "/scans/" + Preferences.basename + String.format("%03d", pageNo) + ".jpg", options);

                    fileNotComplete = data.bitmap.getWidth() <= 0 || data.bitmap.getHeight() <= 0;

                    if (isCancelled()) {
                        return null;
                    }

                } catch (Exception ex) {
                    errCount++;
                    log.error("Error parsing file to byte array: " + Utils.printStackTraceToString(ex));
                    fileNotComplete = true;
                }
            }
            return data;
        }

        public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }

            log.debug("SampleSize: " + inSampleSize);

            return inSampleSize;
        }

        @Override
        protected void onPostExecute(ThumbnailImageData result) {
            if (PreviewActivity.this.isFinishing()) {
                return;
            }

            if (!isCancelled()) {

                mPreviewImage.setImageResource(0);

                if (result.bitmap != null) {

                    mPreviewImage.setImageBitmap(result.bitmap);
                    mPreviewImage.invalidate();
                    mAttacher.update();
                    mZoomInBtn.setEnabled(true);
                    mZoomOutBtn.setEnabled(false);
                    mZoomInBtn.setEnabled(true);
                    mZoomInBtn.getBackground().setAlpha(255);
                    mZoomOutBtn.setEnabled(false);
                    mZoomOutBtn.getBackground().setAlpha(100);

                }
            }
            mProgressBar.setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onCancelled() {
            mProgressBar.setVisibility(View.INVISIBLE);
        }

    }

    private View.OnClickListener getZoomInListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float scale = mAttacher.getScale();
                if(scale < IPhotoView.DEFAULT_MID_SCALE) {
                    mAttacher.setScale(IPhotoView.DEFAULT_MID_SCALE);
                    scale = IPhotoView.DEFAULT_MID_SCALE;
                }else if(scale >= IPhotoView.DEFAULT_MID_SCALE && scale < IPhotoView.DEFAULT_MAX_SCALE){
                    mAttacher.setScale(IPhotoView.DEFAULT_MAX_SCALE);
                    scale = IPhotoView.DEFAULT_MAX_SCALE;
                }

                if (!mZoomOutBtn.isEnabled()) {
                    mZoomOutBtn.setEnabled(true);
                    mZoomOutBtn.getBackground().setAlpha(255);
                }

                if (scale >= mAttacher.getMaximumScale()) {
                    mZoomInBtn.setEnabled(false);
                    mZoomInBtn.getBackground().setAlpha(100);
                }
            }
        };
    }

    //Zoom out button listener
    private View.OnClickListener getZoomOutListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                float scale = mAttacher.getScale();
                if(scale > IPhotoView.DEFAULT_MID_SCALE) {
                    mAttacher.setScale(IPhotoView.DEFAULT_MID_SCALE);
                    scale = IPhotoView.DEFAULT_MID_SCALE;
                }else if(scale <= IPhotoView.DEFAULT_MID_SCALE && scale > IPhotoView.DEFAULT_MIN_SCALE){
                    mAttacher.setScale(IPhotoView.DEFAULT_MIN_SCALE);
                    scale = IPhotoView.DEFAULT_MIN_SCALE;
                }

                if (!mZoomInBtn.isEnabled()) {
                    mZoomInBtn.setEnabled(true);
                    mZoomInBtn.getBackground().setAlpha(255);
                }

                if (scale <= mAttacher.getMinimumScale()) {
                    mZoomOutBtn.setEnabled(false);
                    mZoomOutBtn.getBackground().setAlpha(100);
                }
            }
        };
    }

    public Button getZoomIn() {
        return mZoomInBtn;
    }

    public Button getZoomOut() {
        return mZoomOutBtn;
    }
}

package es.ricoh.scantobox.imageprocessing;

import com.googlecode.leptonica.android.Binarize;
import com.googlecode.leptonica.android.Convert;
import com.googlecode.leptonica.android.Pix;
import com.googlecode.leptonica.android.ReadFile;
import com.googlecode.leptonica.android.Rotate;
import com.googlecode.leptonica.android.Skew;
import com.googlecode.leptonica.android.WriteFile;

import org.apache.commons.net.io.Util;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;

import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Utils;
import es.ricoh.tiffdecoder.TiffCompression;
import es.ricoh.tiffdecoder.TiffUtils;

/**
 * Created by cristian.rodriguez on 09/01/2017.
 */

public class TiffEngine {

    private static final Logger log = ALogger.getLogger(TiffEngine.class);

    public TiffEngine(ArrayList<File> recognizedFiles, String outName) {

        log.debug("Entry TIFF ENGINE");

        String tiffFiles[] = new String[recognizedFiles.size()];
        String outPathMulti = "";

        try{

            log.debug("Loading " + recognizedFiles.size() + " image/s ...");
            int contador = 0;
            
            for(File file : recognizedFiles) {

                outPathMulti = file.getParent() + "/"+ outName +".tiff";

                Pix pix = ReadFile.readFile(file);

                Pix grayImage = Convert.convertTo8(pix);

                pix.recycle();

                Pix binaryImage = Binarize.binarizeByThreshold(grayImage, 190);

                grayImage.recycle();

                String filePath = file.getAbsolutePath();
                String outPath = filePath.substring(0, filePath.lastIndexOf(".")) + ".tiff";

                log.debug("FilePath: " + filePath);
                log.debug("OutPath: " + outPath);

                File outFile = new File(outPath);
                WriteFile.writeImpliedFormat(binaryImage, outFile);

                binaryImage.recycle();

                tiffFiles[contador] = outFile.getAbsolutePath();
                contador++;
            }
            
            //Creamos el MultiTiff
            log.debug("Creating MTIFF with: " + tiffFiles.length + " image/s ...");
            log.debug("Output path MULTI: " + outPathMulti);

            TiffUtils.merge(outPathMulti, tiffFiles, TiffCompression.CCITT_T6);

            //Borramos los otros TIFF's
            for(int i = 0; i < tiffFiles.length; i ++){
                File file = new File(tiffFiles[i]);
                file.delete();
            }
            
        }catch (Exception ex){
            log.error(Utils.printStackTraceToString(ex));
        }

        log.debug("Finish TIFF creation...");
    }
}

package es.ricoh.scantobox.imageprocessing;

import org.apache.log4j.Logger;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;

import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Utils;

/**
 * Created by cristian.rodriguez on 30/01/2017.
 */

public class ImageEngine {

    private static final Logger log = ALogger.getLogger(ImageEngine.class);

    static {
        try {
            System.loadLibrary("opencv_java3");
        } catch (UnsatisfiedLinkError e) {
            log.error("OpenCV error: Cannot load info library for OpenCV");
        }
    }

    public void processImages_rotateImage(File f, int angle){

        log.debug("***** Rotando imagen *****");

        Mat src = Imgcodecs.imread(f.getAbsolutePath());
        Mat dst = new Mat();

        if (angle == 270) {
            // Rotate clockwise 270 degrees
            log.debug("Rotando: " + 270);
            Core.flip(src.t(), dst, 0);
        } else if (angle == 180) {
            // Rotate clockwise 180 degrees
            log.debug("Rotando: " + 180);
            Core.flip(src, dst, -1);
        } else if (angle == 90) {
            // Rotate clockwise 90 degrees
            log.debug("Rotando: " + 90);
            Core.flip(src.t(), dst, 1);
        } else if (angle == 0) {
            // No rotation
            dst = src;
        }

        log.debug("Guardando IMAGEN rotada en disco");
        Imgcodecs.imwrite(f.getAbsolutePath(), dst);

        src.release();
        dst.release();
    }

    public void processImages_toTiffImage(File f, int angle){

        log.debug("***** Rotando imagen *****");

        Mat src = Imgcodecs.imread(f.getAbsolutePath());
        Mat dst = new Mat();

        if (angle == 270) {
            // Rotate clockwise 270 degrees
            log.debug("Rotando: " + 270);
            Core.flip(src.t(), dst, 0);
        } else if (angle == 180) {
            // Rotate clockwise 180 degrees
            log.debug("Rotando: " + 180);
            Core.flip(src, dst, -1);
        } else if (angle == 90) {
            // Rotate clockwise 90 degrees
            log.debug("Rotando: " + 90);
            Core.flip(src.t(), dst, 1);
        } else if (angle == 0) {
            // No rotation
            dst = src;
        }

        log.debug("Guardando IMAGEN rotada en disco");
        Imgcodecs.imwrite(f.getAbsolutePath(), dst);

        src.release();
        dst.release();
    }

    public void processImages_createThumbnails(ArrayList<File> originalFiles, String sourceFileNameNoExt) {
        log.debug("***** START Process Images (Creando Thumbnails) *****");

        int receipts = 0;

        int maxHeight = 400; //400 //800
        int maxWidth = 565; //565 //1130

        try {

            for (File f : originalFiles) {

                Mat src = Imgcodecs.imread(f.getAbsolutePath());

                receipts++;

                int newHeight;
                int newWidth;

                //escalamos thumbnail si es necesario (max 800px alto, max 1130 px ancho)
                if(src.height() > src.width()){
                    newHeight = src.width();
                    newWidth = src.height();

                }else{
                    newHeight = src.height();
                    newWidth = src.width();
                }

                log.debug("Tamaño de imagen ORIGINAL -->" + " Height: " + newHeight + " Width: " + newWidth);

                boolean isChanged = false;

                if(src.height()>maxHeight) {
                    newHeight = maxHeight;
                    newWidth = (src.width() * maxHeight) / src.height();
                    isChanged = true;

                }else if(src.width()>maxWidth){
                    newWidth = maxWidth;
                    newHeight = (src.height() * maxWidth) / src.width();
                    isChanged = true;
                }

                log.debug("Tamaño de imagen ESCALADO -->" + " Height: " + newHeight + " Width: " + newWidth);

                Mat cropThumbnail = null;

                if(isChanged){
                    cropThumbnail = scale(src, newWidth, newHeight);
                }else{
                    cropThumbnail = src.clone();
                    src.release();
                }

                //guardamos las facturas sueltas para la preview
                Imgcodecs.imwrite(sourceFileNameNoExt + "_00" + receipts + "_T.jpg", cropThumbnail);
                cropThumbnail.release();

            }
        }catch (Exception ex) {
            log.error(Utils.printStackTraceToString(ex));
            throw ex;
        }
    }

    public Mat scale(Mat src, int width, int height){
        Size s=new Size(width,height);
        Mat m1=src.clone();
        Mat m2=new Mat();

        Imgproc.resize(m1, m2, s);

        m1.release();
        m1=null;

        return m2;
    }

    /**
     * Binarize the specified image.
     * @param src Can be 8, 24 or 32 bits (corresponding to grayscale, jpeg, png)
     * @param thresh Gray value from what is assumed as white. 0 is black 255 is white
     * @return Binarized image
     */
    public Mat binarize(final Mat src, int thresh){
        Mat current=src.clone();
        if (CvType.channels(current.type())>1){
            Imgproc.cvtColor(current,current,Imgproc.COLOR_BGR2GRAY);
        }
        Imgproc.threshold(current, current, thresh, 255, Imgproc.THRESH_BINARY);

        Mat res = current.clone();
        current.release();
        src.release();
        return res;
    }
}

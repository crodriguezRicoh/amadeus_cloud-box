package es.ricoh.scantobox.imageprocessing;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import es.ricoh.scantobox.exception.PdfCreationException;
import es.ricoh.scantobox.util.ALogger;
import es.ricoh.scantobox.util.Constants;
import es.ricoh.scantobox.util.Preferences;
import es.ricoh.scantobox.util.Utils;


/**
 * Created by cristian.rodriguez on 09/01/2017.
 */

public class PdfEngine {

    //size in mm
    public static final double mmXinch = 0.03937007874;
    public static final double A4width = 210;
    public static final double A4height = 297;
    public static final double Letterwidth = 216;
    public static final double Letterheight = 279;
    public static final double Legalwidth = 216;
    public static final double Legalheight = 356;
    public static final double Oficiowidth = 216;
    public static final double Oficioheight = 340;
    public static final double A3width = 420;
    public static final double A3height = 297;
    public static final double DLetterwidth = 279;
    public static final double DLetterheight = 432;

    private static final Logger log = ALogger.getLogger(PdfEngine.class);

    public PdfEngine(ArrayList<File> files) throws PdfCreationException {
        createMPDF(files);
    }

    private boolean createMPDF(ArrayList<File> files) throws PdfCreationException {
        try {

            log.debug("***** GENERANDO FICHERO PDF *****");

            String name = files.get(0).getAbsolutePath().substring(0, files.get(0).getAbsolutePath().lastIndexOf(".")) + ".pdf";
            log.debug("Creando fichero PDF con el nombre: " + name);

            float width = 0;
            float height = 0;

            Rectangle pSizeOut = getPageSize();

            if(Preferences.size.equals(Constants.sizeAuto)){

                com.lowagie.text.Image image = com.lowagie.text.Image.getInstance(files.get(0).getAbsolutePath());

                width = image.getWidth();
                height = image.getHeight();

                double mmWidth = (width * 25.4) / Preferences.resolutionDPI;
                double mmHeight = (height * 25.4) / Preferences.resolutionDPI;

                pSizeOut = new Rectangle((float) (mmWidth*mmXinch) *72, (float) (mmHeight*mmXinch) *72);

            }

            Document document = new Document(pSizeOut, 0, 0, 0, 0);
            PdfWriter.getInstance(document, new FileOutputStream(name));
            document.open();

            for(File f : files){

                com.lowagie.text.Image image = com.lowagie.text.Image.getInstance(f.getAbsolutePath());
                image.scaleToFit(pSizeOut.getWidth(), pSizeOut.getHeight());
                log.debug("Creando hoja del PDF con el tamaño --> " + "width: " + pSizeOut.getWidth() + " height: " + pSizeOut.getHeight());
                document.add(image);
            }

            document.close();

            return true;
        } catch(Exception e){
            log.error(Utils.printStackTraceToString(e));
            throw new PdfCreationException();
        }
    }

    private Rectangle getPageSize(){
        if (Preferences.size.equals(Constants.sizeA4)) { //Si es auto o A4 normal
            if(Preferences.orientation.equals(Constants.orientationL)){
                return PageSize.A4;
            }else{
                return new Rectangle(842.0F, 595.0F);
            }
        } else if(Preferences.size.equals(Constants.sizeA4L)){//Si A4 landscape
            if(Preferences.orientation.equals(Constants.orientationL)){
                return new Rectangle(842.0F, 595.0F);
            }else{
                return PageSize.A4;
            }
        } else if(Preferences.size.equals(Constants.sizeLetter)){//Si letter
            if(Preferences.orientation.equals(Constants.orientationL)){
                return PageSize.LETTER;
            }else{
                return new Rectangle(792.0F, 612.0F);
            }
        }else if(Preferences.size.equals(Constants.sizeLetterL)){//Si letter landscape
            if(Preferences.orientation.equals(Constants.orientationL)){
                return new Rectangle(792.0F, 612.0F);
            }else{
                return PageSize.LETTER;
            }
        }else if(Preferences.size.equals(Constants.sizeLegal)){//Si Legal
            if(Preferences.orientation.equals(Constants.orientationL)){
                return PageSize.LEGAL;
            }else{
                return new Rectangle(1008.0F, 612.0F);
            }
        }else if(Preferences.size.equals(Constants.sizeLegalL)){//Si Legal landscape
            if(Preferences.orientation.equals(Constants.orientationL)){
                return new Rectangle(1008.0F, 612.0F);
            }else{
                return PageSize.LEGAL;
            }
        }else if(Preferences.size.equals(Constants.sizeOficio)){//Si oficio
            //en iText 1pulgada=72 puntos (pasamos la madedia a pulgadas primero y multiplicamos por 72
            return new Rectangle((float) (Oficiowidth*mmXinch) *72, (float) (Oficioheight*mmXinch) *72);
        }else if(Preferences.size.equals(Constants.sizeA3)){//Si A3
            return new Rectangle(1191.0F, 842.0F);
        }

        return PageSize.A4;
    }
}

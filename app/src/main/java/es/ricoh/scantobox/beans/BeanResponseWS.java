package es.ricoh.scantobox.beans;

/**
 * Created by cristian.rodriguez on 18/01/2017.
 */

public class BeanResponseWS {

    public String callType = "";
    public String errorCode = "";
    public String errorMessage = "";
    public String documentProcessType = "";
    public String existError = "";
    public String idDocument = "";
}

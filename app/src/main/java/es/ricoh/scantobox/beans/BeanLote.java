package es.ricoh.scantobox.beans;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by cristian.rodriguez on 09/01/2017.
 */

public class BeanLote implements Serializable{

    public ArrayList<File> listSheets = new ArrayList<File>();
    int sheetsNumber = 0;

    public void addSheet(File sheet){
        listSheets.add(sheet);
        sheetsNumber++;
    }

    public File getSheet(int indice){
        return listSheets.get(indice);
    }

    public int getNumberOfSheets(){
        return listSheets.size();
    }
}

package es.ricoh.scantobox.beans;

/**
 * Created by alex.arroyo on 27/02/2017.
 */
public class BeanBoxItem {
    public int icon;
    public String itemId;
    public String itemName;
    public String type;
//    public String size;
    public BeanBoxItem(){
        super();
    }

    public BeanBoxItem(int icon, String itemId, String itemName, String type/*, String size*/) {
        super();
        this.icon = icon;
        this.itemId = itemId;
        this.itemName = itemName;
        this.type = type;
//        this.size = size;
    }
}

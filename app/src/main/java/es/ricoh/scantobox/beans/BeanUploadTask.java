package es.ricoh.scantobox.beans;

/**
 * Created by cristian.rodriguez on 14/03/2017.
 */

public class BeanUploadTask {

    public String upload_fileName = "";
    public String upload_status = "";
    public int numFiles;
    public int fileIndex;
    public String upload_response = "";
    public String upload_response_result = "";
}

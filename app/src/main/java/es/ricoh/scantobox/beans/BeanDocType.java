package es.ricoh.scantobox.beans;

import android.widget.RelativeLayout;

/**
 * Created by cristian.rodriguez on 16/01/2017.
 */

public class BeanDocType {

    public String docType_id = "";
    public String docType_description = "";
    public RelativeLayout layout;
}

package es.ricoh.libb64;

public class Encode64 {

    static {
        System.loadLibrary("b64");
    }

    native byte[] encode(String in);
    native byte[] encodePart(String in, long numPart, long maxSize);

    public byte[] encodeFile(String file){
        return encode(file);
    }

    public byte[] encodeFilePart(String in, long numPart, long maxSize){
        return encodePart(in, numPart, maxSize);
    }
}

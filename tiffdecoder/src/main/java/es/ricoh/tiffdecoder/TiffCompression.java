package es.ricoh.tiffdecoder;

/**
 * TiffCompression
 * 
 * @author Manuel.Madera
 *
 * Tiff compression algorithms.
 */
public interface TiffCompression {
	public final static int LZW = 0;
	public final static int JPEG = 1;
	public final static int CCITT_T6 = 2;
}

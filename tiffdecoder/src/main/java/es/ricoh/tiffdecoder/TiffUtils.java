package es.ricoh.tiffdecoder;

import android.util.Log;

/**
 * TiffUtils
 * 
 * @author Manuel.Madera
 *
 */
public class TiffUtils {
	
	static {
		System.loadLibrary("tiffdecoder");
	}

	/**
	 * Converts a tiff file into another tiff changing compression algorithm.
	 * @param out output file.
	 * @param in input file.
	 * @param compression compression algorithm.
	 */
	public static void convert(String out, String in, int compression) {
		nConvert(out, in, compression);
	}
	
	/**
	 * Merge several tiff files into one
	 * @param out output file
	 * @param in input files
	 * @param compression compression algorithm
	 */
	public static void merge(String out, String[] in, int compression) {
		nMerge(out, in, compression);
	}

	// Native fuctions
	private static native void nConvert(String out, String in, int compression);
	private static native void nMerge(String out, String[] in, int compression);
}

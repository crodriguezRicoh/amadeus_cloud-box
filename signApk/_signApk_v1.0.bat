﻿::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::: SignApk v1.0 :::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::

SET unsignedAppPath="..\app\build\outputs\apk\app-release-unsigned.apk"
SET productID=1679820800
SET appName=ScanToBox
SET appVersion=1.0.0

::	Copiamos el apk sin firmar al directorio actual
COPY %unsignedAppPath% app-release-unsigned.apk

::	Añadimos la información del manifest al apk
jar uvmf %productID%.mf app-release-unsigned.apk

::	Firmamos el apk con la keystore de Ricoh
jarsigner -verbose -digestalg SHA1 -sigalg MD5withRSA -keystore .\ricohdevelop.keystore -storepass srseb2r app-release-unsigned.apk ricohtestkey -keypass brkn4u

::	Creamos la carpeta con el nombre de la aplicación
MD %appName% 2>NUL

::	Movemos el .apk una vez firmado a la carpeta <appName> y lo renombramos a <appName>
MOVE app-release-unsigned.apk %appName%\%appName%.apk

::	Creamos el .dalp y su contenido en la carpeta <appName>
@echo ^<?xml version="1.0" encoding="utf-8"?^> > %appName%\%appName%.dalp
@echo ^<dalp spec="3.0" version= "1.0" href="%appName%.dalp"^> >> %appName%\%appName%.dalp
@echo ^<product-id^>%productID%^</product-id^> >> %appName%\%appName%.dalp
@echo ^<information^> >> %appName%\%appName%.dalp
@echo 	^<title^>%appName%^</title^> >> %appName%\%appName%.dalp
@echo 	^<vendor^>Ricoh^</vendor^> >> %appName%\%appName%.dalp
@echo 	^<application-ver^>%appVersion%^</application-ver^> >> %appName%\%appName%.dalp
@echo ^</information^> >> %appName%\%appName%.dalp
@echo ^<information locale="en_US"^> >> %appName%\%appName%.dalp
@echo 	^<title^>%appName%^</title^> >> %appName%\%appName%.dalp
@echo 	^<vendor^>Ricoh^</vendor^> >> %appName%\%appName%.dalp
@echo 	^<application-ver^>%appVersion%^</application-ver^> >> %appName%\%appName%.dalp
@echo ^</information^> >> %appName%\%appName%.dalp
@echo ^<information locale="ja_JP"^> >> %appName%\%appName%.dalp
@echo 	^<title^>%appName%^</title^> >> %appName%\%appName%.dalp
@echo 	^<vendor^>（株）リコー^</vendor^> >> %appName%\%appName%.dalp
@echo 	^<application-ver^>%appVersion%^</application-ver^> >> %appName%\%appName%.dalp
@echo ^</information^> >> %appName%\%appName%.dalp
@echo ^<resources^> >> %appName%\%appName%.dalp
@echo 	^<apk href="./%appName%.apk"/^> >> %appName%\%appName%.dalp
@echo ^</resources^> >> %appName%\%appName%.dalp
@echo ^<apk_app_info^> >> %appName%\%appName%.dalp
@echo 	^<need_reboot^>true^</need_reboot^> >> %appName%\%appName%.dalp
@echo 	^<need_activate^>false^</need_activate^> >> %appName%\%appName%.dalp
@echo ^</apk_app_info^> >> %appName%\%appName%.dalp
@echo ^</dalp^> >> %appName%\%appName%.dalp


::	Comprimimos la carpeta <appName> con el mismo nombre <appName>.zip
::	(Es necesario tener instalado 7zip y añadir la ruta de instalación a la variable de entorno "PATH")
7z a -tzip %appName%-%appVersion%.zip %appName%
